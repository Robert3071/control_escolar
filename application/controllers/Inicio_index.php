<?php

class Inicio_index extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('session');
        $this->load->helper('calendar');
    }

    public function actas() {
        $data = array();

        $data = [
            'inicio_view' => $this->load->view('inicio/actas_teacher_view', $data, TRUE),
        ];
        $data = array(
            'header' => $this->load->view('templates/general/header_view', $data, TRUE),
            'main' => $this->load->view('templates/general/main_view', $data, TRUE),
            'footer' => $this->load->view('templates/general/footer_view', $data, TRUE),
            'scripts' => array('<link rel="stylesheet" href="' . base_url('assets/registro/css/calendarios.css') . '">')
        );
        $this->load->view('layout_general_view', $data);
    }

    public function actas_admin() {
        $data = array();

        $data = [
            'inicio_view' => $this->load->view('inicio/actas_admin_view', $data, TRUE),
        ];
        $data = array(
            'header' => $this->load->view('templates/general/header_view', $data, TRUE),
            'main' => $this->load->view('templates/general/main_view', $data, TRUE),
            'footer' => $this->load->view('templates/general/footer_view', $data, TRUE)
        );
        $this->load->view('layout_general_view', $data);
    }

    public function admin_jefe_carrera() {
        $data = array();

        $data = [
            'inicio_view' => $this->load->view('inicio/admin_jefe_carrera_view', $data, TRUE),
        ];
        $data = array(
            'header' => $this->load->view('templates/general/header_view', $data, TRUE),
            'main' => $this->load->view('templates/general/main_view', $data, TRUE),
            'footer' => $this->load->view('templates/general/footer_view', $data, TRUE)
        );
        $this->load->view('layout_general_view', $data);
    }

    public function calendarios() {

        $data = array();
        $tipo_calendario = $this->input->get('calendary');
        calendario_view($tipo_calendario); //funcion proveniente del helper session_helper.php
    }

}
