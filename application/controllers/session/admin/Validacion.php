<?php

defined('BASEPATH') or exit('No direct script access allowded');

class Validacion extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		ini_set('max_execution_time', 0);
		ini_set('memory_limit', '-1');
		$library = array(
			'pdfgenerator',
			'form_validation', 'zip'
		);
		$this->load->library($library);
		$this->load->helper(array('session', 'text_calificacion', "file"));
		checar_rol_admin_estudiantiles();
		$this->load->model(array('admin/Admin_queries'));

	}

	public function index()
	{
		$data = array(
			'admin' => json_decode(json_encode($this->Admin_queries->get_admin($_SESSION['id'])), true)
		);
		$this->get_template($data);
	}

	private function get_template($data)
	{
		$data = array(
			'header' => $this->load->view('templates/session/admin/head_view', '', TRUE),
			'aside' => $this->load->view('templates/session/admin/aside_view', '', TRUE),
			'main' => $this->load->view('admin/validacion_view', $data, TRUE),
			'footer' => $this->load->view('templates/general/footer_view', '', TRUE),
			'scripts' => [
				'<script  src="' . base_url('assets/registro/js/admin/validacion.js') . '"></script>'
			]
		);

		$this->load->view('layout_general_view', $data);
	}

	public function get_list_lic_by_campus()
	{
		$licenciaturas = $this->Admin_queries->get_list_lic_by_campus($_SESSION['id_campus']);
		echo json_encode($licenciaturas);
	}

	public function get_year_by_campus_lic()
	{

		$years = $this->Admin_queries->get_year_by_campus_lic($this->input->get('campus_carrer'));
		echo json_encode($years);
	}

	public function get_cicles()
	{
		$ciclos = $this->Admin_queries->get_cicles($this->input->get('campus_carrer'), $this->input->get('year'));
		echo json_encode($ciclos);
	}

	public function get_type_group()
	{
		$tipos_grupo = $this->Admin_queries->get_type_group($this->input->get('campus_carrer'), $this->input->get('year'), $this->input->get('ciclo'));
		echo json_encode($tipos_grupo);
	}

	public function get_all_groups()
	{
		$grupos = $this->Admin_queries->get_all_groups($this->input->get('campus_carrer'), $this->input->get('year'), $this->input->get('ciclo'), $this->input->get('tipo_grupo'));
		echo json_encode($grupos);
	}

	//    funcion que muestra la lista de alumnos dependiendo del grupo seleccionado


	public function get_all_groups_qualified()
	{
		$grupos = $this->Admin_queries->get_all_groups_by_validate($this->input->get('campus_carrer'), $this->input->get('year'), $this->input->get('ciclo'), $this->input->get('tipo_grupo'), $_SESSION['id_campus']);
		echo json_encode($grupos);
	}

	public function get_count_gruops()
	{
		$grupos = $this->Admin_queries->get_count_gruops($this->input->get('campus_carrer'), $this->input->get('year'), $this->input->get('ciclo'), $this->input->get('tipo_grupo'), $_SESSION['id_campus']);
		echo json_encode($grupos);
	}

	public function get_count_gruops_validated()
	{
		$grupos = $this->Admin_queries->get_count_gruops_validated($this->input->get('campus_carrer'), $this->input->get('year'), $this->input->get('ciclo'), $this->input->get('tipo_grupo'), $_SESSION['id_campus']);
		echo json_encode($grupos);
	}public function get_count_gruops_validated_no_json()
	{
		$grupos = $this->Admin_queries->get_count_gruops_validated($this->input->get('campus_carrer'), $this->input->get('year'), $this->input->get('ciclo'), $this->input->get('tipo_grupo'), $_SESSION['id_campus']);
		return $grupos;
	}

	public function get_all_students_by_id_group()
	{
		$type_groups = $this->Admin_queries->get_all_students_by_id_group_validate($this->input->get('ID_teacher_by_group'), $_SESSION['id_campus']);
		return $type_groups;
	}//    funcion que muestra la lista de alumnos dependiendo del grupo seleccionado

	public function get_all_students_by_id_group_validate($id_teacher_by_group)
	{
		$type_groups = $this->Admin_queries->get_all_students_by_id_group_validate($id_teacher_by_group, $_SESSION['id_campus']);
		return $type_groups;
	}

	public function ver_acta()
	{
//        $this->input->get('ID_teacher_by_group') = $this->input->get('ID_teacher_by_group');
		$type_groups = $this->get_all_students_by_id_group();
		$data = array();
		$data = [
			'grupo' => $type_groups,
			'datos_grupo' => $this->Admin_queries->get_group_validate($this->input->get('ID_teacher_by_group'), $_SESSION['id_campus'])
		];
		$data = array(
			'header' => $this->load->view('templates/session/admin/head_view', '', TRUE),
			'aside' => $this->load->view('templates/session/admin/aside_view', '', TRUE),
			'main' => $this->load->view('admin/ver_acta_view', $data, TRUE),
			'footer' => $this->load->view('templates/general/footer_view', '', TRUE),
		);
		$this->load->view('layout_general_view', $data);
	}

	public function validate_grupos()
	{
		$_POST = json_decode(file_get_contents("php://input"), true);//NECESARIO para HACER EL USO   DEL METHOD POST
		$id_grupos = [];

		foreach ($_POST['grupos'] as $grupo) {
			$id_grupos[] = $grupo['ID_teacher_by_group'];
		}
		$this->Admin_queries->validate_grupos($id_grupos, $_POST['campus_carrer'], $_POST['ciclo'], $_POST['tipo_grupo'], $_POST['year']);
//        $grupos = $this->Admin_queries->get_all_groups_by_validate($this->input->get('campus_carrer'),$this->input->get('year'),$this->input->get('ciclo'),$this->input->get('tipo_grupo'));
//        echo json_encode($grupos);
	}


	public function delete_multi_actas_pdf()
	{
//		$path=$this->config->base_url()."assets/documents/actas".$this->input->get('campus_carrer')."-".$this->input->get('year')."-".$this->input->get('ciclo')."/";
		$path = "assets/documents/actas" . $this->input->get('campus_carrer') . "-" . $this->input->get('year') . "-" . $this->input->get('ciclo') . "/";//REVISAR
		delete_files("assets/documents/actas" . $this->input->get('campus_carrer') . "-" . $this->input->get('year') . "-" . $this->input->get('ciclo') . "/", TRUE);
		rmdir("assets/documents/actas" . $this->input->get('campus_carrer') . "-" . $this->input->get('year') . "-" . $this->input->get('ciclo') . "/"); // Delete the folder
	}

	public function generar_multi_actas_pdf()
	{

		$grupos = $this->Admin_queries->get_all_groups_by_validate($this->input->get('campus_carrer'), $this->input->get('year'), $this->input->get('ciclo'), $this->input->get('tipo_grupo'), $_SESSION['id_campus']);
		if ((int)count($grupos) == (int)$this->get_count_gruops_validated_no_json()->cantidad) {
			$name_zip = $this->input->get('campus_carrer') . "-" . $this->input->get('year') . "-" . $this->input->get('ciclo');
			$path = "assets/documents/actas" . $this->input->get('campus_carrer') . "-" . $this->input->get('year') . "-" . $this->input->get('ciclo') . "/";
			foreach ($grupos as $grupo) {
				$type_groups = $this->get_all_students_by_id_group_validate($grupo["ID_teacher_by_group"]);
				$data = [
					'grupo' => $type_groups,
					'datos_grupo' => $this->Admin_queries->get_group_validate($grupo["ID_teacher_by_group"], $_SESSION['id_campus'])
				];
				$html = $this->load->view('/templates/general/acta/pdf_acta_view', $data, TRUE);
				$filename = 'actas' . $grupo["group_name"] . '.pdf';
				$this->pdfgenerator->generate_multi_pdf_save($html, $filename, TRUE, 'Letter', 'portrait', FCPATH . $path);

			}
			$this->zip->read_dir($path, FALSE);
			$this->zip->download($name_zip . '.zip', FALSE);
		} else {
			return FALSE;
		}

	}

}
