<?php

defined('BASEPATH') or exit('No direct script access allowded');

class Admin_history_teacher extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library(array('form_validation'));
        $this->load->helper('session');
        checar_rol_admin();
        $this->load->helper('calendar_helper');
        $this->load->model(array('admin/Admin_queries'));
    }

    public function index() {
        $data = array(
            'teachers' => $this->Admin_queries->get_all_teacher(anio(), ciclo(), $_SESSION['id_campus'])
        );
        $this->get_template($data);
    }

    private function get_template($data) {
        $data = array(
            'header' => $this->load->view('templates/session/admin/head_view', '', TRUE),
            'aside' => $this->load->view('templates/session/admin/aside_view', '', TRUE),
            'main' => $this->load->view('admin/teachers_view', $data, TRUE),
			'footer' => $this->load->view('templates/general/footer_view', '', TRUE),
            'scripts' => ['<link rel="stylesheet" href="' . base_url('assets/dist/css/admin/tables/main.css') . '">',
                '<script  src="' . base_url('assets/registro/js/admin/datatable_config.js') . '"></script>'
            ]
        );

        $this->load->view('layout_general_view', $data);
    }

    //    http://localhost/kardex/session/admin/Admin_history_teacher/get_data_history_teacher
    public function get_data_history_teacher($ID_teacher) {
        $data = array(
            'data_info_teacher' => $this->Admin_queries->get_data_teacher_by_id($ID_teacher),
            'data_groups' => $this->_get_groups_by_id_teacher($ID_teacher),
            'data_count_actas' => $this->_get_count_actas_by_id_teacher($ID_teacher)
        );
        return $data;
    }

    public function _get_groups_by_id_teacher($id_teacher) {
        return $this->Admin_queries->get_groups_by_id_teacher($id_teacher, anio(), ciclo(), $_SESSION['id_campus']);
    }

    public function _get_count_actas_by_id_teacher($id_teacher) {
        return $this->Admin_queries->get_count_actas_by_id_teacher($id_teacher, anio(), ciclo(), $_SESSION['id_campus']);
    }

    //    http://localhost/kardex/session/admin/Admin_history_teacher/get_count_actas_by_campus
    public function get_count_actas_by_campus() {
        //$actas=$this->Admin_queries->get_count_actas_by_campus($anio_actual,$ciclo_actual,1);
        $actas = $this->Admin_queries->get_count_actas_by_campus(anio(), ciclo(), $_SESSION['id_campus']);
        echo json_encode($actas);
    }

    public function get_count_actas_by_career() {
        //$actas=$this->Admin_queries->get_count_actas_by_campus($anio_actual,$ciclo_actual,1);
        $actas = $this->Admin_queries->get_count_actas_by_career(anio(), ciclo(), $_SESSION['id_campus']);
        echo json_encode($actas);
    }

    public function detalle_campus() {
        //var_dump($this->get_data_history_teacher($this->input->get('ID_teacher')));
        $data = $this->get_data_history_teacher($this->input->get('ID_teacher'));

        $data = array(
            'header' => $this->load->view('templates/session/admin/head_view', '', TRUE),
            'aside' => $this->load->view('templates/session/admin/aside_view', '', TRUE),
            'main' => $this->load->view('admin/detalle_teacher_view', $data, TRUE),
			'footer' => $this->load->view('templates/general/footer_view', '', TRUE),
            'scripts' => ['<link rel="stylesheet" href="' . base_url('assets/dist/css/admin/tables/main.css') . '">',
                '<script  src="' . base_url('assets/registro/js/admin/datatable_config.js') . '"></script>'
        ]);
        $this->load->view('layout_general_view', $data);
    }

    public function crear_firma() {

        $datos = $this->Admin_queries->get_info_firma($this->input->get('ID_teacher'));
        $data = array(
            'data_groups' => $this->Admin_queries->get_info_firma($this->input->get('ID_teacher')),
        );
        $l_name = $this->eliminar_acentos(str_split($datos[0]['name']));
        $nickname = explode("@", $datos[0]['email']);
        $apellidos = $this->eliminar_acentos(explode(" ", $datos[0]['surnames']));




        $l_ap = str_split($apellidos[0]);
        $l_am = str_split($apellidos[1]);
        if (count($apellidos) > 1) {
            // esto es la contraseña de la firma si tiene dos apellidos
            $pass_firma = $l_ap[0] . $l_ap[1] . $l_am[0] . $l_am[1] . $l_name[0] . $nickname[0];
        } else {
            // esto es la contraseña de la firma si tiene 1 apellido
            $pass_firma = $l_ap[0] . $l_ap[1] . $l_name[0] . $nickname[0];
        }


        // esto genera la firma (llave el texto cifrado)
        $has = password_hash($l_ap[0], PASSWORD_DEFAULT) . password_hash($l_ap[0], PASSWORD_DEFAULT) . password_hash($l_ap[1], PASSWORD_DEFAULT) . password_hash($l_am[0], PASSWORD_DEFAULT) . password_hash($l_am[1], PASSWORD_DEFAULT) . password_hash($l_name[0], PASSWORD_DEFAULT) . password_hash($nickname[0], PASSWORD_DEFAULT);


        if ($datos[0]['key_validate']) {
            // ya esta generada
        } else {
            if ($this->Admin_queries->update_firma_and_pass($datos[0]['ID_teacher'], $has, $pass_firma)) {
                //echo 'se guardo';
            } else {
                //echo 'error';
            }
        }


        $data = array(
            'header' => $this->load->view('templates/session/admin/head_view', '', TRUE),
            'aside' => $this->load->view('templates/session/admin/aside_view', '', TRUE),
            'main' => $this->load->view('admin/firma_teacher_view', $data, TRUE),
			'footer' => $this->load->view('templates/general/footer_view', '', TRUE),
        );
        $this->load->view('layout_general_view', $data);
    }

    public function descargar_fiel() {
        $datos = $this->Admin_queries->get_info_firma($this->input->get('ID_teacher'));
        $apellidos = $this->eliminar_acentos(explode(" ", $datos[0]['surnames']));
        if (count($apellidos) > 1) {

            $archivo = $this->eliminar_acentos($datos[0]['name']) . $apellidos[0] . $apellidos[1];
        }

        if (count($apellidos) == 1) {
            $archivo = $this->eliminar_acentos($datos[0]['name']) . $apellidos[0];
        }

        echo $datos[0]['key_validate'];
        header("Cache-Control: public");
        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=$archivo.irc");
        header("Content-Type: application/zip");
        header("Content-Transfer-Encoding: binary");
    }

    function eliminar_acentos($cadena) {

        //Reemplazamos la A y a
        $cadena = str_replace(
                array('Á', 'À', 'Â', 'Ä', 'á', 'à', 'ä', 'â', 'ª'),
                array('A', 'A', 'A', 'A', 'a', 'a', 'a', 'a', 'a'),
                $cadena
        );

        //Reemplazamos la E y e
        $cadena = str_replace(
                array('É', 'È', 'Ê', 'Ë', 'é', 'è', 'ë', 'ê'),
                array('E', 'E', 'E', 'E', 'e', 'e', 'e', 'e'),
                $cadena);

        //Reemplazamos la I y i
        $cadena = str_replace(
                array('Í', 'Ì', 'Ï', 'Î', 'í', 'ì', 'ï', 'î'),
                array('I', 'I', 'I', 'I', 'i', 'i', 'i', 'i'),
                $cadena);

        //Reemplazamos la O y o
        $cadena = str_replace(
                array('Ó', 'Ò', 'Ö', 'Ô', 'ó', 'ò', 'ö', 'ô'),
                array('O', 'O', 'O', 'O', 'o', 'o', 'o', 'o'),
                $cadena);

        //Reemplazamos la U y u
        $cadena = str_replace(
                array('Ú', 'Ù', 'Û', 'Ü', 'ú', 'ù', 'ü', 'û'),
                array('U', 'U', 'U', 'U', 'u', 'u', 'u', 'u'),
                $cadena);

        //Reemplazamos la N, n, C y c
        $cadena = str_replace(
                array('Ñ', 'ñ', 'Ç', 'ç'),
                array('N', 'n', 'C', 'c'),
                $cadena
        );

        return $cadena;
    }

}
