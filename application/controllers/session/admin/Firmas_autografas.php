<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Firmas_autografas extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('file');
        $this->load->helper('session');
        checar_rol_admin();
        $this->load->helper('calendar_helper');
        $this->load->model(array('admin/Admin_queries'));
    }

    public function generar_qr(/* $user_id */) {
        $this->load->library('ciqrcode');
        $user_id = 12;
        //hacemos configuraciones
        $params['data'] = 'folio: IRC-2021-hmlt';
        $params['level'] = 'M';
        $params['size'] = 10;

        //decimos el directorio a guardar el codigo qr, en este 
        //caso una carpeta en la raíz llamada qr_code
        $params['savename'] = FCPATH . "assets/firmas/qr_$user_id.png";
        //generamos el código qr
        $this->ciqrcode->generate($params);

        $data['img'] = "qr_$user_id.png";
        echo 'hola';
        /*
          $this->load->view('admin/header');
          $this->load->view('admin/codigo_qr', $data);
          $this->load->view('footer');
         */
    }

    public function index() {
        $qrCode = new Endroid\QrCode\QrCode('fgf');

        header('Content-Type: ' . $qrCode->getContentType());
        echo $qrCode->writeString();
//
//        $this->load->helper('url');
//        $this->load->view('datos_view');
    }

//
////localhost/control_escolar/control_escolar/admin/actas/#!/validation-firma
//    public function upload_firma() {
//        if ($this->file_check()) {
////        print_r($_FILES);
////        print_r($_POST);
//            $firma_servidor = file_get_contents("documents/docentes/firmas/firma.irc");
//            $firma_temp = file_get_contents($_FILES['file']['tmp_name']);
//            if ($firma_servidor) {
//                if ($firma_temp === $firma_servidor) {
//                    echo json_encode(array('data' => 'Las firmas coinciden', "code" => $code = 200));
//                } else {
//                    echo json_encode(array('message' => 'Las firmas no coinciden', "code" => $code = 400));
//                }
//            } else {
//                echo json_encode(array('message' => 'El archivo no se cargo correctamente o la extenci&oacute;n no es compatible', "code" => $code = 400));
//            }
//        } else {
//            echo json_encode(array('message' => 'No existe ninguna firma asociada ', "code" => $code = 400));
//        }
//        http_response_code($code);
//    }
//
//    public function save_file_firma() {
//        if (move_uploaded_file($_FILES['file']['tmp_name'], 'public-html/documents/' . $_FILES['file']['name'])) {
////            $this->response('El archivo se subio correctamente', REST_Controller::HTTP_CREATED);
//            echo json_encode(array('message' => 'El archivo se subio correctamente', "code" => $code = 200));
//        } else {
//            echo json_encode(array('message' => 'El archivo no se subio correctamente', "code" => $code = 500));
////            $this->response('El archivo no se subio correctamente', REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
//        }
//        http_response_code($code);
//    }
//
//    public function file_check() {
//        $this->load->library('form_validation');
//        $allowed_mime_type_arr = 'irc';
////        print_r($_FILES);
////        echo ($_FILES['file']['name']);
////        if ($_FILES && $_FILES['file']['name'] && $_FILES['file']['name'] != "") {
//        if ($_FILES['file']['name'] && !empty($_FILES['file']['name'])) {
//            $mime = get_mime_by_extension($_FILES['file']['name']);
//            $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
////              echo $mime;
//            if ($ext == $allowed_mime_type_arr) {
//                return TRUE;
////                echo 'si';
//            } else {
////                echo 'si';
//                return FALSE;
//            }
//        } else {
//            return FALSE;
//        }
//    }
//
//    public function send_email_firma() {
//
//        check_params($this->post());
//        $email = $this->usuarios_model->search_mail($this->post('correo'));
//        if ($email) {
//            $this->load->library('email');
//            $this->email->initialize($this->email_config());
//            $this->email->from('losper.jesy@gmail.com', 'PRUEBAS');
//            $this->email->to('losper.jesy@gmail.com');
//            $this->email->subject('IRC clave');
//            $this->email->message($this->_create_email_information($email['id']));
//            if ($this->email->send()) {
//                $this->response(['message' => 'Hemos enviado las instrucciones de restablecimiento de contrase&ntilde;a a tu direcci&oacute;n de correo electr&oacute;nico.'], REST_Controller::HTTP_OK);
//            } else {
//                if (ENVIRONMENT === 'production') {
//                    $this->response(['message' => 'No se ha podido enviar el correo, por favor intentalo m&aacute;s tarde'], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
//                }
//                $this->response($this->email->print_debugger(), REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
//            }
//        } else {
//            $this->response(['message' => 'No hay ningun usuario en el sistema con el correo proporcionado.'], REST_Controller::HTTP_NOT_FOUND);
//        }
//    }

    public function create_path() {
        $micarpeta = '';
        switch ($_SESSION['id_campus']) {
            case 5://LAD
                $list_docentes = $this->Admin_queries->get_all_teacher(anio(), ciclo(), $_SESSION['id_campus']);
                foreach ($list_docentes as $docente) {
                    $this->generar_qr_acta($docente);
                }


                break;

            default:
                return false;
                break;
        }
    }

    public function generar_qr_acta($docente) {

        $this->load->library('ciqrcode');

        $datos_qr = 'Nombre/s:' . $docente['name'] . ' ' . 'Apellidos:' . $docente['surnames'].'Plan:'.$docente['key_plan'];
        //hacemos configuraciones
        $params['data'] = $datos_qr;
        $params['level'] = 'M';
        $params['size'] = 5;

        $micarpeta = 'firm_autografas/lad/' . $docente['ID_teacher'] . '-' . str_replace(' ','_',$docente['name_user']);

        if (!file_exists($micarpeta)) {
            mkdir($micarpeta, 0777, true);
            $params['savename'] = FCPATH . $micarpeta. '/' . $docente['name_user'] . '.png';
            $this->Admin_queries->update_firma_autografa($docente['ID_teacher'], $micarpeta . '/' . str_replace(' ','_',$docente['name_user']) . '.png');
            //generamos el código qr
            $this->ciqrcode->generate($params);

        }

        //decimos el directorio a guardar el codigo qr, en este
    }

}
