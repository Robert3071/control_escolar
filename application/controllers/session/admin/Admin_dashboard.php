<?php

defined('BASEPATH') or exit('No direct script access allowded');

class Admin_dashboard extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('form_validation'));
        $this->load->helper('session');
        $this->load->helper('calendar');
        checar_rol_admin();
        $this->load->helper('text_calificacion_helper');
        $this->load->model(array('admin/Admin_queries'));
        $library = array(
            'pdfgenerator',
            'DataListGeneral',
            'form_validation'
        );
        $this->load->library($library);
    }

    public function index()
    {
        $data = array(
            'admin' => json_decode(json_encode($this->Admin_queries->get_admin($_SESSION['id'])), true)
        );
        $this->get_template($data);
    }

    private function get_template($data)
    {
        $data = array(
            'header' => $this->load->view('templates/session/admin/head_view', '', TRUE),
            'aside' => $this->load->view('templates/session/admin/aside_view', '', TRUE),
            'main' => $this->load->view('admin/datos_view', $data, TRUE),
			'footer' => $this->load->view('templates/general/footer_view', '', TRUE),
        );

        $this->load->view('layout_general_view', $data);
    }
    public function grupos()
    {
        $data = array(
            'grupos' => $this->Admin_queries->get_all_groups(anio(), ciclo(), $_SESSION['id_campus'])
        );
        $data = array(
            'header' => $this->load->view('templates/session/admin/head_view', '', TRUE),
            'aside' => $this->load->view('templates/session/admin/aside_view', '', TRUE),
            'main' => $this->load->view('admin/grupos_view', $data, TRUE),
			'footer' => $this->load->view('templates/general/footer_view', '', TRUE),
            'scripts' => ['<link rel="stylesheet" href="' . base_url('assets/dist/css/admin/tables/main.css') . '">',
            '<script  src="' . base_url('assets/registro/js/admin/datatable_config.js') . '"></script>'
                ]
        );
        $this->load->view('layout_general_view', $data);
    }

    public function rectificar_grupo()
    {

        $type_groups = $this->Admin_queries->get_all_students_by_id_group(anio(), ciclo(), $this->input->get('ID_teacher_by_group'), $_SESSION['id_campus']);
        //        if ($rectificar_grupo) {
        $data = array();
        $data = [
            'grupo' => $type_groups,
            'datos_grupo' => $this->Admin_queries->get_group($this->input->get('ID_teacher_by_group'), anio(), ciclo(), $_SESSION['id_campus'])
        ];
        $data = array(
            'header' => $this->load->view('templates/session/admin/head_view', '', TRUE),
            'aside' => $this->load->view('templates/session/admin/aside_view', '', TRUE),
            'main' => $this->load->view('admin/rectificar_grupos_view', $data, TRUE),
			'footer' => $this->load->view('templates/general/footer_view', '', TRUE),
        );
        $this->load->view('layout_general_view', $data);
        //          }
    }

    public function desrectificar_alumnos_by_grupo()
    {

        $type_groups = $this->Admin_queries->get_all_students_rectificados_by_id_group($this->input->get('ID_teacher_by_group'));
        $data = array();
        $data = [
            'grupo' => $type_groups,
            'datos_grupo' => $this->Admin_queries->get_group($this->input->get('ID_teacher_by_group'))
        ];
        $data = array(
            'header' => $this->load->view('templates/session/admin/head_view', '', TRUE),
            'aside' => $this->load->view('templates/session/admin/aside_view', '', TRUE),
            'main' => $this->load->view('admin/desrectificar_grupos_view', $data, TRUE),
			'footer' => $this->load->view('templates/general/footer_view', '', TRUE),
        );
        $this->load->view('layout_general_view', $data);
    }

    public function desrectificar_grupo()
    {
        $this->db->trans_start(); //inicia la transaccion 
        $this->Admin_queries->desrectificar_grupo_by_id_group($this->input->post('ID_teacher_by_group'));
        $this->Admin_queries->desrectificate_all_students_by_id_group($this->input->post('id_teacher_by_group'));
        $this->db->trans_complete(); //termina , en una sola transaccion para insertar varios registro o calcelar todo
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            //                imprimir error
        } else {
            $this->db->trans_commit();
            $this->session->set_flashdata('exito', 'Se registro correctamente');
            $this->grupos();
        }
    }

    //funcion que pinta los grupos 
    public function get_all_groups()
    {
        $groups = $this->Admin_queries->get_all_groups(anio(), ciclo(), $_SESSION['id_campus']);
        echo json_encode($groups);
    }

    public function activar_rectificar()
    {

        $url = 'session/admin/Admin_dashboard/rectificar_grupo?ID_teacher_by_group=' . $this->input->post('id_teacher_by_group');
        if (!empty($_POST['check_lista'])) {

            // Contando el numero de input seleccionados "checked" checkboxes.
            $checked_contador = count($_POST['check_lista']);
            echo "<p>Has seleccionado los siguientes " . $checked_contador . " opcione(s):</p> <br/>";
            $this->db->trans_start(); //inicia la transaccion 
            $this->Admin_queries->rectificar_grupo_by_id_group($this->input->post('id_teacher_by_group'));


            foreach ($_POST['check_lista'] as $seleccion) {
                $this->Admin_queries->update_rectificated_student($seleccion);
                echo "<p>" . $seleccion . "</p>";
            }
            $this->db->trans_complete(); //termina , en una sola transaccion para insertar varios registro o calcelar todo
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $this->session->set_flashdata('error_rectificar_bd', 'Ocurrio un error consulta al administrador');
                redirect($url);
            } else {
                $this->db->trans_commit();
                $this->session->set_flashdata('exito_rectificar', 'Se registro correctamente');
                redirect($url);
            }
            //mandar mensaje de exito
        } else {
            $this->session->set_flashdata('error_rectificar', 'Se registro correctamente');
            redirect($url);
            // Mandar mensaje de error aqui no se selecciono nada
        }
    }

    public function desactivar_rectificar_students()
    {
        $url = 'session/admin/Admin_dashboard/desrectificar_alumnos_by_grupo?ID_teacher_by_group=' . $this->input->post('id_teacher_by_group');

        if (!empty($_POST['check_lista'])) {
            // Contando el numero de input seleccionados "checked" checkboxes.
            $checked_contador = count($_POST['check_lista']);
            echo "<p>Has seleccionado los siguientes " . $checked_contador . " opcione(s):</p> <br/>";

            foreach ($_POST['check_lista'] as $seleccion) {
                $this->Admin_queries->update_desrectificated_student($seleccion);
                echo "<p>" . $seleccion . "</p>";
            }
            redirect($url);
        } else {
            redirect($url);
            // Mandar mensaje de error aqui no se selecciono nada
        }
    }

    

    public function pdf_acta()
    {
        $type_groups = $this->get_all_students_by_id_group_all();
        $data = array();
        $datos_grupo=$this->Admin_queries->get_group($this->input->get('ID_teacher_by_group'), anio(), ciclo(), $_SESSION['id_campus']);
        //subject_name
        
            $data = [
            'grupo' => $type_groups,
            'datos_grupo' => $this->Admin_queries->get_group($this->input->get('ID_teacher_by_group'), anio(), ciclo(), $_SESSION['id_campus'])
            ];
        //        print_r(S$data);
        $html = $this->load->view('teacher/pdf_acta_view', $data, TRUE);
        $filename = 'Acta';
        $this->pdfgenerator->generate($html, $filename, TRUE, 'Letter', 'portrait');
        
        

        
    }

    public function get_all_students_by_id_group()
    {
        $type_groups = $this->Admin_queries->get_all_students_by_id_group(anio(), ciclo(), $this->input->get('ID_teacher_by_group'), $_SESSION['id_campus']);
        return $type_groups;
    }
    /**
     * Esta función hace lo mismo que la de arriba
     * pero no excluye a los no rectificados
     */
    public function get_all_students_by_id_group_all()
    {
        $type_groups = $this->Admin_queries->get_all_students_by_id_group_acta($this->input->get('ID_teacher_by_group'));
        return $type_groups;
    }
}
