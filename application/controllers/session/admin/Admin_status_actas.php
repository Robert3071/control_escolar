<?php

defined('BASEPATH') or exit('No direct script access allowded');

class Admin_status_actas extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library(array('form_validation'));
        $this->load->helper('session');
        checar_rol_admin();
        $this->load->helper('calendar_helper');
        $this->load->model(array('admin/Admin_queries'));
    }

    public function index() {
        $data = array(
            'admin' => json_decode(json_encode($this->Admin_queries->get_admin($_SESSION['id'])), true)
        );
        $this->get_template($data);
    }

    private function get_template($data) {
        $data = array(
            'header' => $this->load->view('templates/session/admin/head_view', '', TRUE),
            'aside' => $this->load->view('templates/session/admin/aside_view', '', TRUE),
            'main' => $this->load->view('admin/status_actas_view', $data, TRUE),
			'footer' => $this->load->view('templates/general/footer_view', '', TRUE),
            'scripts' => [
                '<script  src="' . base_url('assets/registro/js/admin/status_actas.js') . '"></script>'
            ]
        );
        $this->load->view('layout_general_view', $data);
    }

    //    http://localhost/kardex/session/admin/Admin_history_teacher/get_count_actas_by_campus
    public function get_count_actas_by_campus() {
        //$actas=$this->Admin_queries->get_count_actas_by_campus($anio_actual,$ciclo_actual,1);
        $actas = $this->Admin_queries->get_count_actas_by_campus(anio(), ciclo(), $_SESSION['id_campus']);
        echo json_encode($actas);
    }

    public function get_count_actas_by_career() {
        //$actas=$this->Admin_queries->get_count_actas_by_campus($anio_actual,$ciclo_actual,1);
        $actas = $this->Admin_queries->get_count_actas_by_career(anio(), ciclo(), $_SESSION['id_campus']);
        echo json_encode($actas);
    }

    public function detalle_campus() {
        $data = array(
            //            'grupos' => $this->Admin_queries->get_groups_by_campus($this->input->get('ID_campus'), date('Y'), '20-1', 1)
            'grupos' => $this->Admin_queries->get_groups_by_campus($this->input->get('ID_campus'), anio(), ciclo())
        );
        $data = array(
            'header' => $this->load->view('templates/session/admin/head_view', '', TRUE),
            'aside' => $this->load->view('templates/session/admin/aside_view', '', TRUE),
            'main' => $this->load->view('admin/detalle_campus_view', $data, TRUE),
			'footer' => $this->load->view('templates/general/footer_view', '', TRUE),
            'scripts' => ['<link rel="stylesheet" href="' . base_url('assets/dist/css/admin/tables/main.css') . '">',
                '<script  src="' . base_url('assets/registro/js/admin/datatable_config.js') . '"></script>'
            ]
        );
        $this->load->view('layout_general_view', $data);
    }

    public function detalle_career() {
        $data = array(
            //            'grupos' => $this->Admin_queries->get_groups_by_campus($this->input->get('ID_campus'), date('Y'), '20-1', 1)
            'grupos' => $this->Admin_queries->get_groups_by_career($this->input->get('ID_career'), anio(), ciclo())
        );
        $data = array(
            'header' => $this->load->view('templates/session/admin/head_view', '', TRUE),
            'aside' => $this->load->view('templates/session/admin/aside_view', '', TRUE),
            'main' => $this->load->view('admin/detalle_campus_view', $data, TRUE),
			'footer' => $this->load->view('templates/general/footer_view', '', TRUE),
            'scripts' => ['<link rel="stylesheet" href="' . base_url('assets/dist/css/admin/tables/main.css') . '">',
                '<script  src="' . base_url('assets/registro/js/admin/datatable_config.js') . '"></script>'
            ]);
        $this->load->view('layout_general_view', $data);
    }

}
