<?php

defined('BASEPATH') or exit('No direct script access allowded');

class Admin_status_actas extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library(array('form_validation'));
        $this->load->helper('session');
        checar_rol_admin_jef_carre();
        $this->load->helper('calendar_helper');
        $this->load->model(array('admin_jef_carr/Admin_jef_carr_model'));
    }

    public function index() {
        $data = array(
            'admin' => json_decode(json_encode($this->Admin_jef_carr_model->get_info_admin($_SESSION['id'])), true)
        );
        $this->get_template($data);
    }

    private function get_template($data) {
        $data = array(
            'header' => $this->load->view('templates/session/admin_jef_carr/head_view', '', TRUE),
            'aside' => $this->load->view('templates/session/admin_jef_carr/aside_view', '', TRUE),
            'main' => $this->load->view('admin_jef_carr/status_actas_view', $data, TRUE),
			'footer' => $this->load->view('templates/general/footer_view', '', TRUE),
            'scripts' => [
                '<script  src="' . base_url('assets/registro/js/admin_jef_carr/status_actas.js') . '"></script>'
            ]
        );
        $this->load->view('layout_general_view', $data);
    }

    public function get_count_actas_by_campus() {
        $actas = $this->Admin_jef_carr_model->get_count_actas_by_campus(anio(), ciclo(), $_SESSION['id_campus_carrer']);
        echo json_encode($actas);
    }

    public function detalle_campus_career() {
        $data = array(
            'grupos' => $this->Admin_jef_carr_model->get_groups_by_id_campus_career($_SESSION['id_campus_carrer'], anio(), ciclo())
        );
        $data = array(
            'header' => $this->load->view('templates/session/admin_jef_carr/head_view', '', TRUE),
            'aside' => $this->load->view('templates/session/admin_jef_carr/aside_view', '', TRUE),
            'main' => $this->load->view('admin_jef_carr/detalle_campus_view', $data, TRUE),
			'footer' => $this->load->view('templates/general/footer_view', '', TRUE),
            'scripts' => ['<link rel="stylesheet" href="' . base_url('assets/dist/css/admin/tables/main.css') . '">',
                '<script  src="' . base_url('assets/registro/js/admin/datatable_config.js') . '"></script>'
            ]
        );
        $this->load->view('layout_general_view', $data);
    }

}
