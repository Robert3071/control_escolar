<?php

defined('BASEPATH') or exit('No direct script access allowded');

class Admin_history_teacher extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library(array('form_validation'));
        $this->load->helper('session');
        checar_rol_admin_jef_carre();
        $this->load->helper('calendar_helper');
        $this->load->model(array('admin_jef_carr/Admin_jef_carr_model'));
    }

    public function index() {
        $data = array(
            'teachers' => $this->Admin_jef_carr_model->get_all_teachers(anio(), ciclo(), $_SESSION['id_campus_carrer'])
        );
        $this->get_template($data);
    }

    private function get_template($data) {
        $data = array(
            'header' => $this->load->view('templates/session/admin_jef_carr/head_view', '', TRUE),
            'aside' => $this->load->view('templates/session/admin_jef_carr/aside_view', '', TRUE),
            'main' => $this->load->view('admin_jef_carr/teachers_view', $data, TRUE),
			'footer' => $this->load->view('templates/general/footer_view', '', TRUE),
            'scripts' => ['<link rel="stylesheet" href="' . base_url('assets/dist/css/admin/tables/main.css') . '">',
                '<script  src="' . base_url('assets/registro/js/admin/datatable_config.js') . '"></script>'
            ]
        );

        $this->load->view('layout_general_view', $data);
    }

    public function _get_groups_by_id_teacher($id_teacher) {

        return $this->Admin_jef_carr_model->get_groups_by_id_teacher(anio(), ciclo(), $id_teacher, $_SESSION['id_campus_carrer']);
    }

    public function _get_count_actas_by_id_teacher($id_teacher, $id_campus_career) {
        return $this->Admin_jef_carr_model->get_count_actas_by_id_teacher(anio(), ciclo(), $id_teacher, $id_campus_career);
    }

    //    http://localhost/kardex/session/admin_jef_carr/Admin_history_teacher/get_count_actas_by_campus
    public function get_count_actas_by_campus() {
        //$actas=$this->Admin_jef_carr_model->get_count_actas_by_campus($anio_actual,$ciclo_actual,1);
        $actas = $this->Admin_jef_carr_model->get_count_actas_by_campus(anio(), ciclo(), $_SESSION['id_campus_carrer']);
        echo json_encode($actas);
    }

    //    http://localhost/kardex/session/admin_jef_carr/Admin_history_teacher/get_data_history_teacher
    public function get_data_history_teacher($ID_teacher) {
        $data = array(
            'data_info_teacher' => $this->Admin_jef_carr_model->get_data_teacher_by_id($ID_teacher),
            'data_groups' => $this->Admin_jef_carr_model->get_groups_by_id_teacher(anio(), ciclo(), $ID_teacher, $_SESSION['id_campus_carrer']),
            'data_count_actas' => $this->_get_count_actas_by_id_teacher($ID_teacher, $_SESSION['id_campus_carrer'])
        );
        return $data;
    }

    public function detalle_teacher() {
        //var_dump($this->get_data_history_teacher($this->input->get('ID_teacher')));
        
        $data = $this->get_data_history_teacher($this->input->get('ID_teacher'));

        $data = array(
            'header' => $this->load->view('templates/session/admin_jef_carr/head_view', '', TRUE),
            'aside' => $this->load->view('templates/session/admin_jef_carr/aside_view', '', TRUE),
            'main' => $this->load->view('admin_jef_carr/detalle_teacher_view', $data, TRUE),
			'footer' => $this->load->view('templates/general/footer_view', '', TRUE),
            'scripts' => ['<link rel="stylesheet" href="' . base_url('assets/dist/css/admin/tables/main.css') . '">',
                '<script  src="' . base_url('assets/registro/js/admin/datatable_config.js') . '"></script>'
            ]
        );
        $this->load->view('layout_general_view', $data);
    }

}
