<?php

defined('BASEPATH') or exit('No direct script access allowded');

class Admin_dashboard extends CI_Controller {

    public function __construct() {
        parent::__construct();
        checar_rol_admin_jef_carre();
        $this->load->helper('text_calificacion_helper');
        $this->load->model(array('admin_jef_carr/Admin_jef_carr_model'));
        $library = array(
            'pdfgenerator',
            'DataListGeneral'
        );
        $this->load->library($library);
    }

    public function index() {
        $data = array(
            'admin' => json_decode(json_encode($this->Admin_jef_carr_model->get_info_admin($_SESSION['id'])), true)
        );
        $this->get_template($data);
    }

    private function get_template($data) {
        $data = array(
            'header' => $this->load->view('templates/session/admin_jef_carr/head_view', '', TRUE),
            'aside' => $this->load->view('templates/session/admin_jef_carr/aside_view', '', TRUE),
            'main' => $this->load->view('admin_jef_carr/datos_view', $data, TRUE),
            'footer' => $this->load->view('templates/general/footer_view', '', TRUE),
        );

        $this->load->view('layout_general_view', $data);
    }

    public function grupos() {
        $data = array(
            'grupos' => $this->Admin_jef_carr_model->get_all_groups($_SESSION['id_campus_carrer'], anio(), ciclo())
        );
        $data = array(
            'header' => $this->load->view('templates/session/admin_jef_carr/head_view', '', TRUE),
            'aside' => $this->load->view('templates/session/admin_jef_carr/aside_view', '', TRUE),
            'main' => $this->load->view('admin_jef_carr/grupos_view', $data, TRUE),
			'footer' => $this->load->view('templates/general/footer_view', '', TRUE),
            'scripts' => ['<link rel="stylesheet" href="' . base_url('assets/dist/css/admin/tables/main.css') . '">',
            '<script  src="' . base_url('assets/registro/js/admin/datatable_config.js') . '"></script>'
                ]
        );
        $this->load->view('layout_general_view', $data);
    }

    public function rectificar_grupo() {

        $type_groups = $this->Admin_jef_carr_model->get_all_students_by_id_group($this->input->get('ID_teacher_by_group'), $_SESSION['id_campus_carrer'], anio(), ciclo());
//        if ($rectificar_grupo) {
        $data = array();
        $data = [
            'grupo' => $type_groups,
            'datos_grupo' => $this->Admin_jef_carr_model->get_group($this->input->get('ID_teacher_by_group'), $_SESSION['id_campus_carrer'])
        ];
        $data = array(
            'header' => $this->load->view('templates/session/admin_jef_carr/head_view', '', TRUE),
            'aside' => $this->load->view('templates/session/admin_jef_carr/aside_view', '', TRUE),
            'main' => $this->load->view('admin_jef_carr/rectificar_grupos_view', $data, TRUE),
			'footer' => $this->load->view('templates/general/footer_view', '', TRUE),
             'scripts' => ['<link rel="stylesheet" href="' . base_url('assets/dist/css/admin/tables/main.css') . '">',
            '<script  src="' . base_url('assets/registro/js/admin/datatable_config.js') . '"></script>'
                ]
        );
        $this->load->view('layout_general_view', $data);
//          }
    }

    public function desrectificar_alumnos_by_grupo() {

        $type_groups = $this->Admin_jef_carr_model->get_all_students_rectificados_by_id_group($this->input->get('ID_teacher_by_group'));
        $data = array();
        $data = [
            'grupo' => $type_groups,
            'datos_grupo' => $this->Admin_jef_carr_model->get_group($this->input->get('ID_teacher_by_group'))
        ];
        $data = array(
            'header' => $this->load->view('templates/session/admin_jef_carr/head_view', '', TRUE),
            'aside' => $this->load->view('templates/session/admin_jef_carr/aside_view', '', TRUE),
            'main' => $this->load->view('admin_jef_carr/desrectificar_grupos_view', $data, TRUE),
			'footer' => $this->load->view('templates/general/footer_view', '', TRUE),
        );
        $this->load->view('layout_general_view', $data);
    }

    public function desrectificar_grupo() {
        $this->db->trans_start(); //inicia la transaccion 
        $this->Admin_jef_carr_model->desrectificar_grupo_by_id_group($this->input->post('ID_teacher_by_group'));
        $this->Admin_jef_carr_model->desrectificate_all_students_by_id_group($this->input->post('id_teacher_by_group'));
        $this->db->trans_complete(); //termina , en una sola transaccion para insertar varios registro o calcelar todo
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
//                imprimir error
        } else {
            $this->db->trans_commit();
            $this->session->set_flashdata('exito', 'Se registro correctamente');
            $this->grupos();
        }
    }

    //funcion que pinta los grupos 
    public function get_all_groups() {
        $groups = $this->Admin_jef_carr_model->get_all_groups();
        echo json_encode($groups);
    }

    public function activar_rectificar() {

        $url = 'session/admin_jef_carr/Admin_dashboard/rectificar_grupo?ID_teacher_by_group=' . $this->input->post('id_teacher_by_group');
        if (!empty($_POST['check_lista'])) {

            // Contando el numero de input seleccionados "checked" checkboxes.
            $checked_contador = count($_POST['check_lista']);
            echo "<p>Has seleccionado los siguientes " . $checked_contador . " opcione(s):</p> <br/>";
            $this->db->trans_start(); //inicia la transaccion 
            $this->Admin_jef_carr_model->rectificar_grupo_by_id_group($this->input->post('id_teacher_by_group'));


            foreach ($_POST['check_lista'] as $seleccion) {
                $this->Admin_jef_carr_model->update_rectificated_student($seleccion);
                echo "<p>" . $seleccion . "</p>";
            }
            $this->db->trans_complete(); //termina , en una sola transaccion para insertar varios registro o calcelar todo
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $this->session->set_flashdata('error_rectificar_bd', 'Ocurrio un error consulta al administrador');
                redirect($url);
                
            } else {
                $this->db->trans_commit();
                $this->session->set_flashdata('exito_rectificar', 'Se registro correctamente');
                redirect($url);
            }
            //mandar mensaje de exito
        } else {
            $this->session->set_flashdata('error_rectificar', 'Se registro correctamente');
            redirect($url);
            // Mandar mensaje de error aqui no se selecciono nada
        }
    }

    public function desactivar_rectificar_students() {
        $url = 'session/admin_jef_carr/Admin_dashboard/desrectificar_alumnos_by_grupo?ID_teacher_by_group=' . $this->input->post('id_teacher_by_group');

        if (!empty($_POST['check_lista'])) {
            // Contando el numero de input seleccionados "checked" checkboxes.
            $checked_contador = count($_POST['check_lista']);
            echo "<p>Has seleccionado los siguientes " . $checked_contador . " opcione(s):</p> <br/>";

            foreach ($_POST['check_lista'] as $seleccion) {
                $this->Admin_jef_carr_model->update_desrectificated_student($seleccion);
                echo "<p>" . $seleccion . "</p>";
            }
            redirect($url);
        } else {
            redirect($url);
            // Mandar mensaje de error aqui no se selecciono nada
        }
    }

    public function pdf_acta() {
        $type_groups = $this->get_all_students_by_id_group_all();
        $data = array();
        $data = [
            'grupo' => $type_groups,
            'datos_grupo' => $this->Admin_jef_carr_model->get_group($this->input->get('ID_teacher_by_group'), $_SESSION['id_campus_carrer'])
        ];
        $html = $this->load->view('teacher/pdf_acta_view', $data, TRUE);
        $filename = 'Acta';
        $this->pdfgenerator->generate($html, $filename, TRUE, 'Letter', 'portrait');
    }

    public function get_all_students_by_id_group() {
        $type_groups = $this->Admin_jef_carr_model->get_all_students_by_id_group($this->input->get('ID_teacher_by_group'));
        return $type_groups;
    }

    /**
     * Esta función hace lo mismo que la de arriba
     * pero no excluye a los no rectificados
     */
    public function get_all_students_by_id_group_all() {
        $type_groups = $this->Admin_jef_carr_model->get_all_students_by_id_group_acta($this->input->get('ID_teacher_by_group'));
        return $type_groups;
    }

}
