<?php

defined('BASEPATH') or exit('No direct script access allowded');

class Grupos extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library(array('form_validation'));
        $this->load->helper('text_calificacion');
        checar_teacher_calificar();
        $this->load->model(array('teacher/Actas_model'));
    }

    public function index() {
        $data = array();
        $this->get_template($data);
    }

    private function get_template($data) {
        $data = array(
            'header' => $this->load->view('templates/session/teacher/head_view', '', TRUE),
            'aside' => $this->load->view('templates/session/teacher/aside_view', '', TRUE),
            'main' => $this->load->view('teacher/grupos_view', $data, TRUE),
              'footer' => $this->load->view('templates/general/footer_view', '', TRUE),
            'scripts' => [
                '<script  src="' . base_url('assets/registro/js/teacher/registro.js') . '"></script>',
                '<script  src="' . base_url('assets/registro/js/teacher/datos.js') . '"></script>',
            ]
        );
        $this->load->view('layout_general_view', $data);
    }

}
