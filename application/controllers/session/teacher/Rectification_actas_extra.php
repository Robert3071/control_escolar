<?php

defined('BASEPATH') or exit('No direct script access allowded');

class Rectification_actas_extra extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $library = array(
            'pdfgenerator',
            'DataListGeneral',
            'form_validation'
        );
        checar_teacher_rectificar_extras();
        $this->load->helper('text_calificacion');
        $this->load->library($library);
        $this->load->model(array('teacher/Rectification_actas_extra_model', 'admin/Admin_queries'));
    }

    public function index() {
        $data = array();
        $data = array(
            'teacher' => json_decode(json_encode($this->Rectification_actas_extra_model->get_teacher($_SESSION['id'])), true),
        );
        $this->get_template($data);
    }

//funcion para comparar pass de firma

    public function get_fiel_teacher() {
        $fiel = $this->Rectification_actas_extra_model->get_teacher($_SESSION['id_teacher']);
        echo json_encode($fiel);
    }

//funcion que pinta los campus con base al id_profesor pero solo los autorizados a rectificar calificacion
//    http://localhost/kardex/session/teacher/rectification_actas/get_all_campus_by_id_teacher
    public function get_all_campus_by_id_teacher() {
        $campus = $this->Rectification_actas_extra_model->get_all_campus_by_id_teacher($_SESSION['id_teacher'], anio(), ciclo(), $_SESSION['id_campus_carrer']);
        echo json_encode($campus);
    }

//funcion que pinta los años dependiendo del id del campus seleccionado y de la variable de sesion del profesor
//    http://localhost/kardex/session/teacher/rectification_actas/get_years_by_id_teacher?ID_campus_career=1
    public function get_years_by_id_teacher() {
        $years = $this->Rectification_actas_extra_model->get_years_by_id_teacher($_SESSION['id_teacher'], $this->input->get('ID_campus_career'), anio(), ciclo());
        echo json_encode($years);
    }

//funcion que pinta los años dependiendo del id del campus seleccionado y de la variable de sesion del profesor
//    http://localhost/kardex/session/teacher/rectification_actas/get_ciclo_by_id_teacher?ID_campus_career=1
    public function get_ciclo_by_id_teacher() {
        $years = $this->Rectification_actas_extra_model->get_ciclo_by_id_teacher($_SESSION['id_teacher'], $this->input->get('ID_campus_career'), $this->input->get('year'), anio(), ciclo());
        echo json_encode($years);
    }

    //funcion que pinta los typos de grupos dependiendo del campus,id del profesor  y año seleccionado el año es texto ej:"2020"
    //http://localhost/kardex/session/teacher/rectification_actas/get_all_type_group_by_id_teacher?ID_campus_career=1&year_active=2020
    public function get_all_type_group_by_id_teacher() {
        $type_groups = $this->Rectification_actas_extra_model->get_all_type_group_by_id_teacher($_SESSION['id_teacher'], $this->input->get('ID_campus_career'), $this->input->get('year_active'));
        echo json_encode($type_groups);
    }

    //funcion que pinta los grupos una vez seleccionado el campo:campus,año,tipo de grupo
//   http://localhost/kardex/session/teacher/rectification_actas/get_all_groups_by_id_teacher_id_group_year_and_id_type_group?campus_select=1&anio_valor=2020&modalidad_valor=1
    public function get_all_groups_by_id_teacher_id_group_year_and_id_type_group() {
        $type_groups = $this->Rectification_actas_extra_model->get_all_groups_by_id_techer_year_id_type_group(
                $_SESSION['id_teacher'], $this->input->get('campus_select'), $this->input->get('anio_valor'), $this->input->get('ciclo'), $this->input->get('modalidad_valor')
        );
        echo json_encode($type_groups);
    }

//    funcion que muestra la lista de alumnos dependiendo del grupo seleccionado
    public function get_all_students_by_id_group() {
        $students = $this->Rectification_actas_extra_model->get_all_students_by_id_group(anio(), ciclo(), $this->input->get('ID_teacher_by_group'), $_SESSION['id_teacher'], $_SESSION['id_campus_carrer']);
        return $students;
//         echo json_encode($students);
    }

    public function get_all_students_by_id_group_rectificate() {
        $students = $this->Rectification_actas_extra_model->get_all_students_by_id_group_rectificate(anio(), ciclo(), $this->input->get('ID_teacher_by_group'), $_SESSION['id_teacher'], $_SESSION['id_campus_carrer']);
       return ordenar_array_asc_by_elemento($students,"surnames");
//         echo json_encode($students);
    }

    public function calificar_grupo() {
//        $_SESSION['id_teacher_by_group'] = $this->input->get('ID_teacher_by_group');
        $teacher_by_group = $this->Rectification_actas_extra_model->get_teacher_by_group($this->input->get('ID_teacher_by_group'));
        if ($teacher_by_group->qualified == 0 && $teacher_by_group->rectificated == 1) {
            redirect('session/teacher/rectification_actas_extra');
        } else {
            $type_groups = $this->get_all_students_by_id_group();
            $data = array();
            $data = [
                'grupo' => $type_groups,
                'datos_grupo' => $this->Rectification_actas_extra_model->get_group($this->input->get('ID_teacher_by_group'), anio(), ciclo(), $_SESSION['id_teacher'])
            ];
            $data = array(
                'header' => $this->load->view('templates/session/teacher/head_view', '', TRUE),
                'aside' => $this->load->view('templates/session/teacher/aside_view', '', TRUE),
                'main' => $this->load->view('teacher/rectificar_extra_view', $data, TRUE),
                  'footer' => $this->load->view('templates/general/footer_view', '', TRUE),
                'scripts' => [
                    '<script  src="' . base_url('assets/registro/js/teacher/mostrar_pass.js') . '"></script>',
                    '<script  src="' . base_url('assets/registro/js/teacher/datos.js') . '"></script>',
                    '<script  src="' . base_url('assets/registro/js/teacher/rectificar.js"') . '"></script>',
                ]
            );
            $this->load->view('layout_general_view', $data);
        }
    }

    public function ver_grupo() {
//        $_SESSION['id_teacher_by_group'] = $this->input->get('ID_teacher_by_group');
        $type_groups = $this->get_all_students_by_id_group();
        $data = array();
        $data = [
            'grupo' => $type_groups,
            'datos_grupo' => $this->Rectification_actas_extra_model->get_group($this->input->get('ID_teacher_by_group'), anio(), ciclo(), $_SESSION['id_teacher'])
        ];
        $data = array(
            'header' => $this->load->view('templates/session/teacher/head_view', '', TRUE),
            'aside' => $this->load->view('templates/session/teacher/aside_view', '', TRUE),
            'main' => $this->load->view('teacher/ver_grupos_view', $data, TRUE),
              'footer' => $this->load->view('templates/general/footer_view', '', TRUE),
        );
        $this->load->view('layout_general_view', $data);
    }

    public function pdf_acta() {
        $type_groups = $this->get_all_students_by_id_group();
        $data = array();
        $data = [
            'grupo' => $type_groups,
            'datos_grupo' => $this->Rectification_actas_extra_model->get_group($this->input->get('ID_teacher_by_group'), anio(), ciclo(), $_SESSION['id_teacher'])
        ];
        $html = $this->load->view('teacher/pdf_acta_view', $data, TRUE);
        $filename = 'Acta';
        $this->pdfgenerator->generate($html, $filename, TRUE, 'Letter', 'portrait');
    }

    public function pdf_acta_rectificada() {



        $type_groups = $this->get_all_students_by_id_group_rectificate($this->input->get('ID_teacher_by_group'));
        $_SESSION['ID_teacher_by_group'] = $this->input->get('ID_teacher_by_group');
        $data = array();
        $data = [
            'grupo' => $type_groups,
            'datos_grupo' => $this->Rectification_actas_extra_model->get_group($this->input->get('ID_teacher_by_group'), anio(), ciclo(), $_SESSION['id_teacher'])
        ];
        $html = $this->load->view('teacher/pdf_acta_view', $data, TRUE);
        $filename = 'Acta';
        $this->pdfgenerator->generate($html, $filename, TRUE, 'Letter', 'portrait');
    }

    private function get_template($data) {


        $data = array(
            'header' => $this->load->view('templates/session/teacher/head_view', '', TRUE),
            'aside' => $this->load->view('templates/session/teacher/aside_view', '', TRUE),
            'main' => $this->load->view('teacher/rectificar_grupos_extra_view', $data, TRUE),
              'footer' => $this->load->view('templates/general/footer_view', '', TRUE),
            'scripts' => [
                '<script  src="' . base_url('assets/registro/js/teacher/rectificar_extras.js') . '"></script>',
            ]
        );

        $this->load->view('layout_general_view', $data);
    }

    public function update_calificacion() {
        $this->_validation_update_cali();
        $array3 = $this->input->post('rec_ID_student');
        $array2 = $this->input->post('rec_ID_teacher_by_group');
        $array1 = $this->input->post('rec_calificacion');
        $this->db->trans_start(); //inicia la transaccion 
        $this->Rectification_actas_extra_model->update_qualified($array2[0], $_SESSION['id_teacher']);
        foreach ($array1 as $key => $value) {
            $this->Rectification_actas_extra_model->update_score_student($array3[$key], $array2[$key], strtoupper($array1[$key]));
        }
//        $this->generar_qr_acta($this->input->post('rec_ID_teacher_by_group')[0]);
        $this->db->trans_complete(); //termina , en una sola transaccion para insertar varios registro o calcelar todo
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            $this->session->set_flashdata('error_ss', 'Ocurrio un error');
            redirect('session/teacher/rectification_actas_extra/calificar_grupo?ID_teacher_by_group=' . $this->input->get('ID_teacher_by_group'));
        } else {
            $this->db->trans_commit();
            $this->session->set_flashdata('exito', 'Se registro correctamente');
            redirect('session/teacher/rectification_actas_extra');
        }
    }

    public function generar_qr_acta($id_teacher_by_group) {

        $this->load->library('ciqrcode');

        $datos_grupo = $this->Rectification_actas_extra_model->get_qr($id_teacher_by_group, anio(), ciclo(), $_SESSION['id_teacher']);

        /*         * Aqui generamos y guardamos qr */
        $acta_id = $id_teacher_by_group;
        $datos_qr = 'Folio:' . $datos_grupo->qr . ' ' . 'No Rectificaciones:' . $datos_grupo->count_rectificated;
        //hacemos configuraciones
        $params['data'] = $datos_qr;
        $params['level'] = 'M';
        $params['size'] = 5;
        $micarpeta = 'assets/codigos_qr/';

        if (!file_exists($micarpeta)) {
            mkdir($micarpeta, 0777, true);
        }
        if (file_exists("assets/codigos_qr/acta_num_$acta_id.png")) {
            unlink("assets/codigos_qr/acta_num_$acta_id.png");
            $params['savename'] = FCPATH . "assets/codigos_qr/acta_num_$acta_id.png";
            $this->ciqrcode->generate($params);
        }

        //decimos el directorio a guardar el codigo qr, en este
        //generamos el código qr
    }

    private function _validation_update_cali() {
        $this->load->library('form_validation');
        $this->form_validation->set_data($this->input->post());
        $this->form_validation->set_rules('year_active', 'A&ntilde;po', 'trim|required');
        $this->form_validation->set_rules('cycle', 'Ciclo', 'trim|required');
        $this->form_validation->set_rules('short_name', 'Nombre abreviatura', 'trim|required');
        $this->form_validation->set_rules('group_name', 'Nombre curso', 'trim|required');
        $this->form_validation->set_rules('key_curse', 'Id curso', 'trim|required');
        $this->form_validation->set_rules('rec_ID_teacher_by_group[]', 'grupo', 'trim|required');
        $this->form_validation->set_rules('rec_ID_student[]', 'estudiante', 'trim|required');
        $this->form_validation->set_rules('rec_calificacion[]', 'calificación', 'trim|required|callback_regex_check_calificacion');
        $this->form_validation->set_rules('rec_calificacionvali[]', 'calificación confirmación', 'trim|required|callback_regex_check_calificacion');
        if (!$this->form_validation->run()) {
            $this->session->set_flashdata('error_ss', validation_errors());
            redirect('session/teacher/Rectification_actas_extra/calificar_grupo?ID_teacher_by_group=' . $this->input->get('ID_teacher_by_group'));
        }
    }

    public function regex_check_calificacion($str) {
        $regex_validacion_calificacion = regex_calificacion();

        if (preg_match($regex_validacion_calificacion, $str)) {
            return TRUE;
        } else {
            $this->form_validation->set_message('regex_check_calificacion', 'La %s  no es valida');
            return FALSE;
        }
    }

    public function ver_acta() {
        $type_groups = $this->get_all_students_by_id_group_rectificate($this->input->get('ID_teacher_by_group'));
        if ($type_groups) {
            $data = array();
            $data = [
                'grupo' => $type_groups,
                'datos_grupo' => $this->Rectification_actas_extra_model->get_group($this->input->get('ID_teacher_by_group'), anio(), ciclo(), $_SESSION['id_teacher'])
            ];
            $data = array(
                'header' => $this->load->view('templates/session/teacher/head_view', '', TRUE),
                'aside' => $this->load->view('templates/session/teacher/aside_view', '', TRUE),
                'main' => $this->load->view('teacher/ver_acta_recti_extra_view', $data, TRUE),
                  'footer' => $this->load->view('templates/general/footer_view', '', TRUE),
                'scripts' => [
                    '<script  src="' . base_url('assets/registro/js/teacher/datos.js') . '"></script>',
                    '<script  src="' . base_url('assets/registro/js/teacher/mostrar_pass.js') . '"></script>',
                ]
            );
            $this->load->view('layout_general_view', $data);
        } else {
            redirect('session/teacher/rectification_actas_extra');
        }
    }

}
