<?php

defined('BASEPATH') or exit('No direct script access allowded');

class Extras extends CI_Controller {

    public function __construct() {
        parent::__construct();
        checar_teacher_calificar_extra();
        checar_rol_teacher();
        $this->load->helper('text_calificacion');
        $library = array(
            'pdfgenerator',
            'DataListGeneral',
            'form_validation'
        );
        $this->load->library($library);
        $this->load->model(array('teacher/Extras_model'));
    }

    public function index() {
        $password_verificada = $this->Extras_model->get_count_update_password_by_user($_SESSION['id']);
        $data = array();
        if ($password_verificada->update_password == 0) {
            $this->get_template_update_pasword($data);
        } else {
            $data = array(
                'teacher' => json_decode(json_encode($this->Extras_model->get_teacher($_SESSION['id'])), true),
            );
            $this->get_template($data);
        }
    }

//funcion para comparar pass de firma

    public function get_fiel_teacher_firma() {
        $fiel = $this->Extras_model->get_teacher_firma($_SESSION['id_teacher']);
        echo json_encode($fiel);
    }

    //funcion para comparar pass de firma

    public function get_fiel_teacher() {
        $fiel = $this->Extras_model->get_teacher_firma($_SESSION['id_teacher']);
        echo json_encode($fiel);
    }

//funcion que pinta los campus con base al id_profesor
    public function get_all_campus_by_id_teacher() {
        $campus = $this->Extras_model->get_all_campus_by_id_teacher($_SESSION['id_teacher'], anio(), ciclo(), $_SESSION['id_campus_carrer']);
        echo json_encode($campus);
    }

//funcion que pinta los años dependiendo del id del campus seleccionado y de la variable de sesion del profesor
    public function get_years_by_id_teacher() {
        $years = $this->Extras_model->get_years_by_id_teacher($_SESSION['id_teacher'], $this->input->get('ID_campus_career'), anio(), ciclo());
        echo json_encode($years);
    }

    //funcion que pinta el ciclo dependiendo del id del campus seleccionado y de la variable de sesion del profesor
    public function get_cicle_by_id_teacher() {
        $years = $this->Extras_model->get_cycle_by_id_teacher($_SESSION['id_teacher'], $this->input->get('ID_campus_career'), $this->input->get('anio'), anio(), ciclo());
        echo json_encode($years);
    }

//funcion que pinta los typos de grupos dependiendo del campus,id del profesor  y año seleccionado el año es texto ej:"2020"
    public function get_all_type_group_by_id_teacher() {
        $type_groups = $this->Extras_model->get_all_type_group_by_id_teacher($_SESSION['id_teacher'], $this->input->get('ID_campus_career'), $this->input->get('year_active'), $this->input->get('ciclo'));
        echo json_encode($type_groups);
    }

    //funcion que pinta los grupos una vez seleccionado el campo:campus,año,tipo de grupo
    public function get_all_groups_by_id_teacher_id_group_year_and_id_type_group() {
        $type_groups = $this->Extras_model->get_all_groups_by_id_techer_year_id_type_group(
                $_SESSION['id_teacher'], $this->input->get('campus_select'), $this->input->get('anio_valor'), $this->input->get('ciclo'), $this->input->get('modalidad_valor')
        );
        echo json_encode($type_groups);
    }

//    funcion que muestra la lista de alumnos dependiendo del grupo seleccionado
    public function get_all_students_by_id_group() {
        $type_groups = $this->Extras_model->get_all_students_by_id_group(anio(), ciclo(), $this->input->get('ID_teacher_by_group'), $_SESSION['id_teacher'], $_SESSION['id_campus_carrer']);
        return ordenar_array_asc_by_elemento($type_groups,"surnames");
    }

    public function calificar_grupo() {
        $type_groups = $this->get_all_students_by_id_group();
        $teacher_by_group = $this->Extras_model->get_teacher_by_group($this->input->get('ID_teacher_by_group'));
        if ($teacher_by_group->qualified == '1') {

            redirect('session/teacher/extras');
        } else {
                
            $data = array();
            $data = [
                'grupo' => $type_groups,
                'datos_grupo' => $this->Extras_model->get_group($this->input->get('ID_teacher_by_group'), anio(), ciclo(), $_SESSION['id_teacher'])
            ];
            $data = array(
                'header' => $this->load->view('templates/session/teacher/head_view', '', TRUE),
                'aside' => $this->load->view('templates/session/teacher/aside_view', '', TRUE),
                'main' => $this->load->view('teacher/calificar_grupos_extras_view', $data, TRUE),
				'footer' => $this->load->view('templates/general/footer_view', '', TRUE),
                'scripts' => [
                    '<script  src="' . base_url('assets/registro/js/teacher/extras.js') . '"></script>',
                    '<script  src="' . base_url('assets/registro/js/teacher/datos.js') . '"></script>',
                    '<script  src="' . base_url('assets/registro/js/teacher/mostrar_pass.js') . '"></script>',
                ]
            );
            $this->load->view('layout_general_view', $data);
        }
    }

    public function ver_grupo() {
//        $this->input->get('ID_teacher_by_group') = $this->input->get('ID_teacher_by_group');
        $type_groups = $this->get_all_students_by_id_group();
        $data = array();
        $data = [
            'grupo' => $type_groups,
            'datos_grupo' => $this->Extras_model->get_group($this->input->get('ID_teacher_by_group'), anio(), ciclo(), $_SESSION['id_teacher'])
        ];
        $data = array(
            'header' => $this->load->view('templates/session/teacher/head_view', '', TRUE),
            'aside' => $this->load->view('templates/session/teacher/aside_view', '', TRUE),
            'main' => $this->load->view('teacher/ver_grupos_extras_view', $data, TRUE),
              'footer' => $this->load->view('templates/general/footer_view', '', TRUE),
        );
        $this->load->view('layout_general_view', $data);
    }

    public function pdf_acta() {
        $type_groups = $this->get_all_students_by_id_group();
        $data = array();
        $data = [
            'grupo' => $type_groups,
            'datos_grupo' => $this->Extras_model->get_group($this->input->get('ID_teacher_by_group'), anio(), ciclo(), $_SESSION['id_teacher'])
        ];
        $html = $this->load->view('teacher/pdf_acta_view', $data, TRUE);
        $filename = 'Acta';
        $this->pdfgenerator->generate($html, $filename, TRUE, 'Letter', 'portrait');
    }

    private function get_template($data) {
        $data = array(
            'header' => $this->load->view('templates/session/teacher/head_view', '', TRUE),
            'aside' => $this->load->view('templates/session/teacher/aside_view', '', TRUE),
            'main' => $this->load->view('teacher/extras_grupos_view', $data, TRUE),
              'footer' => $this->load->view('templates/general/footer_view', '', TRUE),
            'scripts' => [
                '<script  src="' . base_url('assets/registro/js/teacher/extras.js') . '"></script>',
                '<script  src="' . base_url('assets/registro/js/teacher/datos.js') . '"></script>',
            ]
        );

        $this->load->view('layout_general_view', $data);
    }

    private function get_template_update_pasword($data) {
        $data = array(
            'header' => $this->load->view('templates/session/teacher/head_view', '', TRUE),
            'aside' => $this->load->view('templates/session/teacher/aside_update_password', '', TRUE),
            'main' => $this->load->view('teacher/update_password', $data, TRUE),
              'footer' => $this->load->view('templates/general/footer_view', '', TRUE),
            'scripts' => [
                '<script  src="' . base_url('assets/registro/js/teacher/update_password.js') . '"></script>',
            ]
        );

        $this->load->view('layout_general_view', $data);
    }

    public function insert_cal() {

        $id_teacher_qr = $this->input->post('ID_teacher_by_group')[0];

        $this->_validation_insert_cali();
        $folio = $this->input->post('year_active') . '-' . $this->input->post('cycle') . '-' . $this->input->post('short_name') . '-' .
                $this->input->post('group_name') . '-' . $this->input->post('key_curse') . '-' . $this->input->post('ID_teacher_by_group')[0];
        $folio = str_replace(' ', '', $folio);
        $array3 = $this->input->post('ID_student');
        $array2 = $this->input->post('ID_teacher_by_group');
        $array1 = $this->input->post('calificacion');
        $this->db->trans_start(); //inicia la transaccion 
        $this->Extras_model->update_qualified($array2[0], $this->input->post('group_name'), $_SESSION['id_teacher']);
        foreach ($array1 as $key => $value) {
            $this->Extras_model->update_score_student($array3[$key], $array2[$key], strtoupper($array1[$key]));
        }
//        $this->generar_qr_acta($id_teacher_qr);
        $this->db->trans_complete(); //termina , en una sola transaccion para insertar varios registro o calcelar todo
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            $this->session->set_flashdata('error_ss', 'Ocurrio un error');
            redirect('session/teacher/extras/calificar_grupo?ID_teacher_by_group=' . $this->input->get('ID_teacher_by_group'));
//            return FALSE;
        } else {
            $this->db->trans_commit();
            $this->session->set_flashdata('exito', 'Se registro correctamente');
            redirect('session/teacher/extras');
        }
    }

    public function generar_qr_acta($id_teacher_by_group) {

        $this->load->library('ciqrcode');

        $datos_grupo = $this->Extras_model->get_qr($id_teacher_by_group, anio(), ciclo(), $_SESSION['id_teacher']);

        /*         * Aqui generamos y guardamos qr */
        $acta_id = $id_teacher_by_group;
        $datos_qr = 'Folio:' . $datos_grupo->qr . ' ' . 'No Rectificaciones:' . $datos_grupo->count_rectificated;
        //hacemos configuraciones
        $params['data'] = $datos_qr;
        $params['level'] = 'M';
        $params['size'] = 5;
        $micarpeta = 'assets/codigos_qr/';

        if (!file_exists($micarpeta)) {
            mkdir($micarpeta, 0777, true);
        }

        //decimos el directorio a guardar el codigo qr, en este
        $params['savename'] = FCPATH . "assets/codigos_qr/acta_num_$acta_id.png";
        //generamos el código qr
        $this->ciqrcode->generate($params);
    }

//------------jess agregue fucion para validar 
    private function _validation_insert_cali() {
        $this->load->library('form_validation');
        $this->form_validation->set_data($this->input->post());
        $this->form_validation->set_rules('year_active', 'A&ntilde;po', 'trim|required');
        $this->form_validation->set_rules('cycle', 'Ciclo', 'trim|required');
        $this->form_validation->set_rules('short_name', 'Nombre abreviatura', 'trim|required');
        $this->form_validation->set_rules('group_name', 'Nombre curso', 'trim|required');
        $this->form_validation->set_rules('key_curse', 'Id curso', 'trim|required');
        $this->form_validation->set_rules('ID_teacher_by_group[]', 'grupo', 'trim|required');
        $this->form_validation->set_rules('ID_student[]', 'estudiante', 'trim|required');
        $this->form_validation->set_rules('calificacion[]', 'calificación', 'trim|required|callback_regex_check_calificacion');
        $this->form_validation->set_rules('calificacionvali[]', 'calificación confirmación', 'trim|required|callback_regex_check_calificacion');
        if (!$this->form_validation->run()) {
            $this->session->set_flashdata('error_ss', validation_errors());
            redirect('session/teacher/extras/calificar_grupo?ID_teacher_by_group=' . $this->input->get('ID_teacher_by_group'));
        }
    }

    public function regex_check_calificacion($str) {
        $regex_validacion_calificacion = regex_calificacion();

        if (preg_match($regex_validacion_calificacion, $str)) {
            return TRUE;
        } else {
            $this->form_validation->set_message('regex_check_calificacion', 'La %s  no es valida');
            return FALSE;
        }
    }

    public function fei() {
        $adaptar_name = str_replace("Ñ", "&ntilde;", $_SESSION['name_user']);
        $nombre_archivo = $this->eliminar_acentos(str_replace(" ", "", $_SESSION['name_user']));
        $name_titulo = $this->eliminar_acentos($adaptar_name);
        $name_minusculas = strtolower($name_titulo);

        $data = array();
        $data = [
            'nombre_archivo' => $nombre_archivo,
            'name_prof' => $name_minusculas,
            'profe' => $this->Extras_model->get_teacher_firma($_SESSION['id_teacher']),
        ];
        $data = array(
            'header' => $this->load->view('templates/session/teacher/head_view', '', TRUE),
            'aside' => $this->load->view('templates/session/teacher/aside_view', '', TRUE),
            'main' => $this->load->view('teacher/fei_view', $data, TRUE),
              'footer' => $this->load->view('templates/general/footer_view', '', TRUE),
        );


        $this->load->view('layout_general_view', $data);
    }

    public function descargar_fielzip() {
        $descargas = $this->Extras_model->get_teacher_firma($_SESSION['id_teacher']);
        echo $descargas->descargas_fiel;
        if ($descargas->descargas_fiel < 2) {

            $this->Extras_model->update_descarga_fei($descargas->descargas_fiel + 1, $_SESSION['id_teacher']);
            $this->prueba($this->input->post('archivo'));
        } else {
            redirect('session/teacher/Teacher_dashboard/fei');
        }
    }

    public function prueba($url) {
        redirect($url);
    }

    function eliminar_acentos($cadena) {

        //Reemplazamos la A y a
        $cadena = str_replace(
                array('Á', 'À', 'Â', 'Ä', 'á', 'à', 'ä', 'â', 'ª'),
                array('A', 'A', 'A', 'A', 'a', 'a', 'a', 'a', 'a'),
                $cadena
        );

        //Reemplazamos la E y e
        $cadena = str_replace(
                array('É', 'È', 'Ê', 'Ë', 'é', 'è', 'ë', 'ê'),
                array('E', 'E', 'E', 'E', 'e', 'e', 'e', 'e'),
                $cadena);

        //Reemplazamos la I y i
        $cadena = str_replace(
                array('Í', 'Ì', 'Ï', 'Î', 'í', 'ì', 'ï', 'î'),
                array('I', 'I', 'I', 'I', 'i', 'i', 'i', 'i'),
                $cadena);

        //Reemplazamos la O y o
        $cadena = str_replace(
                array('Ó', 'Ò', 'Ö', 'Ô', 'ó', 'ò', 'ö', 'ô'),
                array('O', 'O', 'O', 'O', 'o', 'o', 'o', 'o'),
                $cadena);

        //Reemplazamos la U y u
        $cadena = str_replace(
                array('Ú', 'Ù', 'Û', 'Ü', 'ú', 'ù', 'ü', 'û'),
                array('U', 'U', 'U', 'U', 'u', 'u', 'u', 'u'),
                $cadena);

        //Reemplazamos la N, n, C y c
        $cadena = str_replace(
                array(/* 'Ñ', 'ñ', */ 'Ç', 'ç'),
                array(/* 'N', 'n', */ 'C', 'c'),
                $cadena
        );

        return $cadena;
    }

    public function update_password() {
        $_POST = json_decode(file_get_contents("php://input"), true); //NECESARIO para HACER EL USO   DEL METHOD POST
        $password = $_POST['password'];
//        $password_hash = str_replace("$2a$", "$2y$10$", $password);
        if ($this->_validation_update_password($password)) {
            $this->Extras_model->update_password($password, $_SESSION['id']);
            echo 1;
        } else {
            echo 0;
        }
    }

    public function _validation_update_password($password) {
        $regex_password = '/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&#.$($)$-$_])[A-Za-z\d$@$!%*?&#.$($)$-$_]{8}$/';
        if (preg_match($regex_password, $password)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}
