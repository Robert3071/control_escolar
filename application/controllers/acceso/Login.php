<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library(array('form_validation', 'session', 'user_agent'));
        $this->load->helper(array('login/login_rules','calendar'));
        $this->load->model('login/Login_access');
        $this->load->model('admin_jef_carr/Admin_jef_carr_model');
        $this->load->model('admin/Admin_queries');
        $this->load->model('teacher/Actas_model');
    }
    public function login_teacher() {
        $this->load->helper('session');
        checar_session();
        $data = array();
        $data = [
            'inicio_view' => $this->load->view('templates/login/login_teacher_view', $data, TRUE),
        ];

        $data = array(
            'header' => $this->load->view('templates/general/header_view', $data, TRUE),
            'main' => $this->load->view('templates/general/main_view', $data, TRUE),
            'footer' => $this->load->view('templates/general/footer_view', $data, TRUE),
            'scripts' => [
                '<script  src="' . base_url('assets/registro/js/teacher/update_password.js') . '"></script>',
            ]
        );
        $this->load->view('layout_general_view', $data);
        //$this->load->view('login_view');
    }

    public function login_admin() {
        $this->load->helper('session');
        checar_session();
        $data = array();
        $data = [
            'inicio_view' => $this->load->view('templates/login/login_admin_view', $data, TRUE),
        ];

        $data = array(
            'header' => $this->load->view('templates/general/header_view', $data, TRUE),
            'main' => $this->load->view('templates/general/main_view', $data, TRUE),
            'footer' => $this->load->view('templates/general/footer_view', $data, TRUE)
        );
        $this->load->view('layout_general_view', $data);
        //$this->load->view('login_view');
    }

    public function login_jefe_carrera() {
        $this->load->helper('session');
        checar_session();
        $data = array();
        $data = [
            'inicio_view' => $this->load->view('templates/login/login_admin_jef_carr_view', $data, TRUE),
        ];

        $data = array(
            'header' => $this->load->view('templates/general/header_view', $data, TRUE),
            'main' => $this->load->view('templates/general/main_view', $data, TRUE),
            'footer' => $this->load->view('templates/general/footer_view', $data, TRUE)
        );
        $this->load->view('layout_general_view', $data);
    }



    public function validate_teacher() {
        $usr = $this->input->post('email');
        $pass = $this->input->post('password');
        $data = array(
            'email_ua' => $usr,
            'password_ua' => $pass,
        );
        $res = $this->Login_access->login_teacher($data);

        /**
         * Aqui se valida que exista el correo en la DB
         */
        if (!$res) {
            $this->session->set_flashdata('error_ss', 'Usuario no encontrado');
            redirect('acceso/login/login_teacher');
        }

        if ($res->status == '0' || $res->status == 0) {
            $this->session->set_flashdata('error_ss', 'Usuario Bloqueado consulta al Administrador');
            redirect('acceso/login/login_teacher');
        }

        if (!password_verify($data['password_ua'], $res->password)) {
            echo 'cotraseña incorrecta';
            $this->session->set_flashdata('error_ss', 'Contraseña incorrecta');
            redirect('acceso/login/login_teacher');
        }

        /**
         * Aqui se valida que la contraseña sea correcta
         * si es así entonces se establece la sesión y las
         * variables de sesión
         */
        //var_dump($res);
        if (password_verify($data['password_ua'], $res->password)) {

            $info_profesor = $this->Actas_model->get_teacher($res->ID_user);

            $session = array(
                'id' => $res->ID_user,
                'id_type' => $res->ID_type_user,
                'name_user' => $res->name_user,
                'email' => $res->email,
                'status' => $res->status,
                'logueado' => TRUE,
                'id_campus_carrer' =>0,
                'id_teacher' => $info_profesor->ID_teacher,
                'name_user' => $info_profesor->name . ' ' . $info_profesor->surnames,
                'update_password' => $res->update_password
            );
            $this->session->set_userdata($session); // cargar los datos de sesión
            // esto era por si el usuario esta activo o inactivo


            if ($res->ID_type_user == 1) {
                redirect('session/teacher/Teacher_dashboard/');
            } else {
                redirect('acceso/login/login_teacher');
            }
        } else {
            redirect('acceso/login/login_teacher');
        }
    }

    public function validate_admin() {
        $usr = $this->input->post('email');
        $pass = $this->input->post('password');
        $data = array(
            'email_ua' => $usr,
            'password_ua' => $pass,
        );
        $res = $this->Login_access->login_admin($data);

        /**
         * Aqui se valida que exista el correo en la DB
         */
        if (!$res) {
            $this->session->set_flashdata('error_ss', 'Usuario no encontrado');
            redirect('acceso/login/login_admin');
        }
        /* esto era por si el usuario esta activo o inactivo
          if ($res->status_ua != 1) {
          $this->session->set_flashdata('error_ss', 'Usuario Bloqueado consulta al Administrador');
          redirect('login');
          }
         */
        if (!password_verify($data['password_ua'], $res->password)) {
            echo 'cotraseña incorrecta';
            $this->session->set_flashdata('error_ss', 'Contraseña incorrecta');
            redirect('acceso/login/login_admin');
        }

        /**
         * Aqui se valida que la contraseña sea correcta
         * si es así entonces se establece la sesión y las
         * variables de sesión
         */
        //var_dump($res);
        if (password_verify($data['password_ua'], $res->password)) {
            $info_admin = $this->Admin_queries->get_info_admin($res->ID_user);

            $session = array(
                'id' => $res->ID_user,
                'id_type' => $res->ID_type_user,
                'name_user' => $res->name_user,
                'email' => $res->email,
                'status' => $res->status,
                'id_campus' => $info_admin->ID_campus,
                'logueado' => TRUE
            );
            $this->session->set_userdata($session); // cargar los datos de sesión
            if ($res->ID_type_user == 3 || $res->ID_type_user == 5) {
//                print_r($_SESSION);
                redirect('session/admin/Admin_dashboard/grupos');
            } else {
                redirect('acceso/login/login_admin');
            }
        } else {
            redirect('acceso/login/login_admin');
        }
    }

    public function validate_admin_jefe_carrera() {
        $usr = $this->input->post('email');
        $pass = $this->input->post('password');
        $data = array(
            'email_ua' => $usr,
            'password_ua' => $pass,
        );
        $res = $this->Login_access->login_admin_jef_carr($data);

        if (!$res) {
            $this->session->set_flashdata('error_ss', 'Usuario no encontrado');
            redirect('acceso/login/login_jefe_carrera');
        }

        if (!password_verify($data['password_ua'], $res->password)) {
            echo 'cotraseña incorrecta';
            $this->session->set_flashdata('error_ss', 'Contraseña incorrecta');
            redirect('acceso/login/login_jefe_carrera');
        }

        if (password_verify($data['password_ua'], $res->password)) {
            if ($res->ID_type_user == 4) {
                $info_admin = $this->Admin_jef_carr_model->get_info_admin($res->ID_user);
                $session = array(
                    'id' => $res->ID_user,
                    'email' => $res->email,
                    'id_type' => $res->ID_type_user,
                    'logueado' => TRUE,
                    'id_campus_carrer' => $info_admin->ID_campus_carrer
                );
                $this->session->set_userdata($session); // cargar los datos de sesión
                redirect('session/admin_jef_carr/Admin_dashboard');
            }
        } else {
            redirect('acceso/login/login_jefe_carrera');
        }
    }

    public function logout() {       //cerrar sesión
        session_start();
        $vars = array('id', 'id_type', 'name_user', 'email', 'status', 'logueado');
        $this->session->unset_userdata($vars);
        $this->session->sess_destroy();
        session_unset();
        session_destroy();
        redirect('acceso/Login');
    }

    public function logout_teacher() {       //cerrar sesión
        session_start();
        $vars = array('id', 'id_type', 'name_user', 'email', 'status', 'logueado');
        $this->session->unset_userdata($vars);
        $this->session->sess_destroy();
        session_unset();
        session_destroy();
        redirect('acceso/Login/login_teacher');
    }

    public function logout_admin() {       //cerrar sesión
        session_start();
        $vars = array('id', 'id_type', 'name_user', 'email', 'status', 'logueado');
        $this->session->unset_userdata($vars);
        $this->session->sess_destroy();
        session_unset();
        session_destroy();
        redirect('acceso/Login/login_admin');
    }

    public function logout_admin_jef_carr() {       //cerrar sesión
        session_start();
        $vars = array('id', 'id_type', 'email', 'id_campus_carrer', 'logueado');
        $this->session->unset_userdata($vars);
        $this->session->sess_destroy();
        session_unset();
        session_destroy();
        redirect('acceso/Login/login_jefe_carrera');
    }

    public function calendario() {
        $data = array();
        $data = [
            'inicio_view' => $this->load->view('templates/general/calendarios/calendario4_view', $data, TRUE),
        ];

        $data = array(
            'header' => $this->load->view('templates/general/header_view', $data, TRUE),
            'main' => $this->load->view('templates/general/main_view', $data, TRUE),
            'footer' => $this->load->view('templates/general/footer_view', $data, TRUE)
        );
        $this->load->view('layout_general_view', $data);
    }

    public function olvidao_pass() {
        $data = array();
        $data = [
        ];

        $data = array(
            'header' => $this->load->view('templates/general/header_view', $data, TRUE),
            'main' => $this->load->view('templates/session/teacher/olvido_pass_view', $data, TRUE),
            'footer' => $this->load->view('templates/general/footer_view', $data, TRUE),
        );
        $this->load->view('layout_general_view', $data);
    }

    public function olvidao_pass_change() {
        $res = $this->Login_access->datos_prof_pass($this->input->get('correo'));
        if ($res) {
            if($res->update_password!=0){

                $data = array();
                $data = [
                    'usuario' => $res
                ];
                
                $_SESSION['email']=$res->email;
                $data = array(
                    'header' => $this->load->view('templates/general/header_view', $data, TRUE),
                    'main' => $this->load->view('teacher/olvido_pass_change_view', $data, TRUE),
                    'footer' => $this->load->view('templates/general/footer_view', $data, TRUE),
                    'scripts' => [
                        '<script  src="' . base_url('assets/registro/js/teacher/forget_password.js') . '"></script>',
                    ]
                );
                $this->load->view('layout_general_view', $data);
            }else{
                $this->session->set_flashdata('error_cambio_pass', 'Tiene que cambiar la contraseña, la primera ocasión que ingresa.');
                redirect('acceso/login/olvidao_pass');    
            }
        } else {
            $this->session->set_flashdata('error_cambio_pass', 'Usuario no encontrado');
            redirect('acceso/login/olvidao_pass');
        }
    }

    public function sendEmail($datos) {
        $this->load->library('email');
        $this->email->from('mesadeayuda.cvirc@sectei.cdmx.gob.mx');
        $this->email->to($datos['correo']);
        $this->email->subject('Recuperación de contraseña');
        $mensaje = $this->load->view('teacher/email_view', $datos, TRUE);
        $this->email->message($mensaje);
        $this->email->send();
    }

    public function update_pass() {
        $_POST = json_decode(file_get_contents("php://input"), true); //NECESARIO para HACER EL USO   DEL METHOD POST
        if ($this->_validation_update_password($_POST['password'])) {
            $pass = password_hash($_POST['password'], PASSWORD_DEFAULT);
            if ($this->Login_access->update_pass($pass, $_POST['user'])) {
                $datos = [
                    'correo' => $_SESSION['email'],
                    'user' => $_POST['user'],
                ];
                $this->sendEmail($datos);
                echo 1;
            } else {
                echo 0;
            }
        } else {
            echo 0;
        }
    }

    public function activar_user() {
        echo $this->input->get('id_user');

        if ($this->Login_access->update_status($this->input->get('id_user'))) {
            $this->session->set_flashdata('exito_status', 'Se activo la cuenta correctamente');
            redirect('acceso/Login/login_teacher');
        } else {
            echo 'error';
        }
    }

    public function _validation_update_password($password) {
        $regex_password = '/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&#.$($)$-$_])[A-Za-z\d$@$!%*?&#.$($)$-$_]{8}$/';
        if (preg_match($regex_password, $password)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}
