<div class="content-wrapper" id='app'>
    <div class="container mb-3">
        <div class="row col-md-12">
            <h1 class="text-center alert titulo_historial">Grupos</h1>
        </div>
        <div class="row" style="border: 1px solid #fff;">
            <div class="col-md-12">
                
                    <div class="col-12   text-center">
                        <p class="text-secondary mt-5 titulo_mobile titulo_mobile m-0">Secretaria de Educación, Ciencia,
                            Tecnología e Innovación de la Ciudad de México</p>
                        <p class="text-secondary titulo_mobile m-0">Instituto de Estudios Superiores de la Ciudad de México</p>
                        <p class="text-secondary titulo_mobile m-0">“Rosario Castellanos” </p>
                    </div>
                </div>
            </div>
        </div> 
        <?php if (count($grupo) > 0) { ?>
            
            <div class="mt-5">
                <strong>
                <div class="row col-md-12">
                    <div class="col-md-3">
                        <p>A&ntilde;o: <?php echo (isset($datos_grupo[0]['year_active'])) ? $datos_grupo[0]['year_active'] : '';  ?></p>
                        <p>Ciclo: <?php echo (isset($datos_grupo[0]['cycle'])) ? $datos_grupo[0]['cycle'] : '';  ?></p>
                    </div>
                    
                    <div class="col-md-6">
                        <p>Docente: <?php echo (isset($_SESSION['name_user'])) ? $_SESSION['name_user'] : '';  ?></p>
                        <p>Grupo: <?php echo (isset($datos_grupo[0]['group_name'])) ? $datos_grupo[0]['group_name'] : '';  ?></p>
                        <p>Asignatura: <?php echo (isset($datos_grupo[0]['subject_name'])) ? $datos_grupo[0]['subject_name'] : '';  ?></p>
                    </div>
                    </strong>
                </div>
                <div class="row ">
                    <div class="col-md-12 col-lg-12 col-sm-10" width="100%">
                        <form v-on:submit="onSubmit_califiaciones(<?php echo count($grupo) ?>)" action="<?php echo base_url() . 'session/teacher/extras/insert_cal' ?>" method="post">
                            <input name="year_active" type="hidden" value="<?php echo (isset($datos_grupo[0]['year_active'])) ? $datos_grupo[0]['year_active'] : '';  ?> ">
                            <input name="cycle" type="hidden" value="<?php echo $datos_grupo[0]['cycle']; ?> ">
                            <input name="short_name" type="hidden" value="<?php echo $datos_grupo[0]['short_name']; ?> ">
                            <input name="group_name" type="hidden" value="<?php echo $datos_grupo[0]['group_name']; ?> ">
                            <input name="key_curse" type="hidden" value="<?php echo $datos_grupo[0]['key_curse']; ?> ">
                            <table class="table table-responsive-sm table-bordered p-3">
                                <thead>
                                    <tr class="tabla_profes_calis">
                                        <th>No</th>
                                        <th>Matr&iacute;cula estudiante</th>
                                        <th>Nombre del estudiante</th>
                                        <th>Calificaci&oacute;n</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $sub = 0; ?>
                                    <?php foreach ($grupo as $key => $g) : ?>
                                        <tr>
                                            <td><?php echo ($key + 1) ?></td>
                                            <td><?php echo $g['accountNumber'] ?></td>
                                            <td><?php echo $g['names'] . ' ' . $g['surnames'] ?></td>
                                            <td>

                                                <input v-bind:style="validate_calificacion(<?php echo $sub; ?>) ?{border: ['1px solid #00b140']} :{border: ['4px solid #dc3545']} " type="text" class="form-control m-2" name="calificacion[]" v-model="calificacion[<?php echo $sub; ?>]" placeholder="Ingrese la calificaci&oacute;n">
                                                <input v-bind:style="validate_calificacion(<?php echo $sub; ?>) ?{border: ['1px solid #00b140']} :{border: ['4px solid #dc3545']} " type="text" class="form-control m-2" name="calificacionvali[]" v-model="calificacionvali[<?php echo $sub; ?>]" placeholder="Confirme la calificaci&oacute;n">
                                                <!--                                    <p v-show="!validate_calificacion(<?php echo $sub; ?>)"  v-bind:style="{color: ['#cc3300']}"><i class="fas fa-exclamation-circle"></i></p>
                                                <p v-show="validate_calificacion(<?php echo $sub; ?>)"  v-bind:style="{color: ['#33cc33']}"><i class="fas fa-check-circle"></i></p>-->
                                                <input name="ID_teacher_by_group[]" type="hidden" value="<?php echo $g['ID_teacher_by_group']; ?> ">
                                                <input name="ID_student[]" type="hidden" value="<?php echo $g['ID_student']; ?>">
                                            </td>
                                        </tr>
                                        <?php $sub++; ?>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                            <div id="prueba_axios" class="boton_pdf">
                            </div>
                        </form>
                    </div>
                    <div id="firma" class="card col-lg-10 col-md-10  col-sm-8 offset-sm-1 offset-lg-1 offset-md-1">
                        <div class="card-header tabla_profes_calis">
                            <h3 class="text-center">Por favor v&aacute;lide su firma electr&oacute;nica para poder calificar</h3>
                        </div>
                        <div class="card-body row">
                            <div class="form-group col-md-5 col-lg-5 col-sm-5  rounded">
                                <label for="archivo">Seleccionar archivo</label>
                                <input  type="file" @change="loadTextFromFile">
                            </div>
                            <div class="form-group col-md-7 col-lg-7 col-sm-7">
                                <label for="pass">Contrase&ntilde;a:</label>
                                <div class="input-group">
                                    <input v-model="pass_user" ID="txtPassword" type="Password" Class="form-control">
                                    <div class="input-group-append">
                                        <button id="show_password" class="btn boton_ver" type="button" onclick="mostrarPassword()"> <span class="fa fa-eye-slash icon"></span> </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer row">
                            <div class="col-6 offset-5">
                                <button @click.prevent="compara_pass" class="btn font-weight-bold text-white naranja">Continuar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>
<div id="prueba_axios2" class="boton_pdf">
</div>
<?php if ($this->session->flashdata('error_ss')) : ?>
    <span>
        <div class="alert alert-danger mt-2" role="alert"><?php echo $this->session->flashdata('error_ss'); ?></div>
    </span>
<?php endif; ?>
<?php if ($this->session->flashdata('exito')) : ?>
    <span>
        <div class="alert alert-success mt-2" role="alert"><?php echo $this->session->flashdata('exito'); ?></div>
    </span>
    <?php


     endif; 
