<div class="content-wrapper">
    <div class="container">
        <div class="row col-md-12">
            <h1 class="text-center alert titulo_historial">Firma Digital Institucional</h1>
        </div>
        
        <div class="col-12 letra text-justify">
            <p class="m-4">
                Apreciable docente.
                
            </p>

            <p class="m-4">
            En este apartado deber&aacute; descargar el archivo txt para poder saber la contraseña de su firma, la cual ser&aacute; necesaria para poder calificar, tambi&eacute;n debe descargar su archivo .irc Firma Digital Institucional (FDI).


            </p>
            <p class="m-4">
                A continuaci&oacute;n presione los botones para descargar ambos archivos.
            </p>
            
                <div id="app" class="container bootstrap snippets bootdey alinear_flex justify-content-center ">

                    <a href="<?php echo base_url('session/teacher/Teacher_dashboard/descarga_orientaciones') ?>" type="submit" class="btn naranja olvidar_pass m-4"  target="_blank" rel="noopener noreferrer">Descargar txt</a>
                    
                    <?php if($descargas[0]['descargas_fiel']<2): ?>
                        <a  href="<?php echo base_url('session/teacher/Teacher_dashboard/descargar_fdi') ?>" type="submit"  class="btn naranja olvidar_pass m-4"   rel="noopener noreferrer">Descargar .irc</a>
                    
                        
                        <?php else: ?>
                        <div class="alert alert-info" role="alert">
                            Usted ha excedido el número máximo de descargas.
                        </div>
                    <?php endif; ?>
                </div>


            
        </div>
    </div>
</div>
            

