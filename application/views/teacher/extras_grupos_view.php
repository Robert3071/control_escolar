<div class="content-wrapper" id="app">
    <div class="container mb-3">
        <div class="row col-md-12">
            <h1 class="text-center alert titulo_historial">Grupos</h1>
        </div>
        <div class="row" style="border: 1px solid #fff;">
            <div class="col-md-12">
                <div class="row">
                    
                    <div class="col-12   text-center">
                        <p class="text-secondary mt-5 titulo_mobile titulo_mobile m-0">Secretaria de Educación, Ciencia,
                            Tecnología e Innovación de la Ciudad de México</p>
                        <p class="text-secondary titulo_mobile m-0">Instituto de Estudios Superiores de la Ciudad de México</p>
                        <p class="text-secondary titulo_mobile m-0">“Rosario Castellanos” </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <form v-on:submit.prevent="submitForm">
            <div class="form-row">
                <div class="form-group col-md-3">
                    <label for="inputCity">Unidad Académica</label>
                    <select class="form-control" name="cars" id="cars" @change="campus2(campus_select)" v-model="campus_select">
                        <option v-for="campo in campus" v-bind:value="campo.ID_campus_career">{{campo.campus_name}}-{{ campo.career_name}}</option>
                    </select>
                </div>
                <div v-if="years!=2" class="form-group col-md-2">
                    <label for="inputState">A&ntilde;o:</label>
                    <select class="form-control" name="cars" id="cars" @click="traer_ciclo2" @change="traer_ciclo(anio_valor)" v-model="anio_valor">
                        <option v-for="year in years" v-bind:value="year.year_active">{{year.year_active}}</option>
                    </select>
                </div>
                <div v-if="ciclo!=2" class="form-group col-md-3">
                    <label for="inputState">Ciclo:</label>
                    <select class="form-control" name="cars" id="cars" @click="traer_modalidad2" @change="traer_modalidad(ciclo_valor)" v-model="ciclo_valor">
                        <option v-for="ciclo in cycle_axios" v-bind:value="ciclo.cycle">{{ciclo.cycle}}</option>
                    </select>
                </div>
                <div v-if="ver_modalidad==1" class="form-group col-md-2">
                    <label>Tipo:</label>
                    <select class="form-control" name="cars" id="cars" @change="poner_modalidad(modalidad_valor)" v-model="modalidad_valor">
                        <option v-for="modality in modalidad" v-bind:value="modality.ID_type_group">{{modality.type_group}}</option>
                    </select>
                </div>
                <div v-if="ver_modalidad==1" class="form-group col-md-2 my-auto">
                    <button @click="submitForm" type="submit" class="btn text-white naranja">Buscar</button>
                </div>
            </div>
        </form>
        <div class="col-lg-12 col-md-12">
            <table class="table table-responsive-sm table-striped " width="100%">
                <thead>
                    <tr class="tabla_profes_calis">
                        <th>Clave de grupo</th>
                        <th>Clave asignatura</th>
                        <th>Asignatura</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody class="text-wrap">
                    <tr v-for='group in groups' class="text-wrap"> 
                        <td>
                            {{group.group_name}}
                        </td>
                        <td >
                            {{group.key_curse}}
                        </td>
                        <td>
                            {{group.subject_name}}
                        </td>
                        <td>
						<div v-if="group.qualified==0">
                                <form method="GET" action="<?php echo base_url() . 'session/teacher/extras/calificar_grupo' ?>">
                                    <input name="ID_teacher_by_group" type="hidden" v-model="group.ID_teacher_by_group">
                                    <input class="btn btn-success mt-2" type="submit" value="Calificar">
                                </form>
                            </div>
                            <div v-if="group.qualified==1">
                                <form method="GET" action="<?php echo base_url() . 'session/teacher/extras/pdf_acta' ?>">
                                    <input name="ID_teacher_by_group" type="hidden" v-model="group.ID_teacher_by_group">
                                    <input class="btn btn-info mt-2" type="submit" value="Descargar Acta">
                                </form>
                            </div>
                            <div v-if="group.qualified==1">
                                <form method="GET" action="<?php echo base_url() . 'session/teacher/extras/ver_grupo' ?>">
                                    <input name="ID_teacher_by_group" type="hidden" v-model="group.ID_teacher_by_group">
                                    <input class="btn btn-success mt-2 float-left" type="submit" value="Consultar Acta">
                                </form>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
            <?php if ($this->session->flashdata('exito')) : ?>
                <script>
                    window.onload = function() {

                        Swal.fire({
                            title: 'Se registr&oacute; correctamente el acta.',
                            icon: 'success',
                            confirmButtonText: 'Continuar',
                            //showCancelButton: true,
                            showCloseButton: true,
                            width: 400
                        });
                    };
                </script>
            <?php endif; ?>
        </div>
    </div>
</div>


