<div class="content-wrapper">
    <div class="container">
        <div class="row col-md-12">
            <h1 class="text-center alert titulo_historial">Firma Digital Institucional</h1>
        </div>
        
        
        <div class="col-12 letra text-justify">
            <p class="m-4">
                Apreciable docente
                <span class="capitalize"><?php echo $name_prof; ?>.</span><br>
                
            </p>

            <p class="m-4">
            En el Instituto de Estudios Superiores de la Ciudad de México “Rosario Castellanos”, nos
            preparamos para la transición digital en los procesos académicos administrativos, como
            parte de esta nueva dimensión de trabajo nos permitimos hacerle llegar su Firma
            Digital Institucional (FDI) que utilizará para la firma de actas académicas del ciclo <?php echo $ciclo; ?>.


            </p>
            <p class="m-4">
                Le solicitamos la tenga a buen resguardo y haga uso de ella únicamente para los fines 
                institucionales solicitados.
            </p>
            <p class="m-4">
                Agradecemos su compromiso y apoyo para que el trámite de registro y firma de actas, se 
                realice conforme a las fechas establecidas en el calendario institucional.
            </p>

            <div class="container bootstrap snippets bootdey alinear_flex justify-content-center ">                
                    
                    <div class="mosaico_fei" >
                    <div class="details">
                                <div class="img-fluid img_qr_ime mb-5" alt="Responsive image">
                                    <img src="<?php echo base_url('assets/img/qr_Ime.png');?>" alt="">
                                </div>
                                <hr class="hr_fiel">
                                <h5 class="">ATENTAMENTE</h5>
                                <P>Imelda Berenice González Juárez<br>
                                Coordinación de Soporte
                                </P>
                                
                            
                    </div>
                </div>
                <div class="container bootstrap snippets bootdey alinear_flex justify-content-center ">

                    <a href="<?php echo base_url('session/teacher/Teacher_dashboard/pdf_fei') ?>" type="submit" class="btn btn-info m-4"  target="_blank" rel="noopener noreferrer">Descargar</a>
                    <a href="<?php echo base_url('session/teacher/Teacher_dashboard/descargar_firma') ?>"  class="btn naranja olvidar_pass m-4"  rel="noopener noreferrer">Continuar</a>
                </div>

                

                

            
        </div>
    </div>
</div>
            

<script>
    window.addEventListener("load", function(event) {
     

        Swal.fire({
                                title: 'Atención.',
                                icon: false,
                                html: '<p class="text-justify">Cada uno de los procesos de registro de calificaciones o rectificación de calificaciones, requerirá del uso de la Firma Digital Institucional (FDI), por ello, como primer paso es necesario que realicé la descarga de la documentación asociada:</P> <p class="text-justify">De clic en el botón “descargar” y en el botón “continuar”, en cada una de las pantallas para poder descargar los siguientes tres documentos:</p> <p class="text-justify">1. Firma Digital Institucional</p><p <p class="text-justify">2. Carta de entrega FDI</p><p <p class="text-justify">3. Orientaciones de la contraseña FDI</li></ul>',
                                confirmButtonText: 'Entendido',
                                //showCancelButton: true,
                                cancelButtonText: 'Cancelar',
                                showCloseButton: true,
                                width: 400
                            });
    
        });

</script>
