<!DOCTYPE html>
<html>
    <head>
        <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
        <meta content="width=device-width" name="viewport">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>IRC</title>
        <style type="text/css">

            img {
                outline:none;
                text-decoration:none;
                -ms-interpolation-mode: bicubic;
                display:block;
                margin: auto;
            }
            a img {
                border:none;
            }
            p.header_link {
                margin: 1em 0;
            }
            table td {
                border-collapse: collapse;
                text-align: center;
            }
            table {
                border-collapse: collapse !important;
                mso-table-lspace: 0pt !important;
                mso-table-rspace: 0pt !important;
                margin-bottom: 20px;
            }
            .titulo{font-family: 'Alegreya Sans', sans-serif;  font-weight: 400;  letter-spacing: 0.04em;  line-height: 1.15385; font-size: 14px; text-transform: uppercase;}
            a.header_link {
                color: #999;
                text-decoration:none;

            }
            a.header_link:hover {
                color: #000000;
            }
            a.footer_link {
                color: #999;
                text-decoration:none;
                font:normal 14px lato, helvetica, arial;
            }
            a.footer_link:hover {
                color: #000000;
            }
            .header{
                background: #00b140;
            }
            .footer{
                /*padding: 5px;*/
                background: #465973;
                color: white;
            }
            .footer-bx {
                font:normal 14px/100% helvetica, arial;
                text-transform:uppercase;
                text-decoration: none !important;
                margin:0;
                float:left;
                padding:10px 20px 0 5px;
            }
            .legal-footer strong {
                text-transform:uppercase;
            }
            p{
                font: 19px  helvetica, arial; 
                color:#666; 
                text-align:left; 
                /*line-height:13px;*/
            }
            .align-left-vote{text-align: left!important; padding-left:30px; }
            .align-right-vote{text-align: right!important; padding-right:30px; }
            .img-vote{float:left; padding-left:140px; width:15px!important;}
            .titulo-vote{position:relative; z-index:99; top: -72px; left: 5px; font:bold 12px lato, helvetica, arial!important; color: #FE2E2E;}
            .span-vote{background-color: #F6CECE; padding: 4px; }
            .btn-shop-now img{margin:0 0 0 10px;}
            .titulo-stock {font-family: 'Alegreya Sans', sans-serif;  font-weight: 400;  letter-spacing: 0.04em;  line-height: 1.15385; font-size: 14px; text-transform: uppercase;}
            .titulo {font-family: 'Alegreya Sans', sans-serif;  font-weight: 400;  letter-spacing: 0.04em;  line-height: 1.15385; font-size: 14px; text-transform: uppercase;}

            .titulo-vote4{position:relative; z-index:99; top: -108px; left: 5px; font:bold 12px lato, helvetica, arial!important; color: #FE2E2E;}
            @media only screen and (max-width: 640px) {
                /* basic */
                table[class~=wr_layout], table[class~=frc] { width: 320px !important;}
                .altura-mobile{ height: 80px!important; }
                .mob-only {width: 100% !important;height: auto !important;overflow: visible !important;display: block !important;}
                .mob-only2 {overflow: visible !important;display: block !important;}
                .mob-only2-txt {padding:0 0px !important;}
                .mob-only3 {width: 100% !important;height: auto !important;overflow: visible !important;display: block !important; font:normal 11px lato, helvetica, arial!important; margin: 8px 0 5px 0!important; line-height: 1.2!important}
                /* Table Responsive */
                *[class~=simob] {width: 320px !important;}
                td[class~=simob-left] {width: 320px !important;float:left!important;}
                /* Hide  */
                *[class~=nomob] {display: none!important;}
                td[class~=rdn] {display: none !important; margin: 0 !important;padding: 0 !important;}
                /* zzz  */
                table[class~=def] {padding-bottom: 15px !important;}
                .img-vote{float:left; padding-left:115px; width:15px!important;}
                .titulo-vote{position:relative; z-index:99; top: -72px; left: 20px; font:bold 12px lato, helvetica, arial!important; color: #FE2E2E;}
                .btn-shop-now img{margin:0 0 0 10px;}
                .titulo{font-family: 'Alegreya Sans', sans-serif;  font-weight: 400;  letter-spacing: 0.04em;  line-height: 1.15385; font-size: 12px; text-transform: uppercase;}
                .titulo-vote4{position:relative; z-index:99; top: -108px; left: 20px; font:bold 12px lato, helvetica, arial!important; color: #FE2E2E;}
                .titulo-stock {font-family: 'Alegreya Sans', sans-serif;  font-weight: 400;  letter-spacing: 0.04em;  line-height: 1.15385; font-size: 12px; text-transform: uppercase;}
            }
        </style>
    </head>
    <body bgcolor="#FFFFFF" style="zoom: 100%;">
        <table class="wr_layout" align="center" width="700" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <table class="frc" align="center" width="695" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td class="header"> 
                                <img src="http://189.240.71.205/registro/actas/assets/img/sectei_logo.png" height="auto" width="500px";
                                     class="mob-only" />
                            </td>
                        </tr>
                    </table>
                    <table class="frc def" border="0"  align="center" cellspacing="0" cellpadding="0" style="mso-hide: all;">
                        <tr>
                            <td style="border-top:0px;">
                                <p class="mob-only2-txt" style="text-align:center; line-height:13px; padding:0 75px; text-decoration: none; margin-top:25px;"><strong style="text-transform:uppercase">
                                        Cambio de contrase&ntilde;a</strong><br><br>
                                </p>
                                <p class="mob-only2-txt"  
                                   ><strong>
                                        Apreciado docente:</strong>
                                </p>
                                <p class="mob-only2-txt">
                                    Le informamos que se realiz&oacute; el cambio de contrase&ntilde;a en su usuario, si usted no llev&oacute; a cabo este cambio,
                                    consulte con el jefe de carrera correspondiente.
                                </p>
                                <p style="text-align:center; ">  Para activar la cuenta haga clic en la siguiente liga:</p>
                            </td>
                        </tr>	 
                    </table>
                    <table class="frc" border="0" cellpadding="0" cellspacing="0" align="center" width="695">
                        <tr><td><?php
                                if (isset($user)) {
                                    echo base_url() . 'acceso/login/activar_user?id_user=' . $user;
                                    echo '<br>';
                                    echo '<a class="btn" href=' . base_url() . 'acceso/login/activar_user?id_user=' . $user . ' ' . 'style="-webkit-text-size-adjust: none; text-decoration: none; display: inline-block; color: #ffffff; background-color: #80c1d8; border-radius: 4px; -webkit-border-radius: 4px; -moz-border-radius: 4px; width: auto; width: auto; border-top: 1px solid #80c1d8; border-right: 1px solid #80c1d8; border-bottom: 1px solid #80c1d8; border-left: 1px solid #80c1d8; padding-top: 5px; padding-bottom: 5px; font-family: Poppins, Arial, Helvetica, sans-serif; text-align: center; mso-border-alt: none; word-break: keep-all;" target="_blank"><span style="padding-left:25px;padding-right:25px;font-size:16px;display:inline-block;"><span style="line-height: 32px; word-break: break-word;"></span>Liga</span></a>';
                                }
                                ?></td></tr>
                        <tr>
                            <td style="border-top:0px;">
                                <p style="text-align:center; ">    Haga clic en la liga para activar la cuenta o p&eacute;guela en el navegador de internet. </p>
                            </td>
                        </tr>	 
                    </table>
                    <table class="frc" border="0" cellpadding="0" cellspacing="0" align="center" width="700">
                        <tr>
                            <td class="footer"> 
                                <p class="mob-only2-txt" style="text-align:center; color: white; padding: 0px; font-size: 12px;">Instituto de Estudios Superiores de la Ciudad de M&eacute;xico "Rosario Castellanos"</p>
                                <img src="http://189.240.71.205/registro/actas/assets/img/sectei.png" height="auto" width="500px";
                                     class="mob-only" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>