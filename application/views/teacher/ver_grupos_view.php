<div class="content-wrapper" id='app'>
    <div class="container mb-3">
        <div class="row col-md-12">
            <h1 class="text-center alert titulo_historial">Grupos</h1>
        </div>
        <div class="row" style="border: 1px solid #fff;">
            <div class="col-md-12">
               
                    <div class="col-12 text-center">
                        <p class="text-secondary mt-5 titulo_mobile titulo_mobile m-0">Secretaria de Educación, Ciencia,
                            Tecnología e Innovación de la Ciudad de México</p>
                        <p class="text-secondary titulo_mobile m-0">Instituto de Estudios Superiores de la Ciudad de México</p>
                        <p class="text-secondary titulo_mobile m-0">“Rosario Castellanos” </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row col-md-12">
            <div class="col-md-2">
                <p>A&ntilde;o: <?php echo $datos_grupo[0]['year_active']; ?></p>
            </div>
            <div class="col-md-2">
                <p>Ciclo: <?php echo $datos_grupo[0]['cycle']; ?></p>
            </div>
            <div class="col-md-3">
                <p>Docente: <?php echo $_SESSION['name_user']; ?></p>
            </div>
            <div class="col-md-2">
                <p>Grupo: <?php echo $datos_grupo[0]['group_name']; ?></p>
            </div>
            <div class="col-md-3">
                <p>Asignatura: <?php echo $datos_grupo[0]['subject_name']; ?></p>
            </div>
        </div>
        <div class="col-lg-12 col-md-12">
            <table class="table table-responsive-sm table-bordered " width="100%">
                <thead>
                    <tr class="table-success">
                        <th>No</th>
                        <th>Matr&iacute;cula estudiante</th>
                        <th>Nombre del estudiante</th>
                        <th class="text-center">Calificaci&oacute;n final N&uacute;mero</th>
                        <th class="text-center">Calificaci&oacute;n final Letra</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $sub = 0; ?>
                    <?php foreach ($grupo as $key => $g) : ?>
                        <tr>
                            <td><?php echo ($key + 1) ?></td>
                            <td><?php echo $g['accountNumber'] ?></td>
                            <td><?php echo $g['names'] . ' ' . $g['surnames'] ?></td>
                            <td><?php echo $g['score'] == 'NP' ? '-' : $g['score']; ?></td>
                            <td><?php num_to_text($g['score']) ?> </td>
                        </tr>
                        <?php $sub++; ?>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
