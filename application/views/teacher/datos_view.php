<div class="content-wrapper">

    <div class="container">

        <div class="row col-md-12">
            <h1 class="mx-auto alert titulo_historial">Datos Personales</h1>
        </div>

        <div class="modal fade" id="aviso_modal" name="modal-area" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="false" data-backdrop="static">

            <div class="modal-dialog modal-xl" role="document">

                <div class="modal-content">

                    <div class="text-center p-3 naranja">
                        <h5 class="text-white modal-title fa fa-exclamation-triangle" id="atention"> Aviso de Privacidad</h5>
                    </div>

                    <div class="modal-body" style="background: #f2f2f2">

                        <h6 style="color: #262626;"><b>Ciudad de México, a 05 de julio de 2019</b></h6>

                        <hr>

                        <p align="justify">La Secretaría de Educación, Ciencia, Tecnología e Innovación de la Ciudad de México (SEDU), con
                            domicilio en Avenida Chapultepec No. 49, piso 2, Colonia Centro, Alcaldía Cuauhtémoc, C.P. 06010,
                            Ciudad de México, es la responsable del tratamiento de los datos personales proporcionados, los cuales
                            serán protegidos conforme a lo dispuesto por la Ley de Protección de Datos Personales en Posesión de
                            Sujetos Obligados de la Ciudad de México, y demás normatividad aplicable. Asimismo, se le informa que
                            datos personales recabados serán incorporados y tratados en el Sistema de Datos Personales de la
                            Administración Escolar de los Estudios Superiores de la Ciudad de México.</p>

                        <p align="justify">Los datos recabados se utilizarán para la siguiente finalidad: i. Administración de la información
                            proporcionada por el personal administrativo y personal académico, aspirantes, alumnos y egresados de los
                            programas educativos de nivel superior de la Ciudad de México; ii. Contar con un Sistema de
                            Administración Escolar que apoye los procesos de gestión escolar y administrativa; iii. Facilitar la
                            prestación de los servicios educativos de nivel superior relativos al registro en la convocatoria de ingreso
                            proceso de selección, publicación de resultados de alumnos y profesores por obra y tiempo determinado;
                            inscripción, reinscripción, acreditación y certificación de estudios de alumnos; iv. Otorgamiento del seguro
                            facultativo para alumnos; v. Expedición de documentación de los alumnos y elaboración de
                            identificaciones; vi. Elaboración de estudios y cuestionarios de carácter educativo y socioeconómico; vii.
                            Desarrollo de actividades extracurriculares de fortalecimiento académico; viii. Para trámites de asignación
                            de becas y/o apoyos para estudiantes de educación superior que en su caso procedan y ix. Elaboración de
                            informes, evaluaciones, revisiones respecto de los programas educativos de nivel superior de la Ciudad de
                            México.</p>

                        <p align="justify">Para las finalidades antes señaladas se solicitarán los siguientes datos personales: i. Administración de la
                            información proporcionada por el personal administrativo y personal académico, aspirantes, alumnos y
                            egresados de los programas educativos de nivel superior de la Ciudad de México; ii. Contar con un Sistema
                            de Administración Escolar que apoye los procesos de gestión escolar y administrativa; iii. Facilitar la
                            prestación de los servicios educativos de nivel superior relativos al registro en la convocatoria de ingreso
                            proceso de selección, publicación de resultados de alumnos y profesores por obra y tiempo determinado;
                            inscripción, reinscripción, acreditación y certificación de estudios de alumnos; iv. Otorgamiento del seguro
                            facultativo para alumnos; v. Expedición de documentación de los alumnos y elaboración de
                            identificaciones; vi. Elaboración de estudios y cuestionarios de carácter educativo y socioeconómico; vii.
                            Desarrollo de actividades extracurriculares de fortalecimiento académico; viii. Para trámites de asignación
                            de becas y/o apoyos para estudiantes de educación superior que en su caso procedan; ix. Elaboración de
                            informes, evaluaciones, revisiones respecto de los programas educativos de nivel superior de la Ciudad de
                            México. 1) Los datos recabados de los aspirantes a estudiantes son: A, B, C, D y E. 2) Los datos recabados
                            de los estudiantes son: A, B, C, D, E, F y G. 3) Los datos recabados del personal administrativo y personal
                            académico son: A, C, D, E y F. 4) Los datos recabados de los aspirantes a profesores por obra y tiempo
                            determinado son: A, C, D, E y F</p>

                        <p align="justify">El fundamento para el tratamiento de datos personales es el siguiente: 3°, 7°, 13 Fracción VII, VIII, 14
                            Fracción I, 37 último párrafo de la Ley General de Educación, 32 Apartado A, Fracciones I y VI de la Ley
                            Orgánica del Poder Ejecutivo de la Ciudad de México; 4°, 74, 75 y 76 de la Ley de Educación para el
                            Distrito Federal; Artículos 7 Fracción VII último párrafo del Reglamento Interior del Poder Ejecutivo y de
                            la Administración Pública de la Ciudad de México; Ordinal Primero y Séptimo fracción XVI del Decreto
                            por que se crea el Instituto de Estudios Superiores de la Ciudad de México “Rosario Castellanos”.
                            Para ejercer los derechos de acceso, rectificación, cancelación y oposición, así como la revocación del
                            consentimiento deberá ingresar a la página web <a target="_blank" class="link_aviso" href="http://data.educacion.cdmx.gob.mx/index.php/artuculo121/fraccixx">http://data.educacion.cdmx.gob.mx/index.php/artuculo121/fraccixx</a>
                            para obtener los formatos respectivos o bien acudir a la Unidad de Transparencia ubicada en Av.
                            Chapultepec, número 49, Planta Baja, Colonia Centro, Alcaldía Cuauhtémoc, C.P. 06010, teléfono 51 34
                            07 70 ext. 1017, al correo electrónico: oip-se@educacion.cdmx.gob.mx.</p>

                        <p align="justify">Las modificaciones al presente aviso estarán disponibles en <a target="_blank" class="link_aviso" href="http://www.sectei.cdmx.gob.mx/transparencia">http://www.sectei.cdmx.gob.mx/transparencia</a>
                            apartado Avisos Integrales de Protección de Datos.</p>

                    </div>

                    <div class="modal-footer border-top" style="background: #f2f2f2;">
                        <button type="button" class="btn text-white naranja" data-dismiss="modal">Acepto</button>
                    </div>

                </div>

            </div>

        </div>

        <div class="row" style="border: 1px solid #fff;">

            <div class="col-md-12">

                <div class="card">

                    <div class="card-body color_modal_personales">

                        <div class="row">

                            <div class="col">

                                <label for="nombre">Nombre(s):</label>
                                <p><i><?php echo $teacher['name']; ?></i></p>
                            </div>

                            <div class="col">

                                <label for="apellidos">Apellidos:</label>
                                <p><i><?php echo $teacher["surnames"]; ?></i></p>

                            </div>
                        </div>

                        <div class="row" style="margin-top: 20px;">

                            <div class="col">

                                <label for="email">Correo electr&oacute;nico:</label>
                                <p><i><?php echo $_SESSION['email'];
                                        ?></i></p>

                            </div>
                            <!-- 
                            <div class="col">

                                <label for="fecha-final">RFC:</label>
                                <p><i><?php //echo $teacher["rfc"]; 
                                        ?></i></p>

                            </div> -->

                        </div>
                    </div>

                </div>

            </div>

        </div>

    </div>

</div>
