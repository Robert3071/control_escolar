<script>
    const usuario = "<?php echo $usuario->ID_user; ?>";
</script>
<div id="app">
    <div class="container">
        <div class="container my-4">
            <section id="register-form">
                <div class="row">
                    <div id="edit_password" class="col-md-12 " >
                        <section>
                            <h1 class="text-center">  Cambiar contrase&ntilde;a
                            </h1>
                            <form  class="text-left border border-secondary p-5" v-on:submit.prevent="submit_updated_password" >
                                <div>
                                    <p> Para continuar requiere cambiar la contrase&ntilde;a de acceso, la cual debe conformarse por 8 caracteres alfanum&eacute;ricos.
                                        Considere las siguientes orientaciones para estructurar la contrase&ntilde;a:</p>
                                    <ol>
                                        <li>8 caracteres</li>
                                        <li>Al menos una letra may&uacute;scula</li>
                                        <li>Al menos una letra min&uacute;scula</li>
                                        <li>Al menos un d&iacute;gito del 1 al 9</li>
                                        <li>Al menos 1 car&aacute;cter especial: @ $ ! % * ? & # . $</li>
                                    </ol>
                                    <p class="text-bold">
                                        NOTA. Los espacios en blanco, no se aceptan como car&aacute;cter.<br>
                                        Al cambiar la contrase&ntilde;a será enviado a tu correo, una liga con la cual podrá activar 
                                        su usuario. Es importante revisar en su correo electrónico, la carpeta de SPAM donde es posible que se aloje dicho mensaje. Para poder iniciar sesión, es necesario realizar este proceso.</p>
                                </div>
                                <div class="row">

                                    
                                    <div class="col-md-6 col-sm-6">
                                        <label for="passwd_usuario">Contrase&ntilde;a:</label>
                                        <div class="input-group mb-2">
                                            <div class="input-group-prepend" v-on:click="show_password">
                                                <div class="input-group-text"><i :class="icon_password"></i></div>
                                            </div>
                                            <input data-attr="show-password" required="required" 
                                                   data-toggle="password"
                                                   :type=type_password
                                                   v-model="passwd_usuario"
                                                   class="form-control"  
                                                   pattern = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&#.$($)$-$_])[A-Za-z\d$@$!%*?&#.$($)$-$_]{8}$"
                                                   placeholder="Ingrese la nueva contrase&ntilde;a (8 caracteres)"
                                                   @keyup="verificar_password"
                                                   maxlength="8"
                                                   minlength="8">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <label for="passwd_usuario_edit">
                                            Repita la contrase&ntilde;a:</label>
                                        <div class="input-group mb-2">
                                            <div class="input-group-prepend" v-on:click="show_password_edit">
                                                <div class="input-group-text"><i :class="icon_password_edit"></i></div>
                                            </div>
                                            <input required="required" 
                                                   :type=type_password_edit
                                                   pattern = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&#.$($)$-$_])[A-Za-z\d$@$!%*?&#.$($)$-$_]{8}$"
                                                   v-model="passwd_usuario_edit" 
                                                   @keyup="verificar_password"
                                                   class="form-control"  
                                                   placeholder="Repita la contrase&ntilde;a (8 caracteres)"
                                                   maxlength="8"
                                                   minlength="8">
                                        </div> 
                                    </div> 
                                </div>
                                <p :class="clase">{{message_verify_pass}}</p>
                                <button class="btn btn-info my-4 btn-block waves-effect waves-light" type="submit">Enviar</button>
                            </form>
                        </section>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
