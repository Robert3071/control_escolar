<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <a href="index3.html" class="brand-link">
        <img src="<?php echo base_url(); ?>assets/plugins/img/logo_irc.png" alt="logo IRC" class="brand-image elevation-3">
        <span class="brand-text">IRC</span>
    </a>
    <div class="sidebar">
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">

            <div class="info">
                <a href="#" class="d-block"><i class="fas fa-user fa-lg mr-2"></i>Administrador de Licenciatura</a>
            </div>
        </div>
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item has-treeview menu-open">
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="<?php echo base_url('session/admin_jef_carr/Admin_dashboard/'); ?>" class="nav-link naranja_link">
                                <i class="nav-icon fas fa-chart-pie"></i>
                                <p>Datos Personales</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?php echo base_url('session/admin_jef_carr/Admin_dashboard/grupos'); ?>" class="nav-link naranja_link">
								<i class="nav-icon fas fa-edit"></i>
                                <p>Rectificaci&oacute;n</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?php echo base_url('session/admin_jef_carr/Admin_status_actas'); ?>" class="nav-link naranja_link">
								<i class="nav-icon fas fa-clipboard-check"></i>
                                <p>Estatus actas</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?php echo base_url('session/admin_jef_carr/Admin_history_teacher'); ?>" class="nav-link naranja_link">
                                <i class="nav-icon fas fa-user"></i>
                                <p>Profesores</p>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
    </div>
</aside>
