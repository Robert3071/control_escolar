<main class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">
        
        <nav class="main-header">
        <div class="row ml-auto mr-auto " style="background-color: #9F2241;">
        <img src="<?php echo base_url(); ?>assets/img/logos_encabezado.svg" class="img-fluid" width="600px" alt="">
        </div>
        <div class="navbar navbar-expand navbar-white navbar-light">
            <ul class="navbar-nav"> 
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars hamburguesa"></i></a>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="" class="nav-link">Men&uacute;</a>
                </li>
                <!-- <li class="nav-item d-none d-sm-inline-block">
                    <a href="" class="nav-link">Datos</a>
                </li> -->
            </ul>
            <div class="ml-auto" style="background-color: #9F2241; border-radius:10px">
                <button class="btn">
                    <a style="color:white;" href="<?= base_url('acceso/Login/logout_admin'); ?>">
                        <b>Cerrar sesión</b>
                    </a>
            </button> 
            </div>
        </div>
        </nav>
    </div>
</main>