<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <a href="index3.html" class="brand-link">
        <img src="<?php echo base_url(); ?>assets/plugins/img/logo_irc.png" alt="logo IRC" class="brand-image elevation-3">
        <span class="brand-text">IRC</span>
    </a>
    <div class="sidebar">
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="info">
                <a href="#" class="d-block"><i class="fas fa-user fa-lg mr-2"></i><?= $this->session->userdata('name_user') . ' ' . $this->session->userdata('surname_rur') ?></a>
            </div>
        </div>
        <nav class="mt-2">
           
        </nav>
    </div>
</aside>