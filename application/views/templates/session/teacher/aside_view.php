<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <a href="index3.html" class="brand-link">
        <img src="<?php echo base_url(); ?>assets/plugins/img/logo_irc.png" alt="logo IRC" class="brand-image elevation-3">
        <span class="brand-text">IRC</span>
    </a>
    <div class="sidebar">
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="info">
                <a href="#" class="d-block"><i class="fas fa-user fa-lg mr-2"></i><?= $this->session->userdata('name_user') . ' ' . $this->session->userdata('surname_rur') ?></a>
            </div>
        </div>
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item has-treeview menu-open">

                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="<?php echo base_url('session/teacher/Teacher_dashboard/'); ?>" class="nav-link naranja_link">
                                <i class="far fa-id-card"></i>
                                <p>Datos Personales</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?php echo base_url('session/teacher/Teacher_dashboard/fei'); ?>" class="nav-link naranja_link">
                                <i class="fas fa-key"></i> 
                                <p>FDI</p>
                            </a>
                        </li>
                        <!--condicion para  visualizar botones--> 
                        <?php if (calendario_teacher(calendario_docente_1(), 1, 1) != 0 || calendario_teacher(calendario_docente_2(), 1, 1) != 0) { ?>
                            <li class="nav-item">
                                <a href="<?php echo base_url('session/teacher/Grupos/'); ?>" class="nav-link naranja_link">
                                    <i class="fas fa-users"></i>                               
                                    <p>Grupos</p>
                                    <p>Ordinarios</p>
                                </a>
                            </li>
                        <?php } ?>
                        <?php if (calendario_teacher(calendario_docente_5(), 1, 1) != 0) { ?>
                            <li class="nav-item">
                                <a href="<?php echo base_url('session/teacher/extras/'); ?>" class="nav-link naranja_link">
                                    <i class="fas fa-users"></i>                               
                                    <p>Grupos</p>
                                    <p>Extraordinarios</p>
                                </a>
                            </li>
                        <?php } ?>
                        <?php if (calendario_teacher(calendario_docente_4(), 1, 1) != 0) { ?>
                            <li class="nav-item">
                                <a href="<?php echo base_url('session/teacher/rectification_actas/'); ?>" class="nav-link naranja_link">
                                    <i class="fas fa-edit"></i>
                                    <p>Rectificaci&oacute;n</p>
                                </a>
                            </li>
                        <?php } ?>
                        <?php if (calendario_teacher(calendario_docente_7(), 1, 1) != 0) { ?>
                            <li class="nav-item">
                                <a href="<?php echo base_url('session/teacher/rectification_actas_extra/'); ?>" class="nav-link naranja_link">
                                    <i class="fas fa-edit"></i>
                                    <p>Rectificaci&oacute;n </p>
                                    <p>Extraordinarios</p>
                                </a>
                            </li>
                        <?php } ?>

                    </ul>
                </li>
            </ul>
        </nav>
    </div>
</aside>
