<div class="my-5">
    <div class="container">
        <section id="register-form">
            <div class="row">
                <div id="edit_password" class="col-md-12 " >
                    <h1 class="text-center">  Olvid&oacute; su contrase&ntilde;a
                    </h1>
                    <form  class="text-left border border-secondary p-5 my-5"  method="get" action="<?php echo base_url('acceso/login/olvidao_pass_change '); ?>">
                        <div>
                            <p> Para continuar, digite el correo electr&oacute;nico para crear una nueva contrase&ntilde;a.</p>

                        </div>
                        <div class="form-row mb-4">
                            <div class=" col">
                                <label for="passwd_usuario">Correo :</label>
                                <input name="correo" required="required" 
                                       class="form-control"  
                                       placeholder="Ingrese su correo electrónico"
                                       >
                            </div>
                        </div>
                        <button class="btn btn-info my-4 btn-block waves-effect waves-light" type="submit">Enviar</button>
                    </form>
                    <?php if ($this->session->flashdata('error_cambio_pass')) : ?>
                        <span>
                            <div class="alert alert-danger mt-2" role="alert"><?php echo $this->session->flashdata('error_cambio_pass'); ?></div>
                        </span>
                    <?php endif; ?>
                </div>
            </div>
        </section>
    </div>
</div>
