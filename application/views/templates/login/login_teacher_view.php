<main id="app" class="fondo">
    <?php echo validation_errors(); ?>
    <div class="container pt-3">
        <div class="row justify-content-lg-center p-4">
            <div class="col-lg-6">
                <div class="card cardlogin">
                    <h1 class="card-header text-center">Ingreso Docente</h1>
                    <div class="form p-3">
                        <!-- <div class="alert alert-success" role="alert">
                                Registra los siguientes datos, recuerda que tu contraseña es tu fecha de nacimiento (aaaammdd)
                        </div> -->
                        <form action="<?= base_url('acceso/login/validate_teacher'); ?>" method="POST" id="frm_login" class="needs-validation" novalidate>

                            <div class="col-auto">
                                <label class="" for="inlineFormInputGroup">Correo electrónico</label>
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text" style="background-color:#465973; border: 0px;"><i class="fas fa-envelope text-white"></i></div>
                                    </div>
                                    <input id="user" type="text" name="email" class="form-control" id="inlineFormInputGroup" placeholder="Escriba la direcci&oacute;n de correo electr&oacute;nico proporcionada">
                                </div>
                            </div>
                            <div class="invalid-feedback">Correo Invalido</div>

                            <div class="col-auto">
                                <label class="" for="inlineFormInputGroup">Contraseña</label>
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                    <div class="input-group-prepend" v-on:click="show_password">
                                                <div style="background-color:#465973; border: 0px;" class="input-group-text"><i :class="icon_password" style="background-color:whitesmoke;"></i></div>
                                    </div>
                                    </div>
                                    <input data-attr="show-password" 
                                                   required="required" 
                                                   data-toggle="password"
                                                   :type=type_password
                                                   v-model="passwd_usuario" type="password" name="password" class="form-control" id="inlineFormInputGroup" placeholder="Ingrese la contraseña proporcionada">
                                </div>
                            </div>

                            <div class="form-group mt-4 text-center">
                                <button type="submit" class="btn btn-lg text-white naranja">Acceder</button>
                            </div>
                            <span id="error"></span>
                            <?php if ($this->session->flashdata('error_ss')) : ?>
                                <span>
                                    <div class="alert alert-danger mt-2" role="alert"><?php echo $this->session->flashdata('error_ss'); ?></div>
                                </span>
                            <?php endif; ?>
                            <a class="text-black-50" href="<?php echo base_url('acceso/login/olvidao_pass'); ?>">Olvid&oacute; su contraseña</a>
                        </form>
                        <?php if ($this->session->flashdata('exito_pass')) : ?>
                            <span>
                                <div class="alert alert-success mt-2" role="alert"><?php echo $this->session->flashdata('exito_pass'); ?></div>
                            </span>
                        <?php endif; ?>
                        <?php if ($this->session->flashdata('exito_status')) : ?>
                            <span>
                                <div class="alert alert-success mt-2" role="alert"><?php echo $this->session->flashdata('exito_status'); ?></div>
                            </span>
                        <?php endif; ?>
                    </div>
                </div>

            </div>
        </div>
    </div>
</main>
<script>
    $(document).ready(function () {

        $("#myModal1").modal();
    });
</script>
