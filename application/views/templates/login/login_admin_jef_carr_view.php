<main class="fondo">
		<?php echo validation_errors(); ?>
		<div class="container pt-3">
			<div class="row justify-content-lg-center p-4">
				<div class="col-lg-6">
					<div class="card cardlogin">
						<h1 class="card-header text-center">Ingreso Jefe de carrera</h1>
						<div class="form p-3">
							<!-- <div class="alert alert-success" role="alert">
								Registra los siguientes datos, recuerda que tu contraseña es tu fecha de nacimiento (aaaammdd)
							</div> -->
							<form action="<?= base_url('acceso/login/validate_admin_jefe_carrera'); ?>" method="POST" id="frm_login" class="needs-validation" novalidate>

								<div class="col-auto">
									<label class="" for="inlineFormInputGroup">Correo electrónico</label>
									<div class="input-group mb-2">
										<div class="input-group-prepend">
											<div class="input-group-text" style="color:#454545;"><i class="fas fa-user icono_login"></i></div>
										</div>
										<input id="user" type="text" name="email" class="form-control" id="inlineFormInputGroup" placeholder="Escriba la direcci&oacute;n de correo electr&oacute;nico proporcionada">
									</div>
								</div>
								<div class="invalid-feedback">Correo Invalido</div>
									<!-- Mensaje de error -->
									<span id="error"></span>
									<?php if ($this->session->flashdata('error_ss')) : ?>
										<span>
											<div class="alert alert_login_error mt-2" role="alert"><?php echo $this->session->flashdata('error_ss'); ?></div>
										</span>
									<?php endif; ?>
								<!-- termina mensaje de error -->
								<div class="col-auto">
									<label class="" for="inlineFormInputGroup">Contraseña</label>
									<div class="input-group mb-2">
										<div class="input-group-prepend">
											<div class="input-group-text" style="color:#454545;"><i class="fas fa-lock icono_login"></i></div>
										</div>
										<input id="validaPass" type="password" name="password" class="form-control" id="inlineFormInputGroup" placeholder="Ingrese la contraseña proporcionada">
									</div>
								</div>

								<div class="form-group mt-4 text-center">
									<button type="submit" class="btn btn-lg text-white naranja">Acceder</button>
								</div>
								

							</form>
						</div>
						<!-- <div class="text-message pl-4" style="background-color:#edeff1;">
							<p class="text-muted text-center font-weight-bold m-4">
								En caso de no poder ingresar, </br>consulta nuestra mesa de ayuda
							</p>
						</div> -->
					</div>
				</div>
			</div>
		</div>
	</main>