<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="card">
			<div class="card-header">
				<h1 class="calendar text-center">Atenci&oacute;n consulte el calendario</h1>
			</div>
			<div class="card-body text-center">
				<h2>Sistema Cerrado</h2>
			</div>
			<div class="card-footer d-flex justify-content-center">
				<a class="btn btn-primary" href="<?php echo base_url('acceso/Login/login_teacher'); ?>">Regresar</a>
			</div>
		</div>
	</div>
</div>
