<html lang="es">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="assets/registro/css/principal.css">
        <title>Acta de calificaciones de estudiantes</title>
    </head>

    <body>
        <nav>
        <div class="navbar shadow-sm" style="background-color: #9F2241;">
		<div class="d-flex justify-content-between">
			<img src="assets/img/cintillo_superior.png" class="img-fluid" width="600px" alt="">
		</div>
	</div>
            <div>
                <div class="PDFtitulo">
                    <h2>Acta de calificaciones de estudiantes</h2>
                </div>
            </div>
        </nav>
        <div>

            <table class="PDFtable">
                <tr>
                    <th colspan="2">
                        <p>Nombre del docente: <?php
                            echo ucwords(strtolower($datos_grupo[0]['name'] . ' ' . $datos_grupo[0]['surnames']));
                            ?></p>
                        <p>Folio del acta: <?php
                            if (isset($datos_grupo[0]['folio_acta'])) {
                                echo $datos_grupo[0]['folio_acta'];
                            }
                            ?></p>
                        <p>Fecha del acta: <?php
                            if (isset($datos_grupo[0]['updated_at'])) {
                                echo $datos_grupo[0]['updated_at'];
                            }
                            ?></p>
                        <p>Tipo:<?php
                            if (isset($datos_grupo[0]['type_group'])) {
                                echo $datos_grupo[0]['type_group'];
                            }
                            ?></p>
                    </th>
                    <th colspan="2">
                        <p>A&ntilde;o: <?php
                            if (isset($datos_grupo[0]['year_active'])) {
                                echo $datos_grupo[0]['year_active'];
                            }
                            ?></p>
                        <p>Ciclo: <?php
                            if (isset($datos_grupo[0]['cycle'])) {
                                echo $datos_grupo[0]['cycle'];
                            }
                            ?></p>
                        <p>Clave asignatura: <?php
                            if (isset($datos_grupo[0]['key_curse'])) {
                                echo $datos_grupo[0]['key_curse'];
                            }
                            ?></p>
                        <p>Nombre de la asignatura: <?php
                            if (isset($datos_grupo[0]['subject_name'])) {
                                echo $datos_grupo[0]['subject_name'];
                            }
                            ?></p>
                    </th>
                </tr>
            </table>
            <table class='PDFtable'>
                <thead class="PDFtencabezado">
                    <tr>
                        <th scope="col"><small>No</small></th>
                        <th scope="col"><small>Matr&iacute;cula estudiante</small></th>
                        <th scope="col"><small>Nombre del estudiante</small></th>
                        <th class="text-center" scope="col"><small>Calificaci&oacute;n final N&uacute;mero</small></th>
                        <th class="text-center" scope="col"><small>Calificaci&oacute;n final Letra</small></th>
                    </tr>
                </thead>
                <tbody class="PDFtcuerpo">
                    <?php foreach ($grupo as $key => $g) : ?>
                        <tr>
                            <td class='PDFtcuerpo'><small><?php echo ($key + 1) ?></small></td>
                            <td class='PDFtcuerpo'><small><?php echo $g['accountNumber'] ?></small></td>
                            <td class='PDFtcuerpo'><small><?php echo $g['names'] . ' ' . $g['surnames'] ?></small></td>
                            <td class='PDFtcuerpo'><small><?php echo $g['score'] == 'NP' ? '-' : $g['score']; ?></small></td>
                            <td class='PDFtcuerpo'><small><?php num_to_text($g['score']) ?></small></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <div class="PDFirmas">
            <table class='PDFtfirmas'>
                <tr>
                    <td class="thFirma ">
                        <img src="<?php echo $datos_grupo[0]['ruta_firm_auto']; ?>"  alt="firma"  width="100" height="100" style="margin:0px; padding: 0;">
                      <br/><?php echo ucwords(strtolower($datos_grupo[0]['name'])) . ' ' . ucwords(strtolower($datos_grupo[0]['surnames'])); ?> <br/>
                            Nombre del docente
                    </td>
                </tr>
            </table>
        </div>

        <br><br><br><br><br><br><br>
	<br><br>
	<nav>
		<div class="navbar shadow-sm">
			<div class="d-flex justify-content-between">
				<img src="assets/img/cintillo_inferior.png" class="img-fluid" width="720px" alt="">
			</div>
		</div>
    </body>