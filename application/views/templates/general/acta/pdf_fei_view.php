<html lang="es">

<head>
	<meta charset="UTF-8">
	<meta name="viewport"
		  content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="stylesheet" href="assets/registro/css/principal.css">
	<title>Firma Digital Institucional</title>
</head>

<body>
<nav>
	<div class="navbar shadow-sm" style="background-color: #9F2241;">
		<div class="d-flex justify-content-between">
			<img src="assets/img/cintillo_superior.png" class="img-fluid" width="600px" alt="">
		</div>
	</div>
</nav>
<div>
	<div class="alinear_derecha">
		<p class="alinear_derecha">Ciudad de México, a <?php echo date("d"); ?> de <?php echo $mes; ?>
			del <?php echo date("Y"); ?></p>
	</div>

	<div class="col-12 letra alinear_izquierda">
	</div>
	<div class="col-12 letra text-justify">
		<p class="">
			Apreciable docente.
			<span class="capitalize"><?php echo $name_prof; ?>.</span><br>
		</p>

		<p class="m-4">
			En el Instituto de Estudios Superiores de la Ciudad de México “Rosario Castellanos”, nos
			preparamos para la transición digital en los procesos académicos administrativos, como
			parte de esta nueva dimensión de trabajo nos permitimos hacerle llegar su Firma
			Digital Institucional (FDI) que utilizará para la firma de actas académicas del ciclo <?php echo $ciclo; ?>.


		</p>
		<p class="m-4">
			Le solicitamos la tenga a buen resguardo y haga uso de ella únicamente para los fines
			institucionales solicitados.
		</p>
		<p class="m-4">
			Agradecemos su compromiso y apoyo para que el trámite de registro y firma de actas, se
			realice conforme a las fechas establecidas en el calendario institucional.
		</p>
		<br><br>


		<!-- aqui esta el qr -->

		<div class="PDFirmas">
			<table class='PDFtfirmas'>
				<tr>
					<td class="thFirma ">
						<img src="assets/img/qr_Ime.png" alt="firma" width="100" height="100"
							 style="margin:0px; padding: 0;">
						<br/><h5 class="">ATENTAMENTE</h5>

						<P>Imelda Berenice González Juárez<br>
							Coordinación de Soporte
						</P>
					</td>
				</tr>
			</table>
		</div>

		<!-- termina qe -->

	</div>

	<br><br><br><br><br><br><br>
	<br><br>
	<nav>
		<div class="navbar shadow-sm">
			<div class="d-flex justify-content-between">
				<img src="assets/img/cintillo_inferior.png" class="img-fluid" width="720px" alt="">
			</div>
		</div>

	</nav>
</body>
