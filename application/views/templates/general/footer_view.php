<footer class="page-footer font-small pt-4" style="background-color: #edeff1;">
    <div class="container text-center text-md-left">
    <!-- empieza Grid row -->

    <?php /** Esto es para que el footer no aparesca las redes
     * en la parte de dashboard
     *  */ 
    $uri=explode("/", $_SERVER['REQUEST_URI']);  
    $longitud=count($uri);
    ?>
    <?php if($uri[$longitud-1]=="login_admin" || $uri[$longitud-1]=="login_jefe_carrera" || $uri[$longitud-1]=="login_teacher" || $uri[$longitud-1]=="" ): ?>
    
        <div class="row">

            <div class="col-md-4 col-lg-4 mr-auto my-md-4 my-0 mt-4 mb-1">

                <h5 class="font-weight-bold mb-4 text-muted">IRC</h5>
                <p class="text-muted">Direcci&oacute;n: Avenida Chapultepec 49 Colonia Centro, Alcald&iacute;a Cuauht&eacute;moc C.P. 00000, Ciudad de M&eacute;xico, Tel&eacute;fono: 51340770 Ext. 1420 Horario: Lunes a Viernes 09:00 a 18:00 </p>
                <p>
                    <!---->
                    Tambi&eacute;n puedes visitar el 
                    <a class="text-muted font-weight-bold" target="_blank" href="https://www.rcastellanos.cdmx.gob.mx/">Portal IRC</a>
                </p>
            </div>
            <div class="col-md-4 col-lg-4 mx-auto my-md-4 my-0 mt-4 mb-1">

                <h5 class="font-weight-bold mb-4 text-muted">Dudas</h5>

                <ul class="list-unstyled">
                    <li>
                        <p>
                        <i class="fas fa-question-circle mr-3 text-muted"></i> <a class="text-muted font-weight-bold subrayar" target="_blank" href="http://app.rcastellanos.cdmx.gob.mx/mesadeayuda/inicio/">Mesa de ayuda</a>
                        </p>
                    </li>

                    <li>
                        <p>
                            <i class="fas fa-lock mr-3 text-muted"></i> <a class="text-muted font-weight-bold subrayar" target="_blank" href="<?php echo base_url('/assets/registro/guias_de_uso/aviso_privacidad_IRC.pdf'); ?>">Aviso de privacidad</a>
                        </p>
                    </li>
                    
                </ul>
            </div>
            <hr class="clearfix w-100 d-md-none">
            <!-- Grid column -->
            <div class="col-md-2 col-lg-2 text-center mx-auto my-4">

                <!-- Social buttons -->
                <h5 class="font-weight-bold mb-4 text-muted">S&iacute;guenos</h5>

                <!-- Facebook -->
                <a type="button" class="btn-floating btn-fb text-muted" target="_blank" href="https://www.facebook.com/irosariocastellanos/">
                    <i class="fab fa-facebook-square fa-3x"></i>
                </a>
               

                <!-- Youtube -->
                <a type="button" class="btn-floating btn-tw text-muted" target="_blank" href="https://www.youtube.com/channel/UC-axvfRWGcQGvDvP51BAuKw">
                <i class="fab fa-youtube-square fa-3x"></i>
                </a>

                <a type="button" class="btn-floating btn-tw text-muted" target="_blank" href="https://www.instagram.com/irc_oficial/">
                    <i class="fab fa-instagram-square fa-3x"></i>
                </a>

            </div>
            <!-- Grid column -->

        </div>
        <!-- Grid row -->
    <?php endif; ?>

    </div>
    <!-- Footer Links -->

    <div class="text-footer p-4 text-center" style="background-color: #465973;">
        <h5 class="text-white pb-3">Instituto de Estudios Superiores de la Ciudad de M&eacute;xico "Rosario Castellanos".</h5>
        <img src="<?php echo base_url(); ?>assets/img/logos_footer.svg" class="img-fluid" width="600px" alt="">
    </div>
</footer>
<!-- Footer -->