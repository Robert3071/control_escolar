<!-- Modal Calendario-->
<?php $fecha_inicio = 'calendario_teacher_' . $numero_calendario . '_fecha_ini'; ?>
<?php $fecha_fin = 'calendario_teacher_' . $numero_calendario . '_fecha_fin'; ?>
<?php $fechas = 'calendario_docente_' . $numero_calendario; ?>
<?php $titulo = 'calendario_teacher_' . $numero_calendario . '_title'; ?>
<?php
if (date_today() >= $fecha_inicio() && date_today() <= $fecha_fin()) {
	?>
	<div class="modal-dialog modal-lg">
		<div class="alert alert-calendar text-center" role="alert">
			<i class="fas fa-exclamation-triangle"></i>
			Atenci&oacute;n consulte el calendario
		</div>
		<strong>
			<p class=" text-center "><?php echo $titulo(); ?></P>
		</strong>
		<p class=" text-center "><?php echo date(" Y-m-d H:i:s", $fecha_inicio()) . ' al ' . date(" Y-m-d H:i:s", $fecha_fin()); ?></p>
		<div class="modal-content">
			<div class="card">

				<div class="card-body">
					<table class="table table-responsive-sm ">
						<thead class="color_calendar_table">
						<tr>
							<th>Carrera</th>
							<th>Fecha</th>
							<th>Horario</th>
						</tr>
						</thead>
						<tdbody>
							<tr>
								<td>Licenciatura en administraci&oacute;n y comercio.</td>
								<td>
									<?php echo date("Y-m-d", $fechas()[0]['fecha_ini']) . ' al ' . date("Y-m-d", $fechas()[0]['fecha_fin']); ?>
								</td>
								<td>
									<?php echo date("H:i:s", $fechas()[0]['fecha_ini']) . ' a ' . date("H:i:s", $fechas()[0]['fecha_fin']); ?>
								</td>
							</tr>
							<tr>
								<td>Licenciatura en mercadotecnia y ventas.</td>
								<td>
									<?php echo date("Y-m-d", $fechas()[1]['fecha_ini']) . ' al ' . date("Y-m-d", $fechas()[1]['fecha_fin']); ?>
								</td>
								<td>
									<?php echo date("H:i:s", $fechas()[1]['fecha_ini']) . ' a ' . date("H:i:s", $fechas()[1]['fecha_fin']); ?>
								</td>
							</tr>
							<tr>

								<td>Licenciatura en tecnolog&iacute;a de informaci&oacute;n y comunicaci&oacute;n.</td>
								<td>
									<?php echo date("Y-m-d", $fechas()[2]['fecha_ini']) . ' al ' . date("Y-m-d", $fechas()[2]['fecha_fin']); ?>
								</td>
								<td>
									<?php echo date("H:i:s", $fechas()[2]['fecha_ini']) . ' a ' . date("H:i:s", $fechas()[2]['fecha_fin']); ?>
								</td>
							</tr>
							<tr>
								<td>Licenciatura en humanidades y narrativas multimedia.</td>
								<td>
									<?php echo date("Y-m-d", $fechas()[3]['fecha_ini']) . ' al ' . date("Y-m-d", $fechas()[3]['fecha_fin']); ?>
								</td>
								<td>
									<?php echo date("H:i:s", $fechas()[3]['fecha_ini']) . ' a ' . date("H:i:s", $fechas()[3]['fecha_fin']); ?>
								</td>
							</tr>
							<tr>
								
								<td>Licenciatura en Contaduría y Finanzas.</td>
								<td>
									<?php echo date("Y-m-d", $fechas()[4]['fecha_ini']) . ' al ' . date("Y-m-d", $fechas()[4]['fecha_fin']); ?>
								</td>
								<td>
									<?php echo date("H:i:s", $fechas()[4]['fecha_ini']) . ' a ' . date("H:i:s", $fechas()[4]['fecha_fin']); ?>
								</td>
							</tr>
							<tr>
								<td>Licenciatura en Relaciones Internacionales.</td>
								<td>
									<?php echo date("Y-m-d", $fechas()[5]['fecha_ini']) . ' al ' . date("Y-m-d", $fechas()[5]['fecha_fin']); ?>	
								</td>
								<td>
									<?php echo date("H:i:s", $fechas()[5]['fecha_ini']) . ' a ' . date("H:i:s", $fechas()[5]['fecha_fin']); ?>
								</td>
							</tr>
							<tr>
								<td>Licenciatura en Psicología.</td>
								<td>
									<?php echo date("Y-m-d", $fechas()[6]['fecha_ini']) . ' al ' . date("Y-m-d", $fechas()[6]['fecha_fin']); ?>
								</td>
								<td>
									<?php echo date("H:i:s", $fechas()[6]['fecha_ini']) . ' a ' . date("H:i:s", $fechas()[6]['fecha_fin']); ?>
								</td>
							</tr>
							<tr>
								<td>Licenciatura en Derecho y Criminología</td>
								<td>
									<?php echo date("Y-m-d", $fechas()[7]['fecha_ini']) . ' al ' . date("Y-m-d", $fechas()[7]['fecha_fin']); ?>
								</td>
								<td>
									<?php echo date("H:i:s", $fechas()[7]['fecha_ini']) . ' a ' . date("H:i:s", $fechas()[7]['fecha_fin']); ?>
								</td>
							</tr>
							
						</tdbody>
					</table>
				</div>
			</div>
			<div class="card-footer d-flex justify-content-center">
				<a class="btn btn-calendario" href="<?php echo base_url('acceso/Login/login_teacher'); ?>">Continuar</a>
			</div>

			<div class="card-footer d-flex justify-content-left ">

				<a class="btn btn-regresar-calendary " href="<?php echo base_url('inicio_index/actas'); ?>"><i
							class="fas fa-angle-left m-2"></i>Regresar</a>
			</div>
		</div>
	</div>
<?php } else {
	?>
	<?php cerrado_view(2);
} ?>
