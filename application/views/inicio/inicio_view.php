<section class="row mb-5" style="background-color: blue; margin:0% !important;">
    <div class="col-12" style="background-color: blue; padding:0% !important;">
        <picture>
            <source media="(max-width: 800px)" srcset="<?php echo base_url() ?>assets/img/historial_academico_iimagen_movil.png">
            <img src="<?php echo base_url() ?>assets/img/historial_academico_imagen_escritorio.png" class="img-fluid" style=" width: 100%;" alt="">
        </picture>
        <div>
            <h1 class="titulo-escritorio">Sistema de Historial Académico.</h1>
        </div>
    </div>
</section>
<div class="container mt-4">
    <section class="row">
        <article class="col-12">
            <header class="row">
                <div class="col-12">
                    <div class="contenedorTitulo">
                        <h1 class="text-center mb-5 mobile">Sistema de Historial Académico.</h1>
                    </div>
                </div>
            </header>
            <!-- <?php echo password_hash('patito123', PASSWORD_DEFAULT); ?> -->
            <section class="row">
                <div class="col-12 col-sm-8 offset-sm-2 formatoParrafo">
                    <h2 class="mb-5">
                        Apreciable estudiante del IRC:
                    </h2>
                    <p>Ponemos a tu disposición el Sistema de Historial Académico del Instituto de Estudios Superiores de la Ciudad de México "Rosario Castellanos"; a través de él podrás realizar la solicitud y descarga del documento de tu historia académica.</p>
                    <p>Te invitamos a revisar la <a href="#" class="letra_naranja">Guía de uso del Sistema</a>,
                        si después de revisarlo tienes alguna duda o se presenta un incidente, recuerda que en <a target="_blank" class="letra_naranja" href="http://app.rcastellanos.cdmx.gob.mx/mesadeayuda/inicio">Mesa de Ayuda </a> estamos para atenderte.</p>

                </div>
            </section>
             <!-- empieza windget -->
    <div class="container bootstrap snippets bootdey alinear_flex mt-5 mb-5">
    
    
    

    <div class="col-sm-4 mt-4">
      <div class="widget single-news">
        <div class="image">
          <img src="<?php echo base_url(); ?>assets/img/fondo.png" class="img-responsive">
          <span class="gradient"></span>
        </div>
        <div class="details">
          <div class="category"><a href="">Calendario</a></div>
          <h3><a href="<?php echo base_url('inicio_index/calendarios?calendary=3');?>">Historial Académico</a></h3>
          <time>Ir</time>
        </div>
      </div>
    </div> 
    <div class="col-sm-4 mt-4">
      <div class="widget single-news">
        <div class="image">
          <img src="<?php echo base_url(); ?>assets/img/fondo.png" class="img-responsive">
          <span class="gradient"></span>
        </div>
        <div class="details">
          <div class="category"><a href="<?php echo base_url('inicio_index/calendarios?calendary=6');?>">Calendario</a></div>
          <h3><a href="<?php echo base_url('inicio_index/calendarios?calendary=6');?>">Consulta de historial Extraordinario</a></h3>
          <time>Ir</time>
        </div>
      </div>
    </div> 

    

    </div>

    

    
    
  
    </div>
    <!-- termina windget -->
        </article>
    </section>
</div>
<div class="col-md-12 mb-2">
    <div class="text-center">
        <a class="btn btn-lg text-white naranja" href="<?php echo base_url('acceso/Login') ?>">
            <i class="fas fa-chevron-circle-right"></i> Continuar
        </a>
    </div>
</div>
<!-- aviso de privacidad inicio -->
<div class="modal fade in" id="myModal1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header" style="background-color:#f26e50">
            </div>
            <div class="">

                <div class="card-header">
                    <h2 class="calendar text-center">Aviso de privacidad</h2>
                </div>


                <div class="card-body" style="background-color: WHITE;padding: 3rem 2rem 2rem;">

                    <p>El Instituto de Estudios Superiores de la Ciudad de México “Rosario Castellanos” como órgano
                        desconcentrado de la Secretaría de Educación, Ciencia, Tecnología e Innovación de la Ciudad de México, con
                        domicilio Avenida 506 Eje 3 Norte S/N Esquina Loreto Fabela, Colonia San Juan de Aragón Sección II,
                        Alcaldía Gustavo A. Madero, C.P. 07979, Ciudad de México, es el responsable del tratamiento de los datos
                        personales contenidos en el <b>SISTEMA DE DATOS PERSONALES DEL INSTITUTO DE ESTUDIOS
                            SUPERIORES DE LA CIUDAD DE MÉXICO “ROSARIO CASTELLANOS”, ÓRGANO
                            DESCONCENTRADO DE SECRETARÍA DE EDUCACIÓN, CIENCIA, TECNOLOGÍA E
                            INNOVACIÓN DE LA CIUDAD DE MÉXICO</b>, los cuales serán protegidos conforme a lo dispuesto por la
                        Ley de Protección de Datos Personales en posesión de Sujetos Obligados de la Ciudad de México, y demás
                        normativa aplicable.</p>
                    <p>
                        Los datos recabados se utilizarán con la finalidad de promover una administración escolar eficiente, que apoye
                        la labor educativa del Instituto de Estudios Superiores de la Ciudad de México “Rosario Castellanos” con
                        beneficios para la Secretaría de Educación, Ciencia, Tecnología e Innovación de la Ciudad de México, y con
                        ello facilitar la coordinación en la prestación de los servicios educativos de nivel superior relativos a la
                        admisión, inscripción, reinscripción, inscripción en años posteriores, elaboración de identificaciones, así como
                        la administración responsable de la información proporcionada por las personas de la Comunidad Estudiantil,
                        Comunidad Académica, Egresados, Usuarios de Servicios Educativos Complementarios, Servidores Públicos
                        del Instituto de Estudios Superiores de la Ciudad de México “Rosario Castellanos”; para contar con un
                        Sistema de Administración Escolar que apoye los procesos de gestión escolar y administrativa; facilitar la
                        prestación de los servicios educativos de tipo superior relativos al registro, proceso de admisión, programa
                        para el ingreso al Instituto de Estudios Superiores de la Ciudad de México “Rosario Castellanos”, la
                        publicación de resultados, proceso de inscripción, reinscripción, ingreso en años posteriores, baja temporal,
                        baja definitiva, cancelación o anulación de registro, acreditación y certificación de estudios de estudiantes;
                        otorgamiento del seguro facultativo para estudiantes; expedición de documentación de los estudiantes;
                        elaboración de estudios y cuestionarios de carácter educativo, socioeconómico y de salud; desarrollo de
                        actividades extracurriculares de fortalecimiento académico; para trámites de asignación de becas o apoyos que
                        en su caso procedan; para desahogo de procedimientos derivados de la aplicación de instrumentos jurídicos
                        internos; elaboración de informes, evaluaciones, revisiones respecto de los programas educativos de tipo
                        superior de la Ciudad de México; para la equivalencia y revalidación de estudios; lo mismo cualquier finalidad
                        necesaria para el cumplimiento de los objetivos del Instituto.
                    </p>
                    <p>
                        Para cumplir con las finalidades señaladas podrán solicitarse: Datos Personales de la Comunidad Estudiantil y
                        Egresados: Datos Identificativos: Nombre completo, Domicilio, Edad, Estado civil, Fecha de nacimiento,
                        Firma, Clave de elector (alfa-numérico anverso credencial INE), Clave de elector (alfa-numérico anverso
                        credencial INE), Clave del Registro Federal de Contribuyentes (RFC), Clave Única de Registro de Población
                        (CURP), Folio nacional (anverso credencial del INE), Número identificador OCR (reverso de la credencial
                        IFE), Fotografía, Género, Lugar de nacimiento, Grupo Étnico, Lengua Matrícula del Servicio Militar
                        Nacional, Nacionalidad, Número de Licencia de Conducir, Número de Pasaporte, Número de Seguro Social,
                        Número de Visa, Teléfono celular, Teléfono particular. Datos Biométricos: Huellas Digitales. Datos
                        Electrónicos: Correo electrónico no oficial, Dirección IP. Datos Académicos: Cédula Profesional. Datos
                        Patrimoniales: Bienes inmuebles, Cuentas bancarias, Gravámenes o adeudos, Otro tipo de valores e
                        inversiones, Servicios contratados. Datos Personales de la Comunidad Académica y Servidores Públicos:
                        Datos Identificativos: Nombre completo, Domicilio, Edad, Estado civil, Fecha de nacimiento, Firma, Folio
                        nacional (anverso credencial del INE), Clave de elector (alfa-numérico anverso credencial INE), Clave del
                        Registro Federal de Contribuyentes (RFC), Clave Única de Registro de Población (CURP), Número
                        identificador OCR (reverso de la credencial INE), Fotografía, Género, Lugar de nacimiento, Grupo Étnico,
                        Lengua, Matrícula del Servicio Militar Nacional, Nacionalidad, Número de Licencia de Conducir, Número de
                        Pasaporte, Número de Seguro Social, Número de Visa, Teléfono celular, Teléfono particular. Datos Afectivos
                        y/o familiares: Nombres de familiares, Parentesco. Datos Biométricos: Huellas Digitales. Datos Electrónicos:
                        Correo electrónico no oficial. Datos Académicos: Calificaciones, Cédula Profesional, Certificados y
                        reconocimientos, Escolaridad, Grado académico, Grupo escolar, Matrícula escolar, Nombre del plantel
                        educativo, Títulos, Trayectoria educativa. Datos Laborales: Actividades extracurriculares, Capacitación,
                        Cargo, Nombramiento, Ocupación, Reclutamiento y selección, Referencias laborales, Resultados de la evaluación del desempeño, Trayectoria laboral. Datos Tránsito y movimiento migratorio: Calidad Migratoria,
                        Información relativa al tránsito de las personas dentro del país. Datos Personales correspondientes a los
                        Servicios Educativos Complementarios: Datos Identificativos: Nombre completo, Domicilio, Edad, Estado
                        civil, Fecha de nacimiento, Firma, Folio nacional (anverso credencial del INE), Clave de elector (alfanumérico anverso credencial INE), Clave del Registro Federal de Contribuyentes (RFC), Clave Única de
                        Registro de Población (CURP), Número identificador OCR (reverso de la credencial INE), Fotografía,
                        Género, Lugar de nacimiento, Grupo Étnico, Lengua, Matrícula del Servicio Militar Nacional, Nacionalidad,
                        Número de Licencia de Conducir, Número de Pasaporte, Número de Visa, Teléfono celular, Teléfono
                        particular. Datos Afectivos y/o familiares: Nombres de familiares, Parentesco. Datos Biométricos: Huella
                        digital. Datos Electrónicos: Correo electrónico no oficial. Datos Académicos: Calificaciones, Cédula
                        Profesional, Certificados y reconocimientos, Escolaridad, Grado académico, Grupo escolar, Matrícula escolar,
                        Nombre del plantel educativo, Títulos, Trayectoria educativa. Datos Laborales: Actividades extracurriculares,
                        Capacitación, Cargo, Nombramiento, Ocupación, Trayectoria laboral. Datos Tránsito y movimiento
                        migratorio. Además, se recabarán los siguientes Datos Sensibles: Huella digital. Respecto del ciclo de vida de
                        los datos personales, estos recibirán tratamiento en tanto sean necesarios para el cumplimiento de las
                        finalidades previstas en el presente aviso de privacidad y sujetándose a las disposiciones administrativas,
                        contables, fiscales, jurídicas, archivísticas e históricas aplicables en la Ciudad de México.
                    </p>
                    <p>
                        El fundamento para el tratamiento de datos personales son los Artículos siguientes: 2 Fracciones XXIX y
                        XXXVI, 4, 17, 18, 22, 23 Fracciones XII y XIII, 20, 21, 22, 24, 26, 36, 37, 38, 39 y 40 de la Ley de Protección
                        de Datos Personales en Posesión de Sujetos Obligados de la Ciudad de México, 6 Fracciones XII y XXII, 7,
                        10, 21 y 24 Fracción XXIII de la Ley de Transparencia, Acceso a la Información Pública y Rendición de
                        Cuentas de la Ciudad de México; 32 de la Ley Orgánica del Poder Ejecutivo y la Administración Pública de la
                        Ciudad de México; 23, 68 y 69 de la Ley General de Transparencia y Acceso a la Información Pública; 1, 3
                        Fracción IX, 30 Fracciones VI y VII, 31, 32, 33, 34, 35 Fracciones VI, 37, 38 y 40 de la Ley de Archivos del
                        Distrito Federal; 7 Fracción VII, 32, Fracciones XXXI, XXXIV, XXXV y XXXIX, 302 BIS, 302 QUATER
                        Fracciones VII, XV, XXVII, XXVIII y XXX del Reglamento Interior del Poder Ejecutivo y de la
                        Administración Pública de la Ciudad de México; 1, 2, 4 Fracción II, 9 Fracción IX último párrafo, 47, 48, 51,
                        114 Fracción VIII, IX de la Ley General de Educación; 1, 2 Fracción I, 4, 5, 6, 13 Fracciones I, II, III, V y
                        XXXI, 14 Fracción I, 15 Fracción III, 36 Fracción IV, 60, 120 Fracción I y 141 de la Ley de Educación para el
                        Distrito Federal; 7, 33, 34, 44, 79, 103, 111 y 112 Ley de Procedimiento Administrativo de la Ciudad de
                        México; 141 de la Ley General de Educación, 13 de la Ley de Educación del Distrito Federal, el Decreto por
                        el que se crea el Instituto de Estudios Superiores de la Ciudad de México “Rosario Castellanos” y el Estatuto
                        General del Instituto.
                    </p>

                    <p>
                        Para ejercer los derechos de acceso, rectificación, cancelación y oposición, así como la revocación del
                        consentimiento deberá ingresar a la página web <a href="http://https://www.rcastellanos.cdmx.gob.mx/transparencia" target="_blank" rel="noopener noreferrer">https://www.rcastellanos.cdmx.gob.mx/transparencia</a> para
                        obtener los formatos respectivos o bien acudir a la Jefatura de Unidad Departamental de Atención Ciudadana y
                        Transparencia del Instituto de Estudios Superiores “Rosario Castellanos” sita Avenida 506 Eje 3 Norte S/N
                        Esquina Loreto Fabela, Colonia San Juan de Aragón Sección II, Alcaldía Gustavo A. Madero, Ciudad de
                        México C.P. 07979, Ciudad de México, al correo electrónico <a href="mailto:transparenciairc@sectei.cdmx.gob.mx">transparenciairc@sectei.cdmx.gob.mx</a>.
                    </p>
                    <p>
                        Las modificaciones al presente aviso estarán disponibles en
                        <a href="http://https://www.rcastellanos.cdmx.gob.mx/transparencia" target="_blank" rel="noopener noreferrer">https://www.rcastellanos.cdmx.gob.mx/transparencia</a> apartado Avisos Integrales de Protección de Datos.
                    </p>
                    <div class="text-center text-bold">
                        <p>Ciudad de México, a 14 de septiembre del 2020</p>
                        <p>DOCTORA ROSAURA RUIZ GUTIÉRREZ</p>
                        <br>
                        <p>SECRETARIA DE EDUCACIÓN, CIENCIA,</p>
                        <p>TECNOLOGÍA E INNOVACIÓN DE LA CIUDAD DE MÉXICO</p>
                    </div>

                </div>

                <div class="modal-footer">

                    <button type="button" class="aceptar" data-dismiss="modal" style="background-color: #e76969;"><b>Aceptar</b></button>

                </div>
            </div>

        </div>
    </div>
    <script>
        $(document).ready(function() {

            $("#myModal1").modal();
        });
    </script>