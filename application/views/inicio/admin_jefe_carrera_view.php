<section class="row mb-5" >
    <div class="col-12" >
    <div class="row img_baner">
        <div class="container-fluid contenedor-banner">
            <div class="row">
                <div class="col-12 col-md-6 m-auto">
                    <h1 class="text-banner">Sistema Adminitración para Jefes de carrera</h1>
                </div>
                <div class="col-12 col-md-6 m-auto" >
                    <img src="<?php echo base_url('/assets/img/icono_jefe_carrera.png'); ?>" alt="Personajes" class="img-personajes">
                </div>
            </div>
        </div>
    </div>
    </div>
</section>
<div class="container mt-4">
    <section class="row">
        <article class="col-12">
            <header class="row">
                <div class="col-12">
                    <div class="contenedorTitulo">
                        <h1 class="text-center mb-5 mobile">Sistema Adminitraci&oacute;n.</h1>
                    </div>
                </div>
            </header>
            <section class="row">
                <div class="col-12 col-sm-8 offset-sm-2 formatoParrafo">
                    <h2 class="mb-5">
                        Apreciable Jefe de Carrera:
                    </h2>
                    <p class="text-justify">Ponemos a su disposici&oacute;n el Sistema de Administración de Actas
                        para LAD del Instituto de Estudios Superiores de la Ciudad de México "Rosario Castellanos"; a través del cual podrá consultar la
                        información relacionada a grupos y docentes, así como a las actas del ciclo escolar.</p>
                    <p class="text-justify">Le invitamos a revisar la <a target="_blank" href="https://drive.google.com/file/d/1IczxJwJoWQfwRXI9ypgoacgq37ufhRwL/view"  class="letra_naranja font-weight-bold">Guía de uso del Sistema</a>, 
                        si tiene alguna duda o se presenta un incidente, recuerde que en <a target="_blank" class="letra_naranja font-weight-bold" href="http://app.rcastellanos.cdmx.gob.mx/mesadeayuda/inicio">Mesa de Ayuda </a> estamos para atenderle.</p>

                </div>
            </section>
        </article>
    </section>



</div>
<div class="col-md-12 mb-2">
    <div class="text-center">
        <a class="btn btn-lg text-white naranja"  href="<?php echo base_url('acceso/Login/login_jefe_carrera') ?>">
            <i class="fas fa-chevron-circle-right"></i> Continuar
        </a>
    </div>

</div>

</div>

