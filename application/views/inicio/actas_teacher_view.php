<section class="row mb-5" style="background-color: blue; margin:0% !important;">
	<div class="col-12">
		<div class="row img_baner">
			<div class="container-fluid contenedor-banner">
				<div class="row">
					<div class="col-12 col-md-6 m-auto">
						<h1 class="text-banner">Sistema de Actas</h1>
					</div>
					<div class="col-12 col-md-6 m-auto">
						<img src="<?php echo base_url('/assets/img/icono_docente.png'); ?>" alt="Personajes"
							 class="img-personajes">
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<div class="container mt-4">
	<section class="row">
		<article class="col-12">
			<header class="row">
				<div class="col-12">

				</div>
			</header>
			 
			<section class="row">
				<div class="col-12 col-sm-8 offset-sm-2 formatoParrafo">
					<h2 class="mb-4">
						Apreciables docentes:
					</h2>
					<p>En el Instituto de Estudios Superiores de la Ciudad de México “Rosario Castellanos”, nos
						encontramos transitando a medios digitales para los procesos académicos administrativos, como
						parte de esta nueva dimensión de trabajo, por lo que ponemos a su disposición el Sistema de
						Actas LAD y nos permitimos brindarle las siguientes orientaciones.</p>
					<ul>
						<li>Considere que el usuario y contraseña, son los mismos que utiliza para ingresar a las aulas
							virtuales.
						</li>
						<li>Tenga a la mano, la Firma Digital Institucional (FDI) y la contraseña que se le
							proporcionó. En caso de no contar con ellas, contacté con su Jefe de carrera.
						</li>
						<li>Verifique el calendario de acceso, ya que será programado según la inicial de su primer
							apellido.
						</li>
						<li>Una vez firmada un acta no podrá modificarla, ya que este proceso lo podrá realizar en el
							periodo de rectificación de calificaciones, por lo que le sugerimos revisar las fechas
							programadas para ello.
						</li>
						<li>Por último, descargue el o las actas firmadas y manténgalas a buen resguardo.</li>
					</ul>
					<p>Te invitamos a revisar la <a target="_blank"
													href="https://drive.google.com/file/d/1Zs43BT-Q2E3bW2JIae8Ky79u1wQTFlaC/view"
													class="letra_naranja">Guía de uso del Sistema</a>,
						si tiene alguna duda o se presenta un incidente, recuerda que en <a target="_blank"
																							class="letra_naranja"
																							href="http://app.rcastellanos.cdmx.gob.mx/mesadeayuda/inicio">Mesa
							de Ayuda </a> estamos para atenderte.</p>

					<hr class="hr_teacher">
				</div>
			</section>
		</article>
	</section>
	<!-- empieza windget -->
	<div class="container  text-center">
		<h2>Registro de calificaciones y consulta de calendarios.</h2>
		<!-- mosaicos nuevos -->

		<div class="mosaico_prof h2_mosaico_prof  text-center">
			<?php $clase_1 = '';
			date_today() >= calendario_teacher_1_fecha_ini() && date_today() <= calendario_teacher_1_fecha_fin() ? FALSE : $clase_1 = 'disabled-calendar' ?>
			<div class="details <?php echo $clase_1; ?> x-auto">
				<div class="category"><h2 class="" href=""></h2>
					<p class="h2_mosaico_prof">Calendario</p></div>
				<div>
					<img src="<?php echo base_url(); ?>assets/img/bloque1.png" class="img-responsive">
				</div>
				<h3><a class="h2_mosaico_prof" href="<?php echo base_url('inicio_index/calendarios?calendary=1'); ?>">Primer
						bloque</a></h3>

			</div>
		</div>

		<div class="mosaico_prof h2_mosaico_prof  text-center">
		
			<?php $clase_2 = '';
			date_today() >= calendario_teacher_2_fecha_ini() && date_today() <= calendario_teacher_2_fecha_fin() ? FALSE : $clase_2 = 'disabled-calendar' ?>
			<div class="details <?php echo $clase_2; ?> x-auto">
				<div class="category"><h2 class="" href=""></h2>
					<p class="h2_mosaico_prof">Calendario</p></div>
				<div>
					<img src="<?php echo base_url(); ?>assets/img/bloque2.png" class="img-responsive">
				</div>
				<h3><a class="h2_mosaico_prof" href="<?php echo base_url('inicio_index/calendarios?calendary=2'); ?>">Segundo
						bloque</a></h3>

			</div>
		</div>

		<div class="mosaico_prof h2_mosaico_prof  text-center">
			<?php $clase_5 = '';
			date_today() >= calendario_teacher_5_fecha_ini() && date_today() <= calendario_teacher_5_fecha_fin() ? FALSE : $clase_5 = 'disabled-calendar' ?>
			<div class="details <?php echo $clase_5; ?> x-auto">
				<div class="category"><h2 class="" href=""></h2>
					<p class="h2_mosaico_prof">Calendario</p></div>
				<div>
					<img src="<?php echo base_url(); ?>assets/img/extraordinarios.png" class="img-responsive">
				</div>
				<h3><a class="h2_mosaico_prof" href="<?php echo base_url('inicio_index/calendarios?calendary=5'); ?>">Extraordinarios</a>
				</h3>

			</div>
		</div>

		<!-- termina -->


	</div>
</div>


</div>
<div class="container  text-center">
<h2 class="justify-content-center">Rectificación de calificaciones y consulta de calendarios.</h2>	
</div>
<!-- empieza windget -->

<div class="row bootstrap snippets bootdey justify-content-center alinear_flex mt-5 mb-5">
<!-- mosaicos nuevos -->

	<div class="col-md-4 mosaico_prof_rectificacion h2_mosaico_prof_r text-center">
		<?php $clase_4 = '';
		//(date_today() >= calendario_teacher_4_fecha_ini() && date_today() <= calendario_teacher_4_fecha_fin()) ? FALSE : $clase_4 = 'disabled-calendar' 
		(date_today() >= calendario_teacher_7_fecha_ini() && date_today() <= calendario_teacher_7_fecha_fin()) ? FALSE : $clase_7 = 'disabled-calendar' ?>
		<div class="details <?php echo $clase_4; ?> x-auto">

			<!--Se pone calendario 7 porque el calendario 4 no deja pasar -->
			<h3><a class="h2_mosaico_prof_r" href="<?php echo base_url('inicio_index/calendarios?calendary=7'); ?>">Bloques</a>
			</h3>

		</div>
	</div>

	<div class="col-md-4 mosaico_prof_rectificacion h2_mosaico_prof_r  text-center">
		<?php $clase_7 = '';
		(date_today() >= calendario_teacher_7_fecha_ini() && date_today() <= calendario_teacher_7_fecha_fin()) ? FALSE : $clase_7 = 'disabled-calendar' ?>
		<div class="details <?php echo $clase_7; ?> x-auto">

			<h3><a class="h2_mosaico_prof_r" href="<?php echo base_url('inicio_index/calendarios?calendary=7'); ?>">Extraordinarios</a>
			</h3>

		</div>
	</div>


	<!-- termina -->


</div>
