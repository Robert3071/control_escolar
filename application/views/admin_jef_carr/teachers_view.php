<div class="content-wrapper" id="app">
    <div class="container">
        <div class="row col-md-12">
            <h1 class="text-center alert titulo_historial">Docentes</h1>
        </div>
        <div class="row" style="border: 1px solid #fff;">
            <div class="col-12 col-md-12">
             <div class="row">
                    <div class="col-12 col-md-6 d-flex justify-content-sm-center">
                        <img src="<?php echo base_url('/assets/img/logo_CDMX.png'); ?>" class="img-fluid logo_mobile" alt="Logo">
                    </div>
                    <div class="col-12  col-md-6 text-center">
                        <p class="text-secondary mt-5 titulo_mobile titulo_mobile m-0">Secretaria de Educación, Ciencia,
                            Tecnología e Innovación de la Ciudad de México</p>
                        <p class="text-secondary titulo_mobile m-0">Instituto de Estudios Superiores de la Ciudad de México</p>
                        <p class="text-secondary titulo_mobile m-0">“Rosario Castellanos” </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container pt-5">
        <div class="row justify-content-center">

            <table id="example" class="display table-responsive" style="width:100%">
                <thead>
                    <tr>
                        
                        <th>Nombres</th>
                        <th>Apellidos</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($teachers as $t) : ?>
                        <tr>
                            <td><?php echo $t['name'] ?></td>
                            <td><?php echo $t['surnames'] ?></td>

                            <td>
                            <a class="btn boton_ver_profe" href="<?php echo base_url() . 'session/admin_jef_carr/Admin_history_teacher/detalle_teacher/?ID_teacher=' . $t['ID_teacher'] ?>">Ver detalle</a>
                               

                            </td>

                        </tr>
                    <?php endforeach; ?>
                </tbody>
               
            </table>
            
        </div>
    </div>
</div>