
<div class="content-wrapper" id="app">
    <div class="container">
        <div class="row col-md-12">
            <h1 class="text-center alert titulo_historial">Grupos</h1>
        </div>
        <div class="row" style="border: 1px solid #fff;">
            <div class="col-12 col-md-12">
                <div class="row">
                    <div class="col-12 col-md-6 d-flex justify-content-sm-center">
                        <img src="<?php echo base_url('/assets/img/logo_CDMX.png'); ?>" class="img-fluid logo_mobile" alt="Logo">
                    </div>
                    <div class="col-12  col-md-6 text-center">
                        <p class="text-secondary mt-5 titulo_mobile titulo_mobile m-0">Secretaria de Educación, Ciencia,
                            Tecnología e Innovación de la Ciudad de México</p>
                        <p class="text-secondary titulo_mobile m-0">Instituto de Estudios Superiores de la Ciudad de México</p>
                        <p class="text-secondary titulo_mobile m-0">“Rosario Castellanos” </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="col-lg-12 col-md-12">
            <table id="example" class="display nowrap  table-responsive" style="width:100%">
                <thead>
                    <tr>
                        <th>Profesor</th>
                        <th>Clave de grupo</th>
                        <th>Clave asignatura</th>
                        <th>Asignatura</th>
                        <th>A&ntilde;o</th>
                        <th>Ciclo</th>
                        <th>Tipo de grupo</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (count($grupos) > 0) { ?>
                        <?php foreach ($grupos as $g): ?>
                            <tr>
                                <td><?php echo $g['name'] . ' ' . $g['surnames'] ?></td>
                                <td><?php echo $g['group_name'] ?></td>
                                <td><?php echo $g['key_curse'] ?></td>
                                <td><?php echo $g['subject_name'] ?></td>
                                <td><?php echo $g['year_active'] ?></td>
                                <td><?php echo $g['cycle'] ?></td>
                                <td><?php echo $g['type_group'] ?></td>
                                <td><?php
                                    if ($g['qualified'] == 1 && $g['rectificated'] == 0) {
                                        echo '<form method="GET" action=' . base_url() . 'session/admin_jef_carr/Admin_dashboard/rectificar_grupo>';
                                        echo '<input name="ID_teacher_by_group" type="hidden" value=' . $g['ID_teacher_by_group'] . '>';
                                        echo '<button type="submit" class="btn boton_ver_profe mt-2">Rectificar</button>';
                                        echo '</form>';
                                        echo '<form method="GET" action=' . base_url() . 'session/admin_jef_carr/Admin_dashboard/pdf_acta>';
                                        echo '<input name="ID_teacher_by_group" type="hidden" value=' . $g['ID_teacher_by_group'] . '>';
                                        echo '<button type="submit" class="btn boton_admin_llave mt-2">Acta</button>';
                                        echo '</form>';
                                    } else if ($g['rectificated'] == 1) {
                                        echo '<button  class="btn btn-secondary mt-2"  " >En proceso de Rectificaci&oacute;n</button>';
//                                    echo '<form method="POST" action=' . base_url() . 'session/admin_jef_carr/Admin_dashboard/desrectificar_grupo>';
//                                    echo '<input name="ID_teacher_by_group" type="hidden" value=' . $g['ID_teacher_by_group'] . '>';
//                                    echo '<button type="submit" class="btn btn-danger mt-2"  data-toggle="tooltip" data-placement="top" title="Desrectifica al grupo y a todos los alumnos ">Desrectificar Grupo</button>';
//                                    echo '</form>';
//                                    echo '<form method="GET" action=' . base_url() . 'session/admin_jef_carr/Admin_dashboard/desrectificar_alumnos_by_grupo>';
//                                    echo '<input name="ID_teacher_by_group" type="hidden" value=' . $g['ID_teacher_by_group'] . '>';
//                                    echo '<button type="submit" class="btn btn-secondary mt-2"  data-toggle="tooltip" data-placement="top" title="Desrectifica a alumnos seleccionados" >Desrectificar Alumnos</button>';
//                                    echo '</form>';
                                        echo '<form method="GET" action=' . base_url() . 'session/admin_jef_carr/Admin_dashboard/pdf_acta>';
                                        echo '<input name="ID_teacher_by_group" type="hidden" value=' . $g['ID_teacher_by_group'] . '>';
                                        echo '<button type="submit" class="btn boton_admin_llave mt-2">Acta</button>';
                                        echo '</form>';
                                    } else {
                                        ?><p>El grupo no ha sido calificado</p><?php
                                    }
                                    ?>    
                                </td>

                            </tr>
                        <?php
                        endforeach;
                    } else {
                        ?>
                        <tr>
                            <td scope="row">Sin datos </td>
                        </tr>
                    <?php }
                    ?>
                </tbody>
               
            </table>
        </div>
    </div>
</div>