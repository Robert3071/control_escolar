<div class="content-wrapper" id="app">
    <div class="container">
        <div class="row col-md-12">
            <h1 class="text-center alert titulo_historial">Estatus de actas</h1>
        </div>
        <div class="row" style="border: 1px solid #fff;">
            <div class="col-12 col-md-12">
                <div class="row">
                    <div class="col-12 col-md-6 d-flex justify-content-sm-center">
                        <img src="<?php echo base_url('/assets/img/logo_CDMX.png'); ?>" class="img-fluid logo_mobile" alt="Logo">
                    </div>
                    <div class="col-12  col-md-6 text-center">
                        <p class="text-secondary mt-5 titulo_mobile titulo_mobile m-0">Secretaria de Educación, Ciencia,
                            Tecnología e Innovación de la Ciudad de México</p>
                        <p class="text-secondary titulo_mobile m-0">Instituto de Estudios Superiores de la Ciudad de México</p>
                        <p class="text-secondary titulo_mobile m-0">“Rosario Castellanos” </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="col-12">
            <div class="card border-card" v-if="campus">

                <div class="card-header border-card-header">

                    <div class="row justify-content-center">

                        <div class="m-2">
                            <h2 class="card-title"><strong>A&ntilde;o:</strong> <?php echo anio() ?></h2>
                            <h2 class="card-title"><strong>Ciclo:</strong> <?php echo ciclo() ?></h2>
                        </div>
                    </div>
                        
                        <div class="row justify-content-center">
                        <div class="m-2">
                            <h2 class="card-title"><strong>Unidad acad&eacute;mica:</strong> {{campus.campus_name}} </h2>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="m-2">
                            <h2 class="card-title"><strong>Licenciatura:</strong> {{campus.career_name}}</h2>
                        </div>
                        
                    </div>

                </div>
                <div class="card-body">

                    <div class="row  justify-content-center p-5 " >

                        <!-- Mosaico -->
                        <div class="col-6 col-md-6 col-lg-6 d-flex justify-content-center">                
                    
                            <div class="mosaico" >
                            <div class="details justify-content-center">
                                        <div class="category"><h2 class="h2_mosaico" href="">{{campus.calificados}}/{{campus.total}}</h2> <p class="">Calificados</p></div>
                                        
                                        <a class="btn naranja letra_boton" href="<?php echo base_url() . 'session/admin_jef_carr/Admin_status_actas/detalle_campus_career' ?>">Ver detalle</a>
                                    </div>
                            </div>
                        </div>
                        <!--Termina mosaico -->

                        
                    </div>
                </div>
            </div>
            <div class="alert alert-warning mt-3" role="alert" v-else>
                No hay grupos asignados
            </div>
        </div>
    </div>
</div>