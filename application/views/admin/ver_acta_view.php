<div class="content-wrapper" id='app'>
    <div class="container">
        <div class="row col-md-12">
            <h1 class="text-center alert titulo_historial">Calificar Grupos</h1>
        </div>
        <div class="row" style="border: 1px solid #fff;">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-12 col-md-6 d-flex justify-content-sm-center">
                        <img src="<?php echo base_url('/assets/img/logo_CDMX.png'); ?>" class="img-fluid logo_mobile" alt="Logo">
                    </div>
                    <div class="col-12  col-md-6 text-center">
                        <p class="text-secondary mt-5 titulo_mobile titulo_mobile m-0">Secretaria de Educación, Ciencia,
                            Tecnología e Innovación de la Ciudad de México</p>
                        <p class="text-secondary titulo_mobile m-0">Instituto de Estudios Superiores de la Ciudad de México</p>
                        <p class="text-secondary titulo_mobile m-0">“Rosario Castellanos” </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row col-md-12">
            <div class="col-md-2">
                <p>A&ntilde;o: <?php echo $datos_grupo[0]['year_active']; ?></p>
            </div>
            <div class="col-md-2">
                <p>Ciclo: <?php echo $datos_grupo[0]['cycle']; ?></p>
            </div>
            <div class="col-md-3">
                <p>Docente: <?php  echo ucwords(strtolower($datos_grupo[0]['name'] . ' ' . $datos_grupo[0]['surnames']));?></p>
            </div>
            <div class="col-md-2">
                <p>Grupo: <?php echo $datos_grupo[0]['group_name']; ?></p>
            </div>
            <div class="col-md-3">
                <p>Asignatura: <?php echo $datos_grupo[0]['subject_name']; ?></p>
            </div>
        </div>
        <div class="row ">

            <div class="col-md-12 col-lg-12 col-sm-10  " width="100%">
                <table class="table table-responsive-sm table-bordered p-3">
                    <thead>
                        <tr class="table-success">
                            <th>No</th>
                            <th>Matr&iacute;cula estudiante</th>
                            <th>Nombre del estudiante</th>
                            <th class="text-center">Calificaci&oacute;n final N&uacute;mero</th>
                            <th class="text-center">Calificaci&oacute;n final Letra</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $sub = 0; ?>
                        <?php foreach ($grupo as $key => $g) : ?>
                            <tr>
                                <td><?php echo ($key + 1) ?></td>
                                <td><?php echo $g['accountNumber'] ?></td>
                                <td><?php echo $g['names'] . ' ' . $g['surnames'] ?></td>
                                <td> <?php echo $g['score'] == 'NP' ? '-' : $g['score']; ?></td>
                                <td><?php num_to_text($g['score']) ?> </td>
                            </tr>
                            <?php $sub++; ?>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <div class="col-12 d-flex justify-content-center">
                <a href="<?php echo base_url('session/admin/Validacion/'); ?>" class="btn btn-success d-flex align-items-center" type="submit">Regresar</a>
        </div>
        </div>
    </div>
</div>