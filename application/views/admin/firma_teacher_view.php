<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.5/css/buttons.dataTables.min.css">
<div class="content-wrapper">
    <div class="container">
        <div class="row col-md-12">
            <h1 class="text-center alert titulo_historial">Descargar firma</h1>
        </div>
        <div class="row" style="border: 1px solid #fff;">
            <div class="col-12 col-md-12">
                  <div class="row">
                    <div class="col-12 col-md-6 d-flex justify-content-sm-center">
                        <img src="<?php echo base_url('/assets/img/logo_CDMX.png'); ?>" class="img-fluid logo_mobile" alt="Logo">
                    </div>
                    <div class="col-12  col-md-6 text-center">
                        <p class="text-secondary mt-5 titulo_mobile titulo_mobile m-0">Secretaria de Educación, Ciencia,
                            Tecnología e Innovación de la Ciudad de México</p>
                        <p class="text-secondary titulo_mobile m-0">Instituto de Estudios Superiores de la Ciudad de México</p>
                        <p class="text-secondary titulo_mobile m-0">“Rosario Castellanos” </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

                
    <div class="container">
        <div class="row justify-content-sm-center mt-5">
            <p class="font-weight-bold">Profesor:<?php echo ' '.$data_groups[0]['name'].' '.$data_groups[0]['surnames']; ?></p>
            
        </div>
        <div class="row justify-content-sm-center">
            
            <p class="font-weight-bold">Email:<?php echo ' '.$data_groups[0]['email']; ?></p>
            
        </div>
        <div class="row justify-content-sm-center">
           
            <p>El usuo de la firma es personal e intransferible.</p>
        </div>
        <div class="row justify-content-sm-center">
            <a class="btn btn_turquesa m-5" href="<?php echo base_url('session/admin/Admin_history_teacher/descargar_fiel?ID_teacher='.$data_groups[0]['ID_teacher']);?>">Descargar firma</a>
        </div>
        <a class="btn regresar_admin" href="<?php echo base_url('session/admin/Admin_history_teacher/')?>"><i class="fas fa-angle-left m-5"></i>Regresar</a>
    </div>
</div>