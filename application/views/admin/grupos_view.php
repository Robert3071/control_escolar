<div class="content-wrapper" id="app">
    <div class="container">
        <div class="row col-md-12">
            <h1 class="text-center alert titulo_historial">Grupos</h1>
        </div>
        <div class="row" >
            <div class="col-12 col-md-12 m-2">
                  <div class="row">
                    <div class="col-12 col-md-6 d-flex justify-content-sm-center">
                        <img src="<?php echo base_url('/assets/img/logo_CDMX.png'); ?>" class="img-fluid logo_mobile" alt="Logo">
                    </div>
                    <div class="col-12  col-md-6">
                        <p class="text-secondary  titulo_mobile m-0">Secretaria de Educación, Ciencia,
                            Tecnología e Innovación de la Ciudad de México</p>
                        <p class="text-secondary titulo_mobile m-0">Instituto de Estudios Superiores de la Ciudad de México</p>
                        <p class="text-secondary titulo_mobile m-0">“Rosario Castellanos” </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="col-lg-12 col-md-12">
            <h5>Exportar:</h5>
            <table id="example" class="display nowrap  table-responsive" style="width:100%">
                <thead>
                    <tr>
                        <th>Docente</th>
                        <th>Clave de grupo</th>
                        <th>Clave asignatura</th>
                        <th>Nombre de la asignatura</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($grupos as $g) : ?>
                        <tr>
                            <td><?php echo $g['name'] . ' ' . $g['surnames'] ?></td>
                            <td><?php echo $g['group_name'] ?></td>
                            <td><?php echo $g['key_curse'] ?></td>
                            <td><?php echo $g['subject_name'] ?></td>
                            <td><?php
                                if ($g['qualified'] == 1 && $g['rectificated'] == 0 && isset($_SESSION['id_type']) && $_SESSION['id_type']==3) {
                                    echo '<form method="GET" action=' . base_url() . 'session/admin/Admin_dashboard/rectificar_grupo>';
                                    echo '<input name="ID_teacher_by_group" type="hidden" value=' . $g['ID_teacher_by_group'] . '>';
                                    echo '<button type="submit" class="btn boton_ver_profe mt-2">Rectificar</button>';
                                    echo '</form>';
                                    echo '<form method="GET" action=' . base_url() . 'session/admin/Admin_dashboard/pdf_acta>';
                                    echo '<input name="ID_teacher_by_group" type="hidden" value=' . $g['ID_teacher_by_group'] . '>';
                                    echo '<button type="submit" class="btn boton_admin_llave mt-2">Acta</button>';
                                    echo '</form>';
                                } else if ($g['qualified'] == 1 && $g['rectificated'] == 0 && isset($_SESSION['id_type']) && $_SESSION['id_type']==5) {
                                    echo '<form method="GET" action=' . base_url() . 'session/admin/Admin_dashboard/pdf_acta>';
                                    echo '<input name="ID_teacher_by_group" type="hidden" value=' . $g['ID_teacher_by_group'] . '>';
                                    echo '<button type="submit" class="btn boton_admin_llave mt-2">Acta</button>';
                                    echo '</form>';
                                } else if ($g['rectificated'] == 1) {
                                    echo '<button  class="btn btn-secondary mt-2"  " >En proceso de Rectificaci&oacute;n</button>';
//                                    echo '<form method="POST" action=' . base_url() . 'session/admin/Admin_dashboard/desrectificar_grupo>';
//                                    echo '<input name="ID_teacher_by_group" type="hidden" value=' . $g['ID_teacher_by_group'] . '>';
//                                    echo '<button type="submit" class="btn btn-danger mt-2"  data-toggle="tooltip" data-placement="top" title="Desrectifica al grupo y a todos los alumnos ">Desrectificar Grupo</button>';
//                                    echo '</form>';
//                                    echo '<form method="GET" action=' . base_url() . 'session/admin/Admin_dashboard/desrectificar_alumnos_by_grupo>';
//                                    echo '<input name="ID_teacher_by_group" type="hidden" value=' . $g['ID_teacher_by_group'] . '>';
//                                    echo '<button type="submit" class="btn btn-secondary mt-2"  data-toggle="tooltip" data-placement="top" title="Desrectifica a alumnos seleccionados" >Desrectificar Alumnos</button>';
//                                    echo '</form>';
                                    echo '<form method="GET" action=' . base_url() . 'session/admin/Admin_dashboard/pdf_acta>';
                                    echo '<input name="ID_teacher_by_group" type="hidden" value=' . $g['ID_teacher_by_group'] . '>';
                                    echo '<button type="submit" class="btn boton_admin_llave mt-2">Acta</button>';
                                    echo '</form>';
                                } else {
                                    ?><p>El grupo no ha sido calificado</p><?php
                                }
                                ?>    
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
                <tfoot>
                    <tr>
                        <th>Docente</th>
                        <th>Clave de grupo</th>
                        <th>Clave asignatura</th>
                        <th>Nombre de la Asignatura</th>
                        <th>Acciones</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
