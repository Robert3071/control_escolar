<div class="content-wrapper" id='app'>
    <div class="container">
        <div class="row col-md-12">
            <h1 class="text-center alert titulo_historial">Calificar Grupos</h1>
        </div>
        <div class="row" style="border: 1px solid #fff;">
            <div class="col-md-12">
                  <div class="row">
                    <div class="col-12 col-md-6 d-flex justify-content-sm-center">
                        <img src="<?php echo base_url('/assets/img/logo_CDMX.png'); ?>" class="img-fluid logo_mobile" alt="Logo">
                    </div>
                    <div class="col-12  col-md-6 text-center">
                        <p class="text-secondary mt-5 titulo_mobile titulo_mobile m-0">Secretaria de Educación, Ciencia,
                            Tecnología e Innovación de la Ciudad de México</p>
                        <p class="text-secondary titulo_mobile m-0">Instituto de Estudios Superiores de la Ciudad de México</p>
                        <p class="text-secondary titulo_mobile m-0">“Rosario Castellanos” </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row col-md-12 font-weight-bold m-2">
            <div class="col-md-3">
                <p>A&ntilde;o: <?php echo $datos_grupo[0]['year_active']; ?></p>
                <p>Ciclo: <?php echo $datos_grupo[0]['cycle']; ?></p>
            </div>
            
            <div class="col-md-3">
                <p>Docente: <?php echo $datos_grupo[0]['name']; ?></p>
                <p>Grupo: <?php echo $datos_grupo[0]['group_name']; ?></p>
                <p>Asignatura: <?php echo $datos_grupo[0]['subject_name']; ?></p>
            </div>

        </div>
        <div class="row ">
            <div class="col-md-12 col-lg-12 col-sm-10" width="100%">
                <form action="<?php echo base_url('session/admin/Admin_dashboard/activar_rectificar'); ?>" method="post">
                    <input hidden=" " name="id_teacher_by_group" type="text" value="<?php echo $datos_grupo[0]['ID_teacher_by_group']; ?>">
                    <table class="table table-striped tabla_rectificar p-3">
                        <thead>
                            <tr class="">
                                <th>No</th>
                                <th>Matr&iacute;cula estudiante</th>
                                <th>Nombre del estudiante</th>
                                <th>Rectificar</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $sub = 0; ?>
                            <?php foreach ($grupo as $key => $g) : ?>
                                <tr>
                                    <td><?php echo ($key + 1) ?></td>
                                    <td><?php echo $g['accountNumber'] ?></td>
                                    <td><?php echo $g['names'] . ' ' . $g['surnames'] ?></td>
                                    <td>
                                        <input name="check_lista[]" type="checkbox" id="<?php echo $g['ID_rating_student']; ?>" value="<?php echo $g['ID_rating_student']; ?>"> <label for="cbox2">Habilitar rectificación</label>

                                    </td>
                                </tr>
                                <?php $sub++; ?>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                    <div class="col-6 offset-6 ">
                    <button class="btn btn_turquesa" type="submit">Enviar</button>
                    </div>
                </form>
            </div>
            <div >
            <a class="btn regresar_admin" href="<?php echo base_url('session/admin/Admin_dashboard/grupos'); ?>"><i class="fas fa-angle-left m-2"></i>Regresar</a>
            </div>
        </div>
    </div>
</div>
</div>
<script type="text/javascript" src="<?php echo base_url('assets/registro/js/datos.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/registro/js/mostrar_pass.js'); ?>"></script>
<?php if ($this->session->flashdata('exito_rectificar')) : ?>
                <script>
                    window.onload = function() {

                        Swal.fire({
                            title: 'Se registr&oacute; correctamente el acta.',
                            icon: 'success',
                            confirmButtonText: 'Continuar',
                            //showCancelButton: true,
                            showCloseButton: true,
                            width: 400
                        });
                    };
                </script>
<?php endif; ?>

<?php if ($this->session->flashdata('error_rectificar')) : ?>
                <script>
                    window.onload = function() {

                        Swal.fire({
                            title: 'No se selecciono ningún campo.',
                            icon: 'error',
                            confirmButtonText: 'Continuar',
                            //showCancelButton: true,
                            showCloseButton: true,
                            width: 400
                        });
                    };
                </script>
<?php endif; ?>

<?php if ($this->session->flashdata('error_rectificar_bd')) : ?>
                <script>
                    window.onload = function() {

                        Swal.fire({
                            title: 'Ocurrio un error, consulta al administrador.',
                            icon: 'error',
                            confirmButtonText: 'Continuar',
                            //showCancelButton: true,
                            showCloseButton: true,
                            width: 400
                        });
                    };
                </script>
<?php endif; ?>
