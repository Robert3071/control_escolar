<div class="content-wrapper">
    <div class="container">
        <div class="row col-md-12">
            <h1 class="text-center alert titulo_historial">Profesor - Asignatura (s)</h1>
        </div>
        <div class="row" style="border: 1px solid #fff;">
            <div class="col-12 col-md-12">
                  <div class="row">
                    <div class="col-12 col-md-6 d-flex justify-content-sm-center">
                        <img src="<?php echo base_url('/assets/img/logo_CDMX.png'); ?>" class="img-fluid logo_mobile" alt="Logo">
                    </div>
                    <div class="col-12  col-md-6 text-center">
                        <p class="text-secondary mt-5 titulo_mobile titulo_mobile m-0">Secretaria de Educación, Ciencia,
                            Tecnología e Innovación de la Ciudad de México</p>
                        <p class="text-secondary titulo_mobile m-0">Instituto de Estudios Superiores de la Ciudad de México</p>
                        <p class="text-secondary titulo_mobile m-0">“Rosario Castellanos” </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="col-lg-12 col-md-12">
            <h5>Exportar:</h5>
            <table id="example" class="display" style="width:100%">
                <thead>
                    <tr>
                        <th>Licenciatura</th>
                        <th>Tipo de grupo</th>
                        <th>Clave asignatura</th>
                        <th>Asignatura</th>
                        <th>Estatus</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($data_groups as $g) : ?>
                        <tr>
                            <td><?php echo $g['career_name'] ?></td>
                            <td><?php echo $g['type_group'] ?></td>
                            <td><?php echo $g['key_curse'] ?></td>
                            <td><?php echo $g['subject_name'] ?></td>
                            <td><?php
                                if ($g['qualified'] == 1 && $g['rectificated'] == 0) {
                                    echo 'Calificado';
                                } elseif ($g['rectificated'] == 1 && $g['qualified'] == 1) {
                                    echo 'En proceso de rectificaci&oacute;n';
                                } else {
                                    echo 'Pendiente';
                                }
                                ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
