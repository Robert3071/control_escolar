<div class="content-wrapper" id="app">
    <div class="container">
        <div class="row col-md-12">
            <h1 class="text-center alert titulo_historial">Estatus actas</h1>
        </div>
        <div class="row" style="border: 1px solid #fff;">
            <div class="col-12 col-md-12">
                  <div class="row">
                    <div class="col-12 col-md-6 d-flex justify-content-sm-center">
                        <img src="<?php echo base_url('/assets/img/logo_CDMX.png'); ?>" class="img-fluid logo_mobile" alt="Logo">
                    </div>
                    <div class="col-12  col-md-6 text-center">
                        <p class="text-secondary mt-5 titulo_mobile titulo_mobile m-0">Secretaria de Educación, Ciencia,
                            Tecnología e Innovación de la Ciudad de México</p>
                        <p class="text-secondary titulo_mobile m-0">Instituto de Estudios Superiores de la Ciudad de México</p>
                        <p class="text-secondary titulo_mobile m-0">“Rosario Castellanos” </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="container">
        <div class="col-12">

            
                <div class="card-header ">

                    <div class="row justify-content-center">

                        <div style="margin-right: 15px;">
                            <h2 class="card-title">Año: <?php echo anio() ?></h2>
                        </div>

                        <div>
                            <h2 class="card-title">Ciclo: <?php echo ciclo() ?></h2>
                        </div>
                    </div>
                    
                    <div class="row justify-content-center">
                        <h3>Licenciaturas a Distancia </h3>
                    </div>
                
                <div class="container bootstrap snippets bootdey alinear_flex mt-5 mb-5">                
                    
                    <div class="mosaico" v-for='camp in career'>
                    <div class="details">
                                <div class="category"><h2 class="h2_mosaico" href="">{{camp.calificados}}/{{camp.total}}</h2> <p class="">REGISTRADOS</p></div>
                                <h3 class="{'bg-success' : camp.calificados===camp.total , 'bg-danger' : camp.calificados!==camp.total} h2_mosaico">{{camp.career_name}}</h3>
                                <form method="GET" action="<?php echo base_url() . 'session/admin/Admin_status_actas/detalle_career' ?>">
                                            <input name="ID_career" type="hidden" v-model="camp.ID_career">
                                            <input class="btn naranja letra_boton font-weight-bold  mt-1 mb-1 ml-4" type="submit" value="Ver detalle">
                                </form>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>