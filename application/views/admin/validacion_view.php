<div class="content-wrapper" id="app">
	<div class="container mb-3">
		<div class="row col-md-12">
			<h1 class="text-center alert titulo_historial">Validaci&oacute;n </h1>
		</div>
		<div class="row" style="border: 1px solid #fff;">
			<div class="col-md-12">
				<div class="row">
					<div class="col-12 col-md-6 d-flex justify-content-sm-center">
						<img src="<?php echo base_url('/assets/img/logo_CDMX.png'); ?>" class="img-fluid logo_mobile"
							 alt="Logo">
					</div>
					<div class="col-12  col-md-6 text-center">
						<p class="text-secondary mt-2 titulo_mobile titulo_mobile m-0">Secretaria de Educación, Ciencia,
							Tecnología e Innovación de la Ciudad de México</p>
						<p class="text-secondary titulo_mobile m-0">Instituto de Estudios Superiores de la Ciudad de
							México</p>
						<p class="text-secondary titulo_mobile m-0">“Rosario Castellanos” </p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<form v-on:submit.prevent="buscar_grupos">
			<div class="form-row">
				<div class="form-group col-md-3">
					<label for="campus_lic">Licenciatura:</label>
					<select class="form-control" @change="get_year_by_campus_lic" v-model="campus_lic">
						<option v-for="licenciatura in licenciaturas" :value="licenciatura.ID_campus_career">
							{{licenciatura.career_name}}
						</option>
					</select>
				</div>
				<div class="form-group col-md-2" v-show="years.length>0">
					<label for="year">A&ntilde;o:</label>
					<select class="form-control" name="year" @change="get_cicles" v-model="year">
						<option v-for="year_activ in years" :value="year_activ.year_active">{{year_activ.year_active}}
						</option>
					</select>
				</div>
				<div v-show="ciclos.length>0" class="form-group col-md-3">
					<label for="ciclo">Ciclo:</label>
					<select class="form-control" name="ciclo" @change="get_type_group" v-model="ciclo">
						<option v-for="cicl in ciclos" v-bind:value="cicl.cycle">{{cicl.cycle}}</option>
					</select>
				</div>
				<div v-show="tipos_grupos.length>0" class="form-group col-md-2">
					<label for="tipo_grupo">Tipo:</label>
					<select class="form-control" name="tipo_grupo" v-model="tipo_grupo">
						<option v-for="tipos_grupo in tipos_grupos" :value="tipos_grupo.ID_type_group">
							{{tipos_grupo.type_group}}
						</option>
					</select>
				</div>
				<div v-show="tipos_grupos.length>0 && ciclos.length>0 && years.length>0 && licenciaturas.length>0"
					 class="form-group col-md-2 mt-4">
					<button type="submit" class="btn btn-primary">Buscar</button>
				</div>
			</div>
		</form>

		<div class="col-lg-12 col-md-12 text-center" v-show="grupos.length>0">
			<div class="category">
				<h1>{{cantidad_de_grupos_qualified}}/{{cantidad_de_grupos}}</h1>
				<p>calificadas</p>
				<div v-show="cantidad_de_grupos!==cantidad_de_grupos_qualified" class="alert alert-warning"
					 role="alert">
					No puedes validar ni descargar actas si a&uacute;n no han sido calificadas.
				</div>
			</div>
			<table class="table table-responsive-sm table-bordered" width="100%">
				<thead>
				<tr class="table-success">
					<th>Clave de grupo</th>
					<th>Licenciatura</th>
					<th>Asignatura</th>
					<th>Acciones</th>
					<th>Status</th>
				</tr>
				</thead>
				<tbody>
				<tr v-for='group in grupos'>
					<td>
						{{group.group_name}}
					</td>
					<td>
						{{group.career_name}}
					</td>
					<td>
						{{group.subject_name}}
					</td>
					<td>
						<div>
							<form method="GET" action="<?php echo base_url() . 'session/admin/validacion/ver_acta' ?>">
								<input name="ID_teacher_by_group" type="hidden" v-model="group.ID_teacher_by_group">
								<input class="btn btn-success mt-2" type="submit" value="Consultar Acta">
							</form>
						</div>
					</td>
					<td v-show="group.validated==1 && group.qualified==1" class="text-green">
						<i class="fa fa-check" aria-hidden="true"> Validada</i>
					</td>
					<td v-show="group.validated==0 && group.qualified==1" class="text-danger">
						<i class="fa fa-times" aria-hidden="true"> Sin validar</i>
					</td>
				</tr>
				</tbody>
			</table>
			<button v-show="(cantidad_de_grupos===cantidad_de_grupos_qualified && (cantidad_de_grupos!==cantidad_de_grupos_validados)) || (cantidad_de_grupos!==cantidad_de_grupos_validados  && cantidad_de_grupos===cantidad_de_grupos_qualified) "
					@click="validad_grupos" class="btn btn-primary my-2">Validar todas las actas
			</button>
			<button v-show="cantidad_de_grupos===cantidad_de_grupos_validados" @click="download_zip"
					class="btn btn-secondary my-2">Descargar Zip
			</button>
		</div>
	</div>
</div>
