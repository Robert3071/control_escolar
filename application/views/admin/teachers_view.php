<div class="content-wrapper" id="app">
    <div class="container">
        <div class="row col-md-12">
            <h1 class="text-center alert titulo_historial">Docentes</h1>
        </div>
        <div class="row" style="border: 1px solid #fff;">
            <div class="col-12 col-md-12">
                  <div class="row">
                    <div class="col-12 col-md-6 d-flex justify-content-sm-center">
                        <img src="<?php echo base_url('/assets/img/logo_CDMX.png'); ?>" class="img-fluid logo_mobile" alt="Logo">
                    </div>
                    <div class="col-12  col-md-6 text-center">
                        <p class="text-secondary mt-5 titulo_mobile titulo_mobile m-0">Secretaria de Educación, Ciencia,
                            Tecnología e Innovación de la Ciudad de México</p>
                        <p class="text-secondary titulo_mobile m-0">Instituto de Estudios Superiores de la Ciudad de México</p>
                        <p class="text-secondary titulo_mobile m-0">“Rosario Castellanos” </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="col-lg-12 col-md-12 align-content-center">
            <table id="example" class="display table-responsive" style="width:100%">
                <thead>
                    <tr>
                        <th>Nombres</th>
                        <th>Apellidos</th>
                        <th>Correo electrónico</th>
                        <th>Acciones</th>
<!--                        <th>Firma</th>-->
                        <th>Descarga FEI</th>
                    </tr>
                </thead>
                <tbody>
                    
                    <?php foreach ($teachers as $t) : ?>
                        <tr>
                            <td><?php echo $t['name'] ?></td>
                            <td><?php echo $t['surnames'] ?></td>
                            <td><?php echo $t['email'] ?></td>
                            <td><?php
                                echo '<form method="GET" action=' . base_url() . 'session/admin/Admin_history_teacher/detalle_campus>';
                                echo '<input name="ID_teacher" type="hidden" value=' . $t['ID_teacher'] . '>';
                                echo '<button type="submit" class="btn boton_ver_profe mt-2">Ver</button>';
                                echo '</form>';
                                ?>
                            </td>
<!--                            <td>-->
<!--                                --><?php //if ($t['key_validate']): ?>
<!--                                    --><?php
//                                    echo '<form method="GET" action=' . base_url() . 'session/admin/Admin_history_teacher/crear_firma>';
//                                    echo '<input name="ID_teacher" type="hidden" value=' . $t['ID_teacher'] . '>';
//                                    echo '<button type="submit" class="btn boton_admin_llave mt-2">Descargar Llave</button>';
//                                    echo '</form>';
//                                    ?><!--    -->
<!--                                --><?php //else: ?>
<!--                                    --><?php
//                                    echo '<form method="GET" action=' . base_url() . 'session/admin/Admin_history_teacher/crear_firma>';
//                                    echo '<input name="ID_teacher" type="hidden" value=' . $t['ID_teacher'] . '>';
//                                    echo '<button type="submit" class="btn boton_admin_llave mt-2">Descargar Llave</button>';
//                                    echo '</form>';
//                                    ?><!--    -->
<!--                                </td>        -->
<!--                            --><?php //endif; ?>
                            <td>
                            <?php if($t['descargas_fiel']==0): ?>
                                No se ha descargado la FEI
                                <?php else: ?>
                                    <?php echo $t['descargas_fiel'].' '.'descargas'; ?>
                            <?php endif; ?>
                            
                            </td>
                        </tr>
                        
                    <?php endforeach; ?>
                </tbody>
                <tfoot>
                    <tr>
                        <!-- <th>RFC</th> -->
                        <th>Nombres</th>
                        <th>Apellidos</th>
                        <th>Correo electrónico</th>
<!--                        <th>Acciones</th>-->
                        <th>Firma</th>
                    </tr>
            </table>
        </div>
    </div>
</div>


