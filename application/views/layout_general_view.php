<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="description" content="Autor: IRC">
        <meta name="keywords" content="historial academico, Actas IRC, IRC">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <script type="text/javascript" src="<?php echo base_url('assets/jquery/jquery-3.4.1.min.js'); ?>"></script>
        <link href="https://unpkg.com/ionicons@4.5.10-0/dist/css/ionicons.min.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/fontawesome-free/css/all.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/adminlte.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/registro/css/principal.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/registro/css/propuesta_materiales.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/registro/css/wingets.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/dist/js/adminlte.js"></script>
		<script>
            const base_url = "<?php echo base_url(); ?>";
            const regex_calificacion = <?php echo regex_calificacion(); ?>;

        </script>
        <!--modo desarrollador-->
		
        <!--<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>-->
		<script src="<?php echo base_url(); ?>assets/vendors/vue.min.js"></script>
		<!--        <script src="--><?php //echo base_url(); ?><!--assets/vendors/vue.min.js"></script>-->
        <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
		
		<!-- sweet Alert -->
		<script src="<?php echo base_url('assets/vendors/sweetalert/sweetalert2.all.min.js') ?>"></script>
        <link rel="shortcut icon" type="image/ico" href="<?php echo base_url(); ?>assets/img/ircfavicon.ico" />
        <!--script para tablas--> 
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.flash.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js"></script>
        <title>Sistema Actas</title>
       
    </head>
    <?php $uri=explode("/", $_SERVER['REQUEST_URI']); 
    $longitud=count($uri);
    ?>
    <?php if($uri[$longitud-1]=="login_admin" || $uri[$longitud-1]=="login_jefe_carrera" || $uri[$longitud-1]=="login_teacher"): ?>
        <body id="banner_admin">
    
    <?php else: ?>
     
    <body>
     <?php endif; ?>
        <?php echo (isset($header)) ? $header : FALSE; ?>
        <!--nav-->
        <?php echo (isset($nav)) ? $nav : FALSE; ?>
        <!--aside-->
        <?php echo (isset($aside)) ? $aside : FALSE; ?>
        <!--main-->
        <?php echo (isset($main)) ? $main : FALSE; ?>
        <!--footer-->
        <?php echo (isset($footer)) ? $footer : FALSE; ?>
        <?php
        if (isset($scripts) and is_array($scripts)) {
            foreach ($scripts as $script) {
                echo $script;
            }
        }
        ?>        
    </body>
</html>
