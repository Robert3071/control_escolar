<?php

class Rectification_actas_extra_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->teacher = 'ce_teacher';
        $this->teacher_by_group = 'ce_teacher_by_group';
    }

    //funcion para ver si el grupo ya esta calificado
    public function get_teacher_by_group($id_teacher_by_group) {
        $datos = $this->db->get_where($this->teacher_by_group, array('ID_teacher_by_group' => $id_teacher_by_group), 1);
        if (!$datos->row()) {
            return false;
        }
        return $datos->row();
//        print_r($this->db->last_query());
    }

//funcion para traer datos del profesor
    public function get_teacher($id_teacher) {
        $datos = $this->db->get_where($this->teacher, array('id_user' => $id_teacher), 1);
        if (!$datos->row()) {
            return false;
        }
        return $datos->row();
    }

    public function get_all_students_by_id_group($anio_actual, $ciclo_actual, $id_group, $id_teacher, $id_campus_carrer) {
        $this->db->select('
        ce_rating_student.ID_rating_student,
        ce_rating_student.ID_student,
        ce_rating_student.ID_teacher_by_group,
        ce_rating_student.score,
        ce_rating_student.validate,
        ce_rating_student.`status`,
        ce_rating_student.created_at,
        ce_rating_student.updated_at,
        ce_teacher_by_group.ID_teacher,
        ce_student.names,
        ce_student.surnames,
        ce_student.sex,
        ce_student.accountNumber,
        ce_student.curp,
        ce_student.birthdate,
        ce_student.generation
        ');
        $this->db->from('ce_rating_student');
        $this->db->join('ce_student', 'ce_rating_student.ID_student = ce_student.ID_student');
        $this->db->join('ce_teacher_by_group', 'ce_rating_student.ID_teacher_by_group = ce_teacher_by_group.ID_teacher_by_group');
        $this->db->join('ce_group', 'ce_teacher_by_group.ID_group = ce_group.ID_group');
        $this->db->join('ce_campus_career', 'ce_group.ID_campus_career = ce_campus_career.ID_campus_career');
        $this->db->where('ce_rating_student.rectificated', 1);
        $this->db->where('ce_teacher_by_group.ID_teacher_by_group', $id_group);
        $this->db->where('ce_group.year_active', $anio_actual);
     //   $this->db->where('ce_group.cycle', $ciclo_actual);
        $this->db->where('ce_group.ID_type_group', 2);
        $this->db->where_in('ce_group.ID_campus_career', $id_campus_carrer);
        $this->db->where('ce_teacher_by_group.ID_teacher', $id_teacher);

        $result = $this->db->get();
        return $result->result_array();
    }

    public function get_all_students_by_id_group_rectificate($anio_actual, $ciclo_actual, $id_group, $id_teacher, $id_campus_carrer) {
        $this->db->select('
        ce_rating_student.ID_rating_student,
        ce_rating_student.ID_student,
        ce_rating_student.ID_teacher_by_group,
        ce_rating_student.score,
        ce_rating_student.validate,
        ce_rating_student.`status`,
        ce_rating_student.created_at,
        ce_rating_student.updated_at,
        ce_teacher_by_group.ID_teacher,
        ce_student.names,
        ce_student.surnames,
        ce_student.sex,
        ce_student.accountNumber,
        ce_student.curp,
        ce_student.birthdate,
        ce_student.generation
        ');
        $this->db->from('ce_rating_student');
        $this->db->join('ce_student', 'ce_rating_student.ID_student = ce_student.ID_student');
        $this->db->join('ce_teacher_by_group', 'ce_rating_student.ID_teacher_by_group = ce_teacher_by_group.ID_teacher_by_group');
        $this->db->join('ce_group', 'ce_teacher_by_group.ID_group = ce_group.ID_group');
        $this->db->join('ce_campus_career', 'ce_group.ID_campus_career = ce_campus_career.ID_campus_career');
        $this->db->where('ce_student.status', 1);
        $this->db->where('ce_rating_student.status', 1);
        $this->db->where('ce_teacher_by_group.ID_teacher_by_group', $id_group);
        $this->db->where('ce_group.year_active', $anio_actual);
      //  $this->db->where('ce_group.cycle', $ciclo_actual);
        $this->db->where('ce_teacher_by_group.ID_teacher', $id_teacher);
        $this->db->where_in('ce_group.ID_campus_career', $id_campus_carrer);
        $this->db->where('ce_group.ID_type_group', 2);
        $result = $this->db->get();
        return $result->result_array();
    }

//funcion que trae a tipos de grupos dependiendo del campus,y año elegido
    public function get_all_type_group_by_id_teacher($id_teacher, $id_campus_carre, $year) {
        $this->db->distinct();
        $this->db->select('
        ce_group.ID_campus_career,
        ce_group.year_active,
        ce_group.ID_type_group,
        ce_type_group.type_group');
        $this->db->from('ce_teacher_by_group');
        $this->db->join('ce_group', 'ce_teacher_by_group.ID_group = ce_group.ID_group');
        $this->db->join('ce_type_group', 'ce_group.ID_type_group = ce_type_group.ID_type_group');
        $this->db->where('ID_teacher', $id_teacher);
        $this->db->where('ce_group.ID_campus_career', $id_campus_carre);
        $this->db->where('year_active', $year);
        $this->db->where('ce_teacher_by_group.status', 1);
        $this->db->where('ce_group.ID_type_group', 2);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function get_all_campus_by_id_teacher($id_teacher, $anio_actual, $ciclo_actual, $id_campus_carre) {
        $this->db->distinct();
        $this->db->select('
        ce_campus.campus_name,
        ce_career.career_name,
        ce_group.ID_campus_career');
        $this->db->from('ce_teacher_by_group');
        $this->db->join('ce_group', 'ce_teacher_by_group.ID_group = ce_group.ID_group');
        $this->db->join('ce_campus_career', 'ce_group.ID_campus_career = ce_campus_career.ID_campus_career');
        $this->db->join('ce_campus', 'ce_campus_career.ID_campus = ce_campus.ID_campus');
        $this->db->join('ce_career', 'ce_career.ID_career = ce_campus_career.ID_career');
        $this->db->where('ce_teacher_by_group.status', 1);
        $this->db->where('ce_teacher_by_group.qualified', 1);
        $this->db->where('(ce_teacher_by_group.rectificated=1 or ce_teacher_by_group.count_rectificated > 0)');
        $this->db->where('ce_group.ID_type_group', 2);
        $this->db->where('ID_teacher', $id_teacher);
        $this->db->where('ce_group.year_active', $anio_actual);
        $this->db->where_in('ce_group.ID_campus_career', $id_campus_carre);
     //   $this->db->where('ce_group.cycle', $ciclo_actual);
        $result = $this->db->get();
        return $result->result_array();
        //print_r($this->db->last_query());
    }

    //funcion trae los años en el que el profesor dio clase a un grupo
    public function get_years_by_id_teacher($id_teacher, $id_campus_lic, $anio_actual, $ciclo_actual) {
        $this->db->distinct();
        $this->db->select(' 
        ce_group.ID_campus_career,
        ce_group.year_active');
        $this->db->from('ce_teacher_by_group');
        $this->db->join('ce_group', 'ce_teacher_by_group.ID_group = ce_group.ID_group');
        $this->db->where('ID_teacher', $id_teacher);
        $this->db->where('ce_group.ID_campus_career', $id_campus_lic);
        $this->db->where('ce_group.year_active', $anio_actual);
      //  $this->db->where('ce_group.cycle', $ciclo_actual);
        $this->db->where('ce_teacher_by_group.status', 1);
        $this->db->where('ce_teacher_by_group.qualified', 1);
        $this->db->where('ce_group.status', 1);
        $this->db->where('ce_group.ID_type_group', 2);
        $this->db->where('(ce_teacher_by_group.rectificated=1 or ce_teacher_by_group.count_rectificated > 0)');
        $result = $this->db->get();
//        print_r($this->db->last_query());
        return $result->result_array();
    }

    //funcion trae los años en el que el profesor dio clase a un grupo
    public function get_ciclo_by_id_teacher($id_teacher, $id_campus_lic, $year, $anio_actual, $ciclo_actual) {
        $this->db->distinct();
        $this->db->select('
        ce_group.ID_campus_career,
        ce_group.cycle');
        $this->db->from('ce_teacher_by_group');
        $this->db->join('ce_group', 'ce_teacher_by_group.ID_group = ce_group.ID_group');
        $this->db->where('ID_teacher', $id_teacher);
        $this->db->where('ce_group.ID_campus_career', $id_campus_lic);
        $this->db->where('ce_group.year_active', $year);
        $this->db->where('ce_group.year_active', $anio_actual);
  //      $this->db->where('ce_group.cycle', $ciclo_actual);
        $this->db->where('ce_teacher_by_group.status', 1);
        $this->db->where('ce_teacher_by_group.qualified', 1);
        $this->db->where('ce_group.status', 1);
        $this->db->where('ce_group.ID_type_group', 2);
        $this->db->where('(ce_teacher_by_group.rectificated=1 or ce_teacher_by_group.count_rectificated > 0)');
        $result = $this->db->get();
//        print_r($this->db->last_query());
        return $result->result_array();
    }

//funcion que trae los grupos dependiendo del campo con id_campus,año,tipode grupo seleccionado
    public function get_all_groups_by_id_techer_year_id_type_group($id_teacher, $id_campus_lic, $year, $ciclo, $id_type_group) {
        $this->db->select('
        ce_course.subject_name,
        ce_course.key_curse,
        ce_teacher_by_group.ID_teacher,
        ce_group.ID_campus_career,
        ce_group.year_active,
        ce_group.group_name,
        ce_teacher_by_group.qualified,
        ce_teacher_by_group.rectificated,
        ce_teacher_by_group.ID_teacher_by_group,
        ce_group.ID_type_group');
        $this->db->from('ce_teacher_by_group');
        $this->db->join('ce_group', 'ce_teacher_by_group.ID_group = ce_group.ID_group');
        $this->db->join('ce_course', 'ce_teacher_by_group.ID_curse = ce_course.ID_curse');
        $this->db->where('ce_teacher_by_group.ID_teacher', $id_teacher);
        $this->db->where('ce_group.year_active', $year);
        $this->db->where('ce_group.ID_campus_career', $id_campus_lic);
        $this->db->where('ce_group.ID_type_group', 2);
        $this->db->where('ce_group.cycle', $ciclo);
        $this->db->where('ce_teacher_by_group.status', 1);
        $this->db->where('ce_teacher_by_group.qualified', 1);
        $this->db->where('ce_group.ID_type_group', 2);
        $this->db->where('(ce_teacher_by_group.rectificated=1 or ce_teacher_by_group.count_rectificated > 0)');
        $result = $this->db->get();
//        print_r($this->db->last_query());
        return $result->result_array();
    }

    //funcion para actualizar calificaciones
    public function update_score_student($id_student, $id_group, $score) {
        return($this->db->update('ce_rating_student', ['score' => strtoupper($score), 'rectificated' => 0], ['ID_student' => $id_student, 'status' => 1, 'ID_teacher_by_group' => $id_group]
                ) ? TRUE : FALSE);
    }

    //funcion para actualizar qualified del grupo
    public function update_qualified($id_group, $id_teacher) {
        return($this->db->update('ce_teacher_by_group', ['rectificated' => 0],
                        ['qualified' => 1, 'status' => 1, 'ID_teacher_by_group' => $id_group, 'ID_teacher' => $id_teacher]
                ) ? TRUE : FALSE);
    }

    //funcion para traer los datos del grupo
    public function get_group($id_group) {
        $this->db->select('
        ce_course.subject_name,
        ce_course.key_curse,
        ce_group.year_active,
        ce_group.cycle,
        ce_group.group_name,
        ce_group.ID_type_group,
        ce_teacher.rfc,
        ce_teacher.name,
        ce_teacher.surnames,
        ce_teacher.ruta_firm_auto,
        ce_teacher.ID_teacher,
        ce_type_group.type_group,
        ce_teacher_by_group.folio_acta,
        ce_teacher_by_group.updated_at,
        ce_teacher.ruta_firm_auto,
        ce_teacher.key_pass,
        ce_career.short_name');
        $this->db->from('ce_teacher_by_group');
        $this->db->join('ce_group', 'ce_teacher_by_group.ID_group = ce_group.ID_group');
        $this->db->join('ce_course', 'ce_teacher_by_group.ID_curse = ce_course.ID_curse');
        $this->db->join('ce_teacher', 'ce_teacher_by_group.ID_teacher = ce_teacher.ID_teacher');
        $this->db->join('ce_type_group', 'ce_group.ID_type_group = ce_type_group.ID_type_group');
        $this->db->join('ce_campus_career', 'ce_campus_career.ID_campus_career = ce_group.ID_campus_career');
        $this->db->join('ce_career', 'ce_career.ID_career = ce_campus_career.ID_career');
        $this->db->where('ce_teacher_by_group.status', 1);
        $this->db->where('ce_group.status', 1);
        $this->db->where('ce_group.ID_type_group', 2);
        $this->db->where('ce_teacher.status', 1);
        $this->db->where('ce_teacher_by_group.ID_teacher_by_group', $id_group);
        $result = $this->db->get();
        //print_r($this->db->last_query());
        return $result->result_array();
    }

//    public function get_qr($id_teacher_by_group, $anio_actual, $ciclo_actual, $id_teacher) {
//        $this->db->select('CONCAT(key_plan,"-",folio_acta) as qr,count_rectificated');
//        $this->db->from('ce_teacher_by_group');
//        $this->db->join('ce_group', 'ce_teacher_by_group.ID_group = ce_group.ID_group');
//        $this->db->join('ce_plan', 'ce_group.ID_plan = ce_plan.ID_plan');
//        $this->db->where('ce_teacher_by_group.status', 1);
//        $this->db->where('ce_group.status', 1);
//        //        $this->db->where('ce_teacher_by_group.rectificated', 0);
//        $this->db->where('ce_teacher_by_group.ID_teacher_by_group', $id_teacher_by_group);
//        $this->db->where('ce_group.year_active', $anio_actual);
//        $this->db->where('ce_group.cycle', $ciclo_actual);
//        $this->db->where('ce_teacher_by_group.ID_teacher', $id_teacher);
//        $this->db->limit(1);
//        $result = $this->db->get();
//        return $result->row();
//    }
}
