<?php
defined('BASEPATH') or exit('No direct script access allowed');

class General_model extends CI_Model
{
    protected $user;
    //private $table;

    public function __construct()
    {
        $this->user = 'users';
        $this->load->database();
        //$this->table = '';
    }

    private function set_countries()
    {
        $this->table = '';
        $this->table = 'countries_ar';
        return $this->table;
    }

    private function set_municipalities()
    {
        $this->table = '';
        $this->table = 'municipalities_ar';
        return $this->table;
    }

    private function set_states()
    {
        $this->table = '';
        $this->table = 'states_ar';
        return $this->table;
    }
    
    private function set_aspirants()
    {
        $this->table = '';
        $this->table = 'ri_updated_reinscription';
        return $this->table;
    }

    public function get_aspirants()
    {
        return $this->set_aspirants();
    }

    public function get_countries()
    {
        return $this->set_countries();
    }

    public function get_municipalities()
    {
        return $this->set_municipalities();
    }

    public function get_states()
    {
        return $this->set_states();
    }

    public function type_query($type_query, $query)
    {
        switch ($type_query) {
            case $type_query == 1:
                //object
                if ($query->num_rows() > 0) {
                    return $query->row();
                } else {
                    return FALSE;
                }
                break;

            case $type_query == 2:
                // array of objects
                if ($query->num_rows() > 0) {
                    return $query->result_array();
                } else {
                    return FALSE;
                }
                break;

            default:
                return FALSE;
        }
    }

    public function query_get_data($data, $tb, $type_query)
    {
        $this->db->select($data);
        $this->db->from($tb);
        $query = $this->db->get();
        return $this->type_query($type_query, $query);
    }

    public function query_get_data_arr($data, $tb, $where, $type_query)
    {
        $this->db->select($data);
        $this->db->from($tb);
        $this->db->where($where);
        $query = $this->db->get();
        return $this->type_query($type_query, $query);
    }

    public function insert_row_data($data, $tb, $id = FALSE)
    {
        $this->db->insert($tb, $data);
        if ($id) {
            return $this->db->insert_id();
        } else {
            return FALSE;
        }
    }
}
