<?php

class Admin_jef_carr_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->admin = 'ce_admin';
    }

//funcion para traer datos del admin
    public function get_info_admin($id_user) {
        $this->db->select('ce_campus_career.ID_career,
ce_campus_career.ID_campus,
ce_admin.ID_campus_carrer,
ce_career.career_name,
ce_career.short_name,
ce_campus.campus_name,
ce_campus.ID_campus,
ce_admin.ID_user,
ce_admin.`name`,
ce_admin.surnames,
ce_admin.rfc');
        $this->db->from('ce_admin');
        $this->db->join('ce_campus_career', 'ce_campus_career.ID_campus_career = ce_admin.ID_campus_carrer');
        $this->db->join('ce_career', 'ce_campus_career.ID_career = ce_career.ID_career');
        $this->db->join('ce_campus', 'ce_campus_career.ID_campus = ce_campus.ID_campus');
        $this->db->where('ce_campus_career.status', 1);
        $this->db->where('ce_admin.status', 1);
        $this->db->where('ID_user', $id_user);
        $this->db->limit(1);
        $result = $this->db->get();
        return $result->row();
    }

//funcion que trae todos los grupos
    public function get_all_groups($id_campues_carrer, $anio_actual, $ciclo_actual) {
        $this->db->select('
        ce_course.subject_name,
        ce_course.key_curse,
        ce_teacher_by_group.ID_teacher,
        ce_group.ID_campus_career,
        ce_group.group_name,
        ce_group.cycle,
        ce_group.year_active,
        ce_teacher_by_group.qualified,
        ce_teacher_by_group.rectificated,
        ce_teacher_by_group.ID_teacher_by_group,
        ce_group.ID_type_group,
        ce_type_group.type_group,
        ce_teacher.name,
        ce_teacher.surnames');
        $this->db->from('ce_teacher_by_group');
        $this->db->join('ce_group', 'ce_teacher_by_group.ID_group = ce_group.ID_group');
        $this->db->join('ce_type_group', 'ce_group.ID_type_group = ce_type_group.ID_type_group');
        $this->db->join('ce_teacher', 'ce_teacher_by_group.ID_teacher = ce_teacher.ID_teacher');
        $this->db->join('ce_course', 'ce_teacher_by_group.ID_curse = ce_course.ID_curse');
        $this->db->where('ce_group.ID_campus_career', $id_campues_carrer);
		$this->db->where('ce_group.year_active', $anio_actual);
//		$this->db->where_or('ce_group.cycle', $ciclo_actual);
		$this->db->where('ce_teacher_by_group.status', 1);
        //$this->db->where('ce_teacher_by_group.rectificated', 0);
        $result = $this->db->get();
//        print_r($this->db->last_query());
        return $result->result_array();
    }

//funcion que trae los grupos por unidad academica
    public function get_groups_by_id_campus_career($id_campus_carrer, $anio_actual, $ciclo_actual) {
        $this->db->select('
        ce_course.subject_name,
        ce_course.key_curse,
        ce_teacher_by_group.ID_teacher,
        ce_group.ID_campus_career,
        ce_group.year_active,
        ce_group.cycle,
        ce_group.group_name,
        ce_group.ID_type_group,
        ce_teacher_by_group.qualified,
        ce_teacher_by_group.rectificated,
        ce_teacher_by_group.ID_teacher_by_group,
        ce_type_group.type_group,
        ce_teacher.name,
        ce_teacher.surnames,
        ce_campus.ID_campus,
        ce_campus.campus_name,
        ce_career.career_name');
        $this->db->from('ce_teacher_by_group');
        $this->db->join('ce_group', 'ce_teacher_by_group.ID_group = ce_group.ID_group');
        $this->db->join('ce_type_group', 'ce_group.ID_type_group = ce_type_group.ID_type_group');
        $this->db->join('ce_teacher', 'ce_teacher_by_group.ID_teacher = ce_teacher.ID_teacher');
        $this->db->join('ce_course', 'ce_teacher_by_group.ID_curse = ce_course.ID_curse');
        $this->db->join('ce_campus_career', 'ce_campus_career.ID_campus_career = ce_group.ID_campus_career');
        $this->db->join('ce_career', 'ce_career.ID_career = ce_campus_career.ID_career');
        $this->db->join('ce_campus', 'ce_campus.ID_campus = ce_campus_career.ID_campus');
        $this->db->where('ce_teacher_by_group.status', 1);
        $this->db->where('ce_group.ID_campus_career', $id_campus_carrer);
        $this->db->where('ce_group.year_active', $anio_actual);
        $this->db->where('ce_group.cycle', $ciclo_actual);
        $result = $this->db->get();
        return $result->result_array();
    }

    //funcion que trae a todos los estudiantes del grupo seleccionado
    public function get_all_students_by_id_group($id_group, $id_campues_carrer, $anio_actual, $ciclo_actual) {
        $this->db->select('
        ce_rating_student.ID_rating_student,
        ce_rating_student.ID_student,
        ce_rating_student.ID_teacher_by_group,
        ce_rating_student.score,
        ce_rating_student.validate,
        ce_rating_student.`status`,
        ce_rating_student.created_at,
        ce_rating_student.updated_at,
        ce_student.names,
        ce_student.surnames,
        ce_student.sex,
        ce_student.accountNumber,
        ce_student.curp,
        ce_student.birthdate,
        ce_student.generation
        ');
        $this->db->from('ce_rating_student');
        $this->db->join('ce_student', 'ce_rating_student.ID_student = ce_student.ID_student');
        $this->db->join('ce_teacher_by_group', 'ce_rating_student.ID_teacher_by_group = ce_teacher_by_group.ID_teacher_by_group');
        $this->db->join('ce_group', 'ce_teacher_by_group.ID_group = ce_group.ID_group');
        $this->db->where('ce_student.status', 1);
        $this->db->where('ce_rating_student.status', 1);
        $this->db->where('ce_rating_student.rectificated', 0);
        $this->db->where('ce_teacher_by_group.ID_teacher_by_group', $id_group);
        $this->db->where('ce_group.ID_campus_career', $id_campues_carrer);
        $this->db->where('ce_group.year_active', $anio_actual);
        //$this->db->where('ce_group.cycle', $ciclo_actual);
        $result = $this->db->get();
//        print_r($this->db->last_query());
        return $result->result_array();
    }

    /**
     * Para traer a los todos los alumnos
     * no importa so estan rectificados o no
     */
    public function get_all_students_by_id_group_acta($id_group) {
        $this->db->select('
        ce_rating_student.ID_rating_student,
        ce_rating_student.ID_student,
        ce_rating_student.ID_teacher_by_group,
        ce_rating_student.score,
        ce_rating_student.validate,
        ce_rating_student.`status`,
        ce_rating_student.created_at,
        ce_rating_student.updated_at,
        ce_student.names,
        ce_student.surnames,
        ce_student.sex,
        ce_student.accountNumber,
        ce_student.curp,
        ce_student.birthdate,
        ce_student.generation
        ');
        $this->db->from('ce_rating_student');
        $this->db->join('ce_student', 'ce_rating_student.ID_student = ce_student.ID_student');
        $this->db->where('ce_student.status', 1);
        $this->db->where('ce_rating_student.status', 1);
        $this->db->where('ID_teacher_by_group', $id_group);
        $result = $this->db->get();
        return $result->result_array();
    }

    //funcion que trae a todos los estudiantes que tubieron rectificados del grupo seleccionado
    public function get_all_students_rectificados_by_id_group($id_group) {
        $this->db->select('
        ce_rating_student.ID_rating_student,
        ce_rating_student.ID_student,
        ce_rating_student.ID_teacher_by_group,
        ce_rating_student.score,
        ce_rating_student.validate,
        ce_rating_student.`status`,
        ce_rating_student.created_at,
        ce_rating_student.updated_at,
        ce_student.names,
        ce_student.surnames,
        ce_student.sex,
        ce_student.accountNumber,
        ce_student.curp,
        ce_student.birthdate,
        ce_student.generation
        ');
        $this->db->from('ce_rating_student');
        $this->db->join('ce_student', 'ce_rating_student.ID_student = ce_student.ID_student');
        $this->db->where('ce_student.status', 1);
        $this->db->where('ce_rating_student.status', 1);
        $this->db->where('ce_rating_student.rectificated', 1);
        $this->db->where('ID_teacher_by_group', $id_group);
        $result = $this->db->get();
        return $result->result_array();
    }

//funcion para actualizar calificaciones
    public function update_rectificated_student($ID_rating_student) {
        return($this->db->update('ce_rating_student', ['rectificated' => 1], ['ID_rating_student' => $ID_rating_student]
                ) ? TRUE : FALSE);
    }

    public function update_desrectificated_student($ID_rating_student) {
        return($this->db->update('ce_rating_student', ['rectificated' => 0], ['ID_rating_student' => $ID_rating_student]
                ) ? TRUE : FALSE);
    }

//funcion para rectificar grupo
    public function rectificar_grupo_by_id_group($ID_teacher_by_group) {
        return($this->db->update('ce_teacher_by_group', ['rectificated' => 1], ['ID_teacher_by_group' => $ID_teacher_by_group]
                ) ? TRUE : FALSE);
    }

//funcion para derectificar grupo
    public function desrectificar_grupo_by_id_group($ID_teacher_by_group) {
        return($this->db->update('ce_teacher_by_group', ['rectificated' => 0], ['ID_teacher_by_group' => $ID_teacher_by_group]
                ) ? TRUE : FALSE);
    }

//funcion para derectificar alumnos
    public function desrectificate_all_students_by_id_group($ID_teacher_by_group) {
        return($this->db->update('ce_rating_student', ['rectificated' => 0], ['ID_teacher_by_group' => $ID_teacher_by_group]
                ) ? TRUE : FALSE);
    }

    //funcion para traer los datos del grupo
    public function get_group($id_group, $id_campues_carrer) {
        $this->db->select('
        ce_course.subject_name,
        ce_course.key_curse,
        ce_group.year_active,
        ce_group.cycle,
        ce_group.group_name,
        ce_group.ID_type_group,
        ce_teacher.rfc,
        ce_teacher.name,
        ce_teacher.surnames,
        ce_teacher.ruta_firm_auto,
        ce_teacher_by_group.ID_teacher_by_group,
        ce_teacher_by_group.folio_acta,
        ce_teacher_by_group.updated_at,
        ce_type_group.type_group,
        ce_career.short_name');
        $this->db->from('ce_teacher_by_group');
        $this->db->join('ce_group', 'ce_teacher_by_group.ID_group = ce_group.ID_group');
        $this->db->join('ce_course', 'ce_teacher_by_group.ID_curse = ce_course.ID_curse');
        $this->db->join('ce_teacher', 'ce_teacher_by_group.ID_teacher = ce_teacher.ID_teacher');
        $this->db->join('ce_type_group', 'ce_group.ID_type_group = ce_type_group.ID_type_group');
        $this->db->join('ce_campus_career', 'ce_campus_career.ID_campus_career = ce_group.ID_campus_career');
        $this->db->join('ce_career', 'ce_career.ID_career = ce_campus_career.ID_career');
        $this->db->where('ce_teacher_by_group.status', 1);
        $this->db->where('ce_group.status', 1);
        $this->db->where('ce_teacher.status', 1);
        $this->db->where('ce_teacher_by_group.ID_teacher_by_group', $id_group);
        $this->db->where('ce_group.ID_campus_career', $id_campues_carrer);
        $result = $this->db->get();
        //print_r($this->db->last_query());
        return $result->result_array();
    }

//funcion  que trae al profesor dependiedo del rfc a buscar

    public function get_data_teacher_by_id($ID_teacher) {
        $this->db->select('
        ce_teacher.ID_teacher,
        ce_teacher.rfc,
        ce_teacher.name,
        ce_teacher.surnames,
        case  
   when sex=1 then "Masculino"
   when sex=0 then "Femenino"
        end as sexo', FALSE);
        $this->db->from('ce_teacher');
        $this->db->where('ce_teacher.status', 1);
        $this->db->where('ce_teacher.ID_teacher', $ID_teacher);
        $this->db->limit(1);
        $result = $this->db->get();
        return $result->row();
    }

    public function get_all_teachers($anio_actual, $ciclo_actual, $id_campues_carrer) {
        $this->db->select(' DISTINCT
            ce_teacher.`name`,
            ce_teacher.surnames,
            ce_teacher.rfc,
            ce_teacher.ID_teacher,
            ce_teacher.ID_user', false);
        $this->db->from('ce_teacher_by_group');
        $this->db->join('ce_group', 'ce_teacher_by_group.ID_group = ce_group.ID_group');
        $this->db->join('ce_teacher', 'ce_teacher_by_group.ID_teacher = ce_teacher.ID_teacher');
        $this->db->where('ce_teacher.status', 1);
        $this->db->where('ce_group.ID_campus_career', $id_campues_carrer);
        $this->db->where('ce_group.year_active', $anio_actual);
        $this->db->where('ce_group.cycle', $ciclo_actual);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function get_groups_by_id_teacher($anio_actual, $ciclo_actual, $id_teacher, $id_campues_carrer) {
        $this->db->select('
                ce_course.subject_name,
            ce_course.key_curse,
            ce_group.year_active,
            ce_group.cycle,
            ce_group.group_name,
            ce_type_group.type_group,
            ce_career.short_name,
            ce_career.career_name,
            ce_teacher_by_group.ID_teacher,
            ce_teacher_by_group.qualified,
            ce_teacher_by_group.rectificated,
            CASE
            WHEN qualified = 1 THEN
                    "Calificado"
            WHEN qualified = 0 THEN
                    "No calificado"
            END AS calificado', FALSE);
        $this->db->from('ce_teacher_by_group');
        $this->db->join('ce_group', 'ce_teacher_by_group.ID_group = ce_group.ID_group');
        $this->db->join('ce_course', 'ce_teacher_by_group.ID_curse = ce_course.ID_curse');
        $this->db->join('ce_teacher', 'ce_teacher_by_group.ID_teacher = ce_teacher.ID_teacher');
        $this->db->join('ce_type_group', 'ce_group.ID_type_group = ce_type_group.ID_type_group');
        $this->db->join('ce_campus_career', 'ce_campus_career.ID_campus_career = ce_group.ID_campus_career');
        $this->db->join('ce_career', 'ce_career.ID_career = ce_campus_career.ID_career');
        $this->db->where('ce_teacher_by_group.status', 1);
        $this->db->where('ce_group.ID_campus_career', $id_campues_carrer);
        $this->db->where('ce_group.status', 1);
        $this->db->where('ce_teacher.status', 1);
        $this->db->where('ce_teacher.ID_teacher', $id_teacher);
        $this->db->where('ce_group.year_active', $anio_actual);
        $this->db->where('ce_group.cycle', $ciclo_actual);
        $result = $this->db->get();
//        print_r($this->db->last_query());
        return $result->result_array();
    }

    public function get_count_actas_by_id_teacher($anio_actual, $ciclo_actual, $id_teacher, $id_campues_carrer) {
        $this->db->select('
                        CASE
        WHEN qualified = 1 THEN
                "Calificado"
        WHEN qualified = 0 THEN
                "No calificado"
        END AS calificado,
        COUNT(qualified) as cantidad_calificado', FALSE);
        $this->db->from('ce_teacher_by_group');
        $this->db->join('ce_group', 'ce_teacher_by_group.ID_group = ce_group.ID_group');
        $this->db->join('ce_course', 'ce_teacher_by_group.ID_curse = ce_course.ID_curse');
        $this->db->join('ce_teacher', 'ce_teacher_by_group.ID_teacher = ce_teacher.ID_teacher');
        $this->db->join('ce_type_group', 'ce_group.ID_type_group = ce_type_group.ID_type_group');
        $this->db->join('ce_campus_career', 'ce_campus_career.ID_campus_career = ce_group.ID_campus_career');
        $this->db->join('ce_career', 'ce_career.ID_career = ce_campus_career.ID_career');
        $this->db->where('ce_teacher_by_group.status', 1);
        $this->db->where('ce_group.status', 1);
        $this->db->where('ce_teacher.status', 1);
        $this->db->where('ce_teacher_by_group.ID_teacher', $id_teacher);
        $this->db->where('ce_group.ID_campus_career', $id_campues_carrer);
        $this->db->where('ce_group.year_active', $anio_actual);
        $this->db->where('ce_group.cycle', $ciclo_actual);
        $this->db->group_by('qualified');
        $result = $this->db->get();
    //        print_r($this->db->last_query());
        return $result->result_array();
    }

    //// revisar esto como manejan los ciclos
    public function get_count_actas_by_campus($anio_actual, $ciclo_actual, $id_campues_carrer) {
        $this->db->select('
                        ce_campus.ID_campus,
                        ce_campus.campus_name,
                        ce_career.career_name,
                        count(qualified) as total,
                        sum(case when qualified = 1 THEN
			1 ELSE 0 end) AS calificados', FALSE);
        $this->db->from('ce_teacher_by_group');
        $this->db->join('ce_group', 'ce_teacher_by_group.ID_group = ce_group.ID_group');
        $this->db->join('ce_campus_career', 'ce_campus_career.ID_campus_career = ce_group.ID_campus_career');
        $this->db->join('ce_campus', ' ce_campus_career.ID_campus = ce_campus.ID_campus');
        $this->db->join('ce_career', 'ce_career.ID_career = ce_campus_career.ID_career');
        $this->db->where('ce_teacher_by_group.status', 1);
        $this->db->where('ce_group.year_active', $anio_actual);
        $this->db->where('ce_group.cycle', $ciclo_actual);
        $this->db->where('ce_group.ID_campus_career', $id_campues_carrer);
        $this->db->group_by('ce_campus.ID_campus,ce_campus.campus_name,ce_career.career_name');
        $result = $this->db->get();
        return $result->row();
    }

}
