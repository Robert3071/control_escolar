<?php

class Admin_queries extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->admin = 'ce_admin';
	}

	//funcion para traer datos del profesor
	public function get_admin($id_admin)
	{
		$datos = $this->db->get_where($this->admin, array('id_user' => $id_admin), 1);
		if (!$datos->row()) {
			return false;
		}
		return $datos->row();
	}

	public function get_info_admin($id_user)
	{
		$this->db->select('ce_campus_career.ID_career,
ce_campus_career.ID_campus,
ce_admin.ID_campus_carrer,
ce_career.career_name,
ce_career.short_name,
ce_campus.campus_name,
ce_campus.ID_campus,
ce_admin.ID_user,
ce_admin.`name`,
ce_admin.surnames,
ce_admin.rfc');
		$this->db->from('ce_admin');
		$this->db->join('ce_campus_career', 'ce_campus_career.ID_campus_career = ce_admin.ID_campus_carrer');
		$this->db->join('ce_career', 'ce_campus_career.ID_career = ce_career.ID_career');
		$this->db->join('ce_campus', 'ce_campus_career.ID_campus = ce_campus.ID_campus');
		$this->db->where('ce_campus_career.status', 1);
		$this->db->where('ce_admin.status', 1);
		$this->db->where('ID_user', $id_user);
		$this->db->limit(1);
		$result = $this->db->get();
		return $result->row();
	}

	//funcion que trae todos los grupos
	public function get_all_groups($anio_actual, $ciclo_actual, $campus)
	{
		$this->db->select('
        ce_course.subject_name,
        ce_course.key_curse,
        ce_teacher_by_group.ID_teacher,
        ce_group.ID_campus_career,
        ce_group.ID_type_group,
           ce_group.group_name,
        ce_group.year_active,
        ce_teacher_by_group.qualified,
        ce_teacher_by_group.rectificated,
        ce_teacher_by_group.ID_teacher_by_group,
        ce_teacher.name,
        ce_teacher.surnames');
		$this->db->from('ce_teacher_by_group');
		$this->db->join('ce_group', 'ce_teacher_by_group.ID_group = ce_group.ID_group');
		$this->db->join('ce_teacher', 'ce_teacher_by_group.ID_teacher = ce_teacher.ID_teacher');
		$this->db->join('ce_course', 'ce_teacher_by_group.ID_curse = ce_course.ID_curse');
		$this->db->join('ce_campus_career', 'ce_group.ID_campus_career = ce_campus_career.ID_campus_career');
		$this->db->where('ce_group.year_active', $anio_actual);
		$this->db->where('ce_group.cycle', $ciclo_actual);
		$this->db->where('ce_campus_career.ID_campus', $campus);
		$this->db->where('ce_teacher_by_group.status', 1);
		$result = $this->db->get();
		//        print_r($this->db->last_query());
		return $result->result_array();
	}

	//funcion que trae los grupos por unidad academica
	public function get_groups_by_campus($id_campus, $anio_actual, $ciclo_actual)
	{
		$this->db->select('
    ce_course.subject_name,
    ce_course.key_curse,
    ce_teacher_by_group.ID_teacher,
    ce_group.ID_campus_career,
    ce_group.year_active,
    ce_group.ID_type_group,
       ce_group.group_name,
    ce_teacher_by_group.qualified,
    ce_teacher_by_group.rectificated,
    ce_teacher_by_group.ID_teacher_by_group,
    ce_type_group.type_group,
    ce_teacher.name,
    ce_teacher.surnames,
    ce_campus.ID_campus,
    ce_campus.campus_name,
    SUBSTRING(ce_career.career_name, 17) AS career_name');
		$this->db->from('ce_teacher_by_group');
		$this->db->join('ce_group', 'ce_teacher_by_group.ID_group = ce_group.ID_group');
		$this->db->join('ce_type_group', 'ce_group.ID_type_group = ce_type_group.ID_type_group');
		$this->db->join('ce_teacher', 'ce_teacher_by_group.ID_teacher = ce_teacher.ID_teacher');
		$this->db->join('ce_course', 'ce_teacher_by_group.ID_curse = ce_course.ID_curse');
		$this->db->join('ce_campus_career', 'ce_campus_career.ID_campus_career = ce_group.ID_campus_career');
		$this->db->join('ce_career', 'ce_career.ID_career = ce_campus_career.ID_career');
		$this->db->join('ce_campus', 'ce_campus.ID_campus = ce_campus_career.ID_campus');
		$this->db->where('ce_teacher_by_group.status', 1);
		$this->db->where('ce_campus.ID_campus', $id_campus);
		$this->db->where('year_active', $anio_actual);
		$this->db->where('cycle', $ciclo_actual);
		$result = $this->db->get();
		return $result->result_array();
	}

	public function get_groups_by_career($id_career, $anio_actual, $ciclo_actual)
	{
		$this->db->select('
    ce_course.subject_name,
    ce_course.key_curse,
    ce_teacher_by_group.ID_teacher,
    ce_group.ID_campus_career,
    ce_group.year_active,
    ce_group.ID_type_group,
       ce_group.group_name,
    ce_teacher_by_group.qualified,
        ce_teacher_by_group.rectificated,
    ce_teacher_by_group.ID_teacher_by_group,
    ce_type_group.type_group,
    ce_teacher.name,
    ce_teacher.surnames,
    ce_career.ID_career,
    ce_career.career_name,
    SUBSTRING(ce_career.career_name, 17) AS career_name');
		$this->db->from('ce_teacher_by_group');
		$this->db->join('ce_group', 'ce_teacher_by_group.ID_group = ce_group.ID_group');
		$this->db->join('ce_type_group', 'ce_group.ID_type_group = ce_type_group.ID_type_group');
		$this->db->join('ce_teacher', 'ce_teacher_by_group.ID_teacher = ce_teacher.ID_teacher');
		$this->db->join('ce_course', 'ce_teacher_by_group.ID_curse = ce_course.ID_curse');
		$this->db->join('ce_campus_career', 'ce_campus_career.ID_campus_career = ce_group.ID_campus_career');
		$this->db->join('ce_career', 'ce_career.ID_career = ce_campus_career.ID_career');
		//$this->db->join('ce_campus', 'ce_campus.ID_campus = ce_campus_career.ID_campus');
		$this->db->where('ce_teacher_by_group.status', 1);
		$this->db->where('ce_career.ID_career', $id_career);
		$this->db->where('year_active', $anio_actual);
		$this->db->where('cycle', $ciclo_actual);
		$result = $this->db->get();
		return $result->result_array();
	}

	//funcion que trae a todos los estudiantes del grupo seleccionado
	public function get_all_students_by_id_group($anio_actual, $ciclo_actual, $id_group, $campus)
	{
		$this->db->select('
        ce_rating_student.ID_rating_student,
        ce_rating_student.ID_student,
        ce_rating_student.ID_teacher_by_group,
        ce_rating_student.score,
        ce_rating_student.validate,
        ce_rating_student.`status`,
        ce_rating_student.created_at,
        ce_rating_student.updated_at,
        ce_student.names,
        ce_student.surnames,
        ce_student.sex,
        ce_student.accountNumber,
        ce_student.curp,
        ce_student.birthdate,
        ce_student.generation
        ');
		$this->db->from('ce_rating_student');
		$this->db->join('ce_student', 'ce_rating_student.ID_student = ce_student.ID_student');
		$this->db->join('ce_teacher_by_group', 'ce_rating_student.ID_teacher_by_group = ce_teacher_by_group.ID_teacher_by_group');
		$this->db->join('ce_group', 'ce_teacher_by_group.ID_group = ce_group.ID_group');
		$this->db->join('ce_campus_career', 'ce_group.ID_campus_career = ce_campus_career.ID_campus_career');
		$this->db->where('ce_group.year_active', $anio_actual);
		$this->db->where('ce_group.cycle', $ciclo_actual);
		$this->db->where('ce_campus_career.ID_campus', $campus);
		$this->db->where('ce_teacher_by_group.ID_teacher_by_group', $id_group);
		$this->db->where('ce_student.status', 1);
		$this->db->where('ce_rating_student.status', 1);
		$this->db->where('ce_rating_student.rectificated', 0);
		$result = $this->db->get();
		return $result->result_array();
	}

	public function get_all_students_by_id_group_validate($id_group, $campus)
	{
		$this->db->select('
        ce_rating_student.ID_rating_student,
        ce_rating_student.ID_student,
        ce_rating_student.ID_teacher_by_group,
        ce_rating_student.score,
        ce_rating_student.validate,
        ce_rating_student.`status`,
        ce_rating_student.created_at,
        ce_rating_student.updated_at,
        ce_student.names,
        ce_student.surnames,
        ce_student.sex,
        ce_student.accountNumber,
        ce_student.curp,
        ce_student.birthdate,
        ce_student.generation
        ');
		$this->db->from('ce_rating_student');
		$this->db->join('ce_student', 'ce_rating_student.ID_student = ce_student.ID_student');
		$this->db->join('ce_teacher_by_group', 'ce_rating_student.ID_teacher_by_group = ce_teacher_by_group.ID_teacher_by_group');
		$this->db->join('ce_group', 'ce_teacher_by_group.ID_group = ce_group.ID_group');
		$this->db->join('ce_campus_career', 'ce_group.ID_campus_career = ce_campus_career.ID_campus_career');
		$this->db->where('ce_campus_career.ID_campus', $campus);
		$this->db->where('ce_teacher_by_group.ID_teacher_by_group', $id_group);
		$this->db->where('ce_student.status', 1);
		$this->db->where('ce_teacher_by_group.qualified', 1);
		$this->db->where('ce_rating_student.status', 1);
		$result = $this->db->get();
		return $result->result_array();
	}

	/**
	 * Para traer a los todos los alumnos
	 * no importa so estan rectificados o no
	 */
	public function get_all_students_by_id_group_acta($id_group)
	{
		$this->db->select('
        ce_rating_student.ID_rating_student,
        ce_rating_student.ID_student,
        ce_rating_student.ID_teacher_by_group,
        ce_rating_student.score,
        ce_rating_student.validate,
        ce_rating_student.`status`,
        ce_rating_student.created_at,
        ce_rating_student.updated_at,
        ce_student.names,
        ce_student.surnames,
        ce_student.sex,
        ce_student.accountNumber,
        ce_student.curp,
        ce_student.birthdate,
        ce_student.generation
        ');
		$this->db->from('ce_rating_student');
		$this->db->join('ce_student', 'ce_rating_student.ID_student = ce_student.ID_student');
		$this->db->where('ce_student.status', 1);
		$this->db->where('ce_rating_student.status', 1);
		$this->db->where('ce_rating_student.ID_teacher_by_group', $id_group);
		$result = $this->db->get();
		return $result->result_array();
	}

	//funcion que trae a todos los estudiantes que tubieron rectificados del grupo seleccionado
	public function get_all_students_rectificados_by_id_group($id_group)
	{
		$this->db->select('
        ce_rating_student.ID_rating_student,
        ce_rating_student.ID_student,
        ce_rating_student.ID_teacher_by_group,
        ce_rating_student.score,
        ce_rating_student.validate,
        ce_rating_student.`status`,
        ce_rating_student.created_at,
        ce_rating_student.updated_at,
        ce_student.names,
        ce_student.surnames,
        ce_student.sex,
        ce_student.accountNumber,
        ce_student.curp,
        ce_student.birthdate,
        ce_student.generation
        ');
		$this->db->from('ce_rating_student');
		$this->db->join('ce_student', 'ce_rating_student.ID_student = ce_student.ID_student');
		$this->db->where('ce_student.status', 1);
		$this->db->where('ce_rating_student.status', 1);
		$this->db->where('ce_rating_student.rectificated', 1);
		$this->db->where('ID_teacher_by_group', $id_group);
		$result = $this->db->get();
		return $result->result_array();
	}

	//funcion para actualizar calificaciones
	public function update_rectificated_student($ID_rating_student)
	{
		return ($this->db->update(
			'ce_rating_student',
			['rectificated' => 1],
			['ID_rating_student' => $ID_rating_student]
		) ? TRUE : FALSE);
	}

	public function update_desrectificated_student($ID_rating_student)
	{
		return ($this->db->update(
			'ce_rating_student',
			['rectificated' => 0],
			['ID_rating_student' => $ID_rating_student]
		) ? TRUE : FALSE);
	}

	//funcion para rectificar grupo
	public function rectificar_grupo_by_id_group($ID_teacher_by_group)
	{
		return ($this->db->update(
			'ce_teacher_by_group',
			['rectificated' => 1],
			['ID_teacher_by_group' => $ID_teacher_by_group]
		) ? TRUE : FALSE);
	}

	//funcion para derectificar grupo
	public function desrectificar_grupo_by_id_group($ID_teacher_by_group)
	{
		return ($this->db->update(
			'ce_teacher_by_group',
			['rectificated' => 0],
			['ID_teacher_by_group' => $ID_teacher_by_group]
		) ? TRUE : FALSE);
	}

	//funcion para derectificar alumnos
	public function desrectificate_all_students_by_id_group($ID_teacher_by_group)
	{
		return ($this->db->update(
			'ce_rating_student',
			['rectificated' => 0],
			['ID_teacher_by_group' => $ID_teacher_by_group]
		) ? TRUE : FALSE);
	}

	public function validate_grupos($id_grupos, $campus_carrer, $ciclo, $tipo_grupo, $year)
	{
		$sql = "UPDATE ce_teacher_by_group " .
			"JOIN ce_group ON ce_teacher_by_group.ID_group = ce_group.ID_group " .
			"SET ce_teacher_by_group.validated = 1  " .
			"WHERE ce_group.year_active = " . $year .
			" AND ce_group.ID_type_group= " . $tipo_grupo .
			" AND ce_group.cycle= " . "'" . $ciclo . "'" .
			" AND ce_group.ID_campus_career= " . $campus_carrer .
			" AND ce_teacher_by_group.ID_teacher_by_group in(" . implode(", ", $id_grupos) . ")";
		$this->db->query($sql);
		return ($this->db->affected_rows() > 0 ? TRUE : FALSE);
	}

//    public function validate_grupos($id_grupos, $campus_carrer, $ciclo, $tipo_grupo, $year) {
//        
//        $this->db->set('ce_teacher_by_group.validated',1);
//        $this->db->where('ce_group.year_active', $year);
//        $this->db->where('ce_group.ID_type_group', $tipo_grupo);
//        $this->db->where_in('ce_teacher_by_group.ID_teacher_by_group', $id_grupos);
//        $this->db->where('ce_group.cycle', $ciclo);
//        $this->db->where('ce_group.ID_campus_career', $campus_carrer);
////        $this->db->join( 'ce_group',' ce_teacher_by_group.ID_group = ce_group.ID_group');
//        $this->db->where('ce_teacher_by_group.ID_group = ce_group.ID_group');
//        $this->db->update('ce_teacher_by_group , ce_group');
//
//    }
	//funcion para traer los datos del grupo
	public function get_group($id_group, $anio_actual, $ciclo_actual, $campus)
	{
		$this->db->select('
        ce_course.subject_name,
        ce_course.key_curse,
        ce_group.year_active,
        ce_group.cycle,
        ce_group.group_name,
        ce_group.ID_type_group,
        ce_teacher.rfc,
        ce_teacher.name,
        ce_teacher.surnames,
        ce_teacher.ruta_firm_auto,
        ce_teacher.ID_teacher,
        ce_type_group.type_group,
        ce_teacher_by_group.ID_teacher_by_group,
        ce_teacher_by_group.folio_acta,
        ce_teacher_by_group.updated_at,
        ce_career.short_name');
		$this->db->from('ce_teacher_by_group');
		$this->db->join('ce_group', 'ce_teacher_by_group.ID_group = ce_group.ID_group');
		$this->db->join('ce_course', 'ce_teacher_by_group.ID_curse = ce_course.ID_curse');
		$this->db->join('ce_teacher', 'ce_teacher_by_group.ID_teacher = ce_teacher.ID_teacher');
		$this->db->join('ce_type_group', 'ce_group.ID_type_group = ce_type_group.ID_type_group');
		$this->db->join('ce_campus_career', 'ce_campus_career.ID_campus_career = ce_group.ID_campus_career');
		$this->db->join('ce_career', 'ce_career.ID_career = ce_campus_career.ID_career');
		$this->db->where('ce_teacher_by_group.status', 1);
		$this->db->where('ce_group.status', 1);
		$this->db->where('ce_teacher.status', 1);
		$this->db->where('ce_group.year_active', $anio_actual);
		$this->db->where('ce_group.cycle', $ciclo_actual);
		$this->db->where('ce_campus_career.ID_campus', $campus);
		$this->db->where('ce_teacher_by_group.ID_teacher_by_group', $id_group);
		$result = $this->db->get();
		//print_r($this->db->last_query());
		return $result->result_array();
	}

	//funcion para traer los datos del grupo
	public function get_group_validate($id_group, $campus)
	{
		$this->db->select('
        ce_course.subject_name,
        ce_course.key_curse,
        ce_group.year_active,
        ce_group.cycle,
        ce_group.group_name,
        ce_group.ID_type_group,
        ce_teacher.rfc,
        ce_teacher.name,
        ce_teacher.surnames,
        ce_teacher.ruta_firm_auto,
        ce_teacher.ID_teacher,
        ce_type_group.type_group,
        ce_teacher_by_group.validated,
        ce_teacher_by_group.qualified,
        ce_teacher_by_group.ID_teacher_by_group,
        ce_teacher_by_group.folio_acta,
        ce_teacher_by_group.updated_at,
        ce_career.short_name');
		$this->db->from('ce_teacher_by_group');
		$this->db->join('ce_group', 'ce_teacher_by_group.ID_group = ce_group.ID_group');
		$this->db->join('ce_course', 'ce_teacher_by_group.ID_curse = ce_course.ID_curse');
		$this->db->join('ce_teacher', 'ce_teacher_by_group.ID_teacher = ce_teacher.ID_teacher');
		$this->db->join('ce_type_group', 'ce_group.ID_type_group = ce_type_group.ID_type_group');
		$this->db->join('ce_campus_career', 'ce_campus_career.ID_campus_career = ce_group.ID_campus_career');
		$this->db->join('ce_career', 'ce_career.ID_career = ce_campus_career.ID_career');
		$this->db->where('ce_teacher_by_group.status', 1);
		$this->db->where('ce_group.status', 1);
//        $this->db->where('ce_teacher_by_group.qualified', 1);
		$this->db->where('ce_teacher.status', 1);
		$this->db->where('ce_campus_career.ID_campus', $campus);
		$this->db->where('ce_teacher_by_group.ID_teacher_by_group', $id_group);
		$result = $this->db->get();
		//print_r($this->db->last_query());
		return $result->result_array();
	}

	//funcion  que trae al profesor dependiedo del rfc a buscar

	public function get_data_teacher_by_id($ID_teacher)
	{
		$this->db->select('
        ce_teacher.ID_teacher,
        ce_teacher.rfc,
        ce_teacher.name,
        ce_teacher.surnames,
        case  
   when sex=1 then "Masculino"
   when sex=0 then "Femenino"
        end as sexo', FALSE);
		$this->db->from('ce_teacher');
		$this->db->where('ce_teacher.status', 1);
		$this->db->where('ce_teacher.ID_teacher', $ID_teacher);
		$this->db->limit(1);
		$result = $this->db->get();
		return $result->row();
	}

	public function get_all_teacher($anio_actual, $ciclo_actual)
	{
		$this->db->select(' DISTINCT
            ce_teacher.`name`,
            ce_teacher.surnames,
            ce_teacher.rfc,
            ce_user.email,
            ce_user.name_user,
            ce_teacher.key_validate,
            ce_teacher.descargas_fiel,
            ce_teacher.ID_teacher,
            ce_plan.key_plan,
            ce_teacher.ID_user', false);
		$this->db->from('ce_teacher');
		$this->db->join('ce_teacher_by_group', 'ce_teacher_by_group.ID_teacher = ce_teacher.ID_teacher');
		$this->db->join('ce_course', 'ce_teacher_by_group.ID_curse = ce_course.ID_curse');
		$this->db->join('ce_group', 'ce_teacher_by_group.ID_group = ce_group.ID_group');
		$this->db->join('ce_plan', 'ce_course.id_plan = ce_plan.ID_plan');
		$this->db->join('ce_user', 'ce_user.ID_user = ce_teacher.ID_user');
		$this->db->where('ce_teacher.status', 1);
		//$this->db->where('ce_group.year_active', $anio_actual);
		//$this->db->where('ce_group.cycle', $ciclo_actual); 
		$result = $this->db->get();
		return $result->result_array();
	}

	public function get_groups_by_id_teacher($id_teacher, $anio_actual, $ciclo_actual, $campus)
	{
		$this->db->select('
            ce_course.subject_name,
            ce_course.key_curse,
            ce_group.year_active,
            ce_group.cycle,
            ce_group.group_name,
            ce_type_group.type_group,
            ce_career.short_name,
            SUBSTRING(ce_career.career_name, 17) AS career_name,
            ce_teacher_by_group.ID_teacher,
            ce_teacher_by_group.qualified,
            ce_teacher_by_group.rectificated,
            CASE
            WHEN qualified = 1 THEN
                    "Calificado"
            WHEN qualified = 0 THEN
                    "No calificado"
            END AS calificado', FALSE);
		$this->db->from('ce_teacher_by_group');
		$this->db->join('ce_group', 'ce_teacher_by_group.ID_group = ce_group.ID_group');
		$this->db->join('ce_course', 'ce_teacher_by_group.ID_curse = ce_course.ID_curse');
		$this->db->join('ce_teacher', 'ce_teacher_by_group.ID_teacher = ce_teacher.ID_teacher');
		$this->db->join('ce_type_group', 'ce_group.ID_type_group = ce_type_group.ID_type_group');
		$this->db->join('ce_campus_career', 'ce_campus_career.ID_campus_career = ce_group.ID_campus_career');
		$this->db->join('ce_career', 'ce_career.ID_career = ce_campus_career.ID_career');
		$this->db->where('ce_teacher_by_group.status', 1);
		$this->db->where('ce_group.status', 1);
		$this->db->where('ce_teacher.status', 1);
		$this->db->where('ce_campus_career.ID_campus', $campus);
		$this->db->where('ce_teacher.ID_teacher', $id_teacher);
		$this->db->where('ce_group.year_active', $anio_actual);
		$this->db->where('ce_group.cycle', $ciclo_actual);
		$result = $this->db->get();
		//        print_r($this->db->last_query());
		return $result->result_array();
	}

	public function get_count_actas_by_id_teacher($id_teacher, $anio_actual, $ciclo_actual, $campus)
	{
		$this->db->select('
                        CASE
        WHEN qualified = 1 THEN
                "Calificado"
        WHEN qualified = 0 THEN
                "No calificado"
        END AS calificado,
        COUNT(qualified) as cantidad_calificado', FALSE);
		$this->db->from('ce_teacher_by_group');
		$this->db->join('ce_group', 'ce_teacher_by_group.ID_group = ce_group.ID_group');
		$this->db->join('ce_course', 'ce_teacher_by_group.ID_curse = ce_course.ID_curse');
		$this->db->join('ce_teacher', 'ce_teacher_by_group.ID_teacher = ce_teacher.ID_teacher');
		$this->db->join('ce_type_group', 'ce_group.ID_type_group = ce_type_group.ID_type_group');
		$this->db->join('ce_campus_career', 'ce_campus_career.ID_campus_career = ce_group.ID_campus_career');
		$this->db->join('ce_career', 'ce_career.ID_career = ce_campus_career.ID_career');
		$this->db->where('ce_teacher_by_group.status', 1);
		$this->db->where('ce_group.status', 1);
		$this->db->where('ce_teacher.status', 1);
		$this->db->where('ce_teacher_by_group.ID_teacher', $id_teacher);
		$this->db->where('ce_group.year_active', $anio_actual);
		$this->db->where('ce_group.cycle', $ciclo_actual);
		$this->db->where('ce_campus_career.ID_campus', $campus);
		$this->db->group_by('qualified');
		$result = $this->db->get();
		//        print_r($this->db->last_query());
		return $result->result_array();
	}

	public function get_count_gruops_validated($id_camp_carr, $year, $ciclo, $tipo_group, $sede)
	{
		$this->db->select('count(ce_teacher_by_group.ID_teacher_by_group) as cantidad');
		$this->db->from('ce_teacher_by_group');
		$this->db->join('ce_group', 'ce_teacher_by_group.ID_group = ce_group.ID_group');
		$this->db->join('ce_teacher', 'ce_teacher_by_group.ID_teacher = ce_teacher.ID_teacher');
		$this->db->join('ce_course', 'ce_teacher_by_group.ID_curse = ce_course.ID_curse');
		$this->db->join('ce_campus_career', 'ce_group.ID_campus_career = ce_campus_career.ID_campus_career');
		$this->db->join('ce_career', 'ce_campus_career.ID_career = ce_career.ID_career');
		$this->db->where('ce_group.year_active', $year);
		$this->db->where('ce_group.ID_type_group', $tipo_group);
		$this->db->where('ce_group.cycle', $ciclo);
		$this->db->where('ce_campus_career.ID_campus_career', $id_camp_carr);
		$this->db->where('ce_campus_career.ID_campus', $sede);
		$this->db->where('ce_teacher_by_group.status', 1);
		$this->db->where('ce_group.status', 1);
		$this->db->where('ce_teacher.status', 1);
		$this->db->where('ce_teacher_by_group.qualified', 1);
		$this->db->where('ce_teacher_by_group.validated', 1);
		$result = $this->db->get();
		return $result->row();
	}

	//// revisar esto como manejan los ciclos
	public function get_count_actas_by_campus($anio_actual, $ciclo_actual, $campus)
	{
		$this->db->select('
                        ce_campus.ID_campus,
                        ce_campus.campus_name,
                        count(qualified) as total,
                        sum(case when qualified = 1 THEN
			1 ELSE 0 end) AS calificados', FALSE);
		$this->db->from('ce_teacher_by_group');
		$this->db->join('ce_group', 'ce_teacher_by_group.ID_group = ce_group.ID_group');
		$this->db->join('ce_campus_career', 'ce_campus_career.ID_campus_career = ce_group.ID_campus_career');
		$this->db->join('ce_campus', ' ce_campus_career.ID_campus = ce_campus.ID_campus');
		$this->db->where('ce_teacher_by_group.status', 1);
		$this->db->where('year_active', $anio_actual);
		$this->db->where('cycle', $ciclo_actual);
		$this->db->where('ce_campus_career.ID_campus', $campus);
		$this->db->group_by('ce_campus.ID_campus,campus_name');
		$result = $this->db->get();
		//        print_r($this->db->last_query());
		return $result->result_array();
	}

	public function get_count_actas_by_career($anio_actual, $ciclo_actual, $campus)
	{
		$this->db->select('
                        ce_career.ID_career,
                        ce_career.career_name,
                        count(qualified) as total,
                        sum(case when qualified = 1 THEN
			1 ELSE 0 end) AS calificados', FALSE);
		$this->db->from('ce_teacher_by_group');
		$this->db->join('ce_group', 'ce_teacher_by_group.ID_group = ce_group.ID_group');
		$this->db->join('ce_campus_career', 'ce_campus_career.ID_campus_career = ce_group.ID_campus_career');
		$this->db->join('ce_career', 'ce_campus_career.ID_career = ce_career.ID_career');
		$this->db->where('ce_teacher_by_group.status', 1);
		$this->db->where('year_active', $anio_actual);
		$this->db->where('cycle', $ciclo_actual);
		$this->db->where('ce_campus_career.ID_campus', $campus);
		$this->db->group_by('ce_career.career_name,ce_career.ID_career');
		$result = $this->db->get();
		return $result->result_array();
	}

	//funcion para insertar la firma y su contraseña
	public function update_firma_and_pass($ID_teacher, $firma, $pass_firma)
	{
		return ($this->db->update('ce_teacher', ['key_validate' => $firma, 'key_pass' => $pass_firma], ['ID_teacher' => $ID_teacher]
		) ? TRUE : FALSE);
	}

	public function update_firma_autografa($ID_teacher, $ruta_firma_autografa)
	{
		return ($this->db->update('ce_teacher', ['ruta_firm_auto' => $ruta_firma_autografa], ['ID_teacher' => $ID_teacher]
		) ? TRUE : FALSE);
	}

	// traer datos para generar la firma
	public function get_info_firma($id_teacher)
	{
		$this->db->select('*');
		$this->db->from('ce_user u');
		$this->db->join('ce_teacher t', 'u.ID_user = t.ID_user');
		$this->db->where('t.ID_teacher', $id_teacher);
		$result = $this->db->get();
		return $result->result_array();
	}

	public function get_list_lic_by_campus($id_campus)
	{
		$this->db->select('ce_career.career_name,ce_campus_career.ID_campus_career');
		$this->db->from('ce_campus_career');
		$this->db->join('ce_career', ' ce_campus_career.ID_career = ce_career.ID_career');
		$this->db->where('ID_campus', $id_campus);
		$result = $this->db->get();
		return $result->result_array();
	}

	public function get_year_by_campus_lic($id_camp_carr)
	{
		$this->db->select('DISTINCT year_active', false);
		$this->db->from('ce_group');
		$this->db->join('ce_teacher_by_group', 'ce_teacher_by_group.ID_group = ce_group.ID_group');
		$this->db->where('ce_group.ID_campus_career', $id_camp_carr);
		$result = $this->db->get();
		return $result->result_array();
	}

	public function get_cicles($id_camp_carr, $year)
	{
		$this->db->select('DISTINCT ce_group.cycle', false);
		$this->db->from('ce_group');
		$this->db->join('ce_teacher_by_group', 'ce_teacher_by_group.ID_group = ce_group.ID_group');
		$this->db->where('ce_group.ID_campus_career', $id_camp_carr);
		$this->db->where('ce_group.year_active', $year);
		$result = $this->db->get();
		return $result->result_array();
	}

	public function get_type_group($id_camp_carr, $year, $ciclo)
	{
		$this->db->select('DISTINCT ce_group.ID_type_group,type_group', false);
		$this->db->from('ce_group');
		$this->db->join('ce_teacher_by_group', 'ce_teacher_by_group.ID_group = ce_group.ID_group');
		$this->db->join('ce_type_group', 'ce_group.ID_type_group = ce_type_group.ID_type_group');
		$this->db->where('ce_group.ID_campus_career', $id_camp_carr);
		$this->db->where('ce_group.year_active', $year);
		$this->db->where('ce_group.cycle', $ciclo);
		$result = $this->db->get();
		return $result->result_array();
	}

	public function get_all_groups_by_validate($id_camp_carr, $year, $ciclo, $tipo_group, $sede)
	{
		$this->db->select('
        ce_course.subject_name,
        ce_course.key_curse,
        ce_teacher_by_group.validated,
        ce_teacher_by_group.qualified,
           ce_group.group_name,
    ce_career.career_name,
        ce_group.year_active,
        ce_teacher_by_group.ID_teacher_by_group');
		$this->db->from('ce_teacher_by_group');
		$this->db->join('ce_group', 'ce_teacher_by_group.ID_group = ce_group.ID_group');
		$this->db->join('ce_teacher', 'ce_teacher_by_group.ID_teacher = ce_teacher.ID_teacher');
		$this->db->join('ce_course', 'ce_teacher_by_group.ID_curse = ce_course.ID_curse');
		$this->db->join('ce_campus_career', 'ce_group.ID_campus_career = ce_campus_career.ID_campus_career');
		$this->db->join('ce_career', 'ce_campus_career.ID_career = ce_career.ID_career');
		$this->db->where('ce_group.year_active', $year);
		$this->db->where('ce_group.ID_type_group', $tipo_group);
		$this->db->where('ce_group.cycle', $ciclo);
		$this->db->where('ce_teacher_by_group.status', 1);
		$this->db->where('ce_campus_career.ID_campus_career', $id_camp_carr);
		$this->db->where('ce_campus_career.ID_campus', $sede);
		$this->db->where('ce_teacher_by_group.qualified', 1);
		$result = $this->db->get();
		//        print_r($this->db->last_query());
		return $result->result_array();
	}

	public function get_count_gruops($id_camp_carr, $year, $ciclo, $tipo_group, $sede)
	{
		$this->db->select('count(ce_teacher_by_group.ID_teacher_by_group) as cantidad');
		$this->db->from('ce_teacher_by_group');
		$this->db->join('ce_group', 'ce_teacher_by_group.ID_group = ce_group.ID_group');
		$this->db->join('ce_teacher', 'ce_teacher_by_group.ID_teacher = ce_teacher.ID_teacher');
		$this->db->join('ce_course', 'ce_teacher_by_group.ID_curse = ce_course.ID_curse');
		$this->db->join('ce_campus_career', 'ce_group.ID_campus_career = ce_campus_career.ID_campus_career');
		$this->db->join('ce_career', 'ce_campus_career.ID_career = ce_career.ID_career');
		$this->db->where('ce_group.year_active', $year);
		$this->db->where('ce_group.ID_type_group', $tipo_group);
		$this->db->where('ce_group.cycle', $ciclo);
		$this->db->where('ce_teacher_by_group.status', 1);
		$this->db->where('ce_campus_career.ID_campus_career', $id_camp_carr);
		$this->db->where('ce_campus_career.ID_campus', $sede);
		$result = $this->db->get();
		return $result->row();
	}

}
