<?php
class Login_access extends CI_Model
{
    private $users;

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->users = 'ce_user';
    }
    public function login($data)
    {
        
        $datos = $this->db->get_where($this->users, array('email' => $data['email_ua'],'ID_type_user' => 1), 1);
        if (!$datos->row()) {
            return false;
        }
//
        return $datos->row();
    }
    public function login_teacher($data)
    {
        
        $datos = $this->db->get_where($this->users, array('email' => $data['email_ua'],'ID_type_user' => 2), 1);
        if (!$datos->row()) {
            return false;
        }

        return $datos->row();
    }
    public function login_admin($data)
    {
        
        $datos = $this->db->get_where($this->users, array('email' => $data['email_ua']), 1);
        if (!$datos->row()) {
            return false;
        }

        return $datos->row();
    }
    public function login_admin_jef_carr($data)
    {
        
        $datos = $this->db->get_where($this->users, array('email' => $data['email_ua'],'ID_type_user' => 4), 1);
        if (!$datos->row()) {
            return false;
        }

        return $datos->row();
    }

    public function datos_prof_pass($correo)
    {
        
        $datos = $this->db->get_where($this->users, array('email' => $correo), 1);
        if (!$datos->row()) {
            return false;
        }

        return $datos->row();
    }

    //funcion para actualizar la contraseña
    public function update_pass($pass, $id_user) {
        return ($this->db->update(
                        'ce_user',
                        ['password' => $pass,'status' => 0],
                        ['ID_user' => $id_user]
                ) ? TRUE : FALSE);
    }

    public function update_status( $id_user) {
        return ($this->db->update(
                        'ce_user',
                        ['status' => $pass,'status' => 1],
                        ['ID_user' => $id_user]
                ) ? TRUE : FALSE);
    }
}
