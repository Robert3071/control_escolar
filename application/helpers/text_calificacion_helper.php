<?php

if (!function_exists('num_to_text')) {

    function num_to_text($cal_num) {

        convertir($cal_num);
    }

}
if (!function_exists('normalize_calif')) {

    function normalize_calif($indice) {

        $list_num = array(
            '0' => 'CERO',
            '1' => 'UNO',
            '2' => 'DOS',
            '3' => 'TRES',
            '4' => 'CUATRO',
            '5' => 'CINCO',
            '6' => 'SEIS',
            '7' => 'SIETE',
            '8' => 'OCHO',
            '9' => 'NUEVE',
            '10' => ' DIEZ',
            '11' => ' ONCE',
            '12' => ' DOCE',
            '13' => ' TRECe',
            '14' => ' CATORCE',
            '15' => ' QUINCE',
            '16' => ' DIECISÉIS',
            '17' => ' DIECISIETE',
            '18' => ' DIECIOCHO',
            '19' => ' DIECINUEVE',
            '20' => ' VEINTE',
            '21' => ' VEINTIUNO',
            '22' => ' VEINTIDÓS',
            '23' => ' VEINTITRÉS',
            '24' => ' VEINTICUATRO',
            '25' => ' VEINTICINCO',
            '26' => ' VEINTISÉIS',
            '27' => ' VEINTISIETE',
            '28' => ' VEINTIOCHO',
            '29' => ' VEINTINUEVE',
            '30' => 'TREINTA',
            '31' => 'TREINTA Y UNO',
            '32' => 'TREINTA Y DOS',
            '33' => 'TREINTA Y TRES',
            '34' => 'TREINTA Y CUATRO',
            '35' => 'TREINTA Y CINCO',
            '36' => 'TREINTA Y SEIS',
            '37' => 'TREINTA Y SIETE',
            '38' => 'TREINTA Y OCHO',
            '39' => 'TREINTA Y NUEVE',
            '40' => 'CUARENTA',
            '41' => 'CUARENTA Y UNO',
            '42' => 'CUARENTA Y DOS',
            '43' => 'CUARENTA Y TRES',
            '44' => 'CUARENTA Y CUATRO',
            '45' => 'CUARENTA Y CINCO',
            '46' => 'CUARENTA Y SEIS',
            '47' => 'CUARENTA Y SIETE',
            '48' => 'CUARENTA Y OCHO',
            '49' => 'CUARENTA Y NUEVE',
            '50' => 'CINCUENTA',
            '51' => 'CINCUENTA Y UNO',
            '52' => 'CINCUENTA Y DOS',
            '53' => 'CINCUENTA Y TRES',
            '54' => 'CINCUENTA Y CUATRO',
            '55' => 'CINCUENTA Y CINCO',
            '56' => ' CINCUENTA Y SEIS',
            '57' => ' CINCUENTA Y SIETE',
            '58' => ' CINCUENTA Y OCHO',
            '59' => ' CINCUENTA Y NUEVE',
            '60' => ' SESENTA',
            '61' => ' SESENTA Y UNO',
            '62' => ' SESENTA Y DOS',
            '63' => ' SESENTA Y TRES',
            '64' => ' SESENTA Y CUATRO',
            '65' => ' SESENTA Y CINCO',
            '66' => ' SESENTA Y SEIS',
            '67' => ' SESENTA Y SIETE',
            '68' => ' SESENTA Y OCHO',
            '69' => ' SESENTA Y NUEVE',
            '70' => ' SETENTA',
            '71' => ' SETENTA Y UNO',
            '72' => ' SETENTA Y DOS',
            '73' => ' SETENTA Y TRES',
            '74' => ' SETENTA Y CUATRO',
            '75' => ' SETENTA Y CINCO',
            '76' => ' SETENTA Y SEIS',
            '77' => ' SETENTA Y SIETE',
            '78' => ' SETENTA Y OCHO',
            '79' => ' SETENTA Y NUEVE',
            '80' => ' OCHENTA',
            '81' => ' OCHENTA Y UNO',
            '82' => ' OCHENTA Y DOS',
            '83' => ' OCHENTA Y TRES',
            '84' => ' OCHENTA Y CUATRO',
            '85' => ' OCHENTA Y CINCO',
            '86' => ' OCHENTA Y SEIS',
            '87' => ' OCHENTA Y SIETE',
            '88' => ' OCHENTA Y OCHO',
            '89' => ' OCHENTA Y NUEVE',
            '90' => ' NOVENTA',
            '91' => 'NOVENTA Y UNO',
            '92' => ' NOVENTA Y DOS',
            '93' => ' NOVENTA Y TRES',
            '94' => ' NOVENTA Y CUATRO',
            '95' => ' NOVENTA Y CINCO',
            '96' => ' NOVENTA Y SEIS',
            '97' => ' NOVENTA Y SIETE',
            '98' => ' NOVENTA Y OCHO',
            '99' => ' NOVENTA Y NUEVE',
            '100' => 'CIEN',
            '00' => 'CERO CERO',
            '01' => 'CERO UNO',
            '02' => 'CERO DOS',
            '03' => 'CERO TRES',
            '04' => 'CERO CUATRO',
            '05' => 'CERO CINCO',
            '06' => 'CERO SEIS',
            '07' => 'CERO SIETE',
            '08' => 'CERO OCHO',
            '09' => 'CERO NUEVE',
            'NP' => 'NP',
            'SC' => 'Sin cursar',
        );
        if (array_key_exists($indice, $list_num)) {
            return $list_num[$indice];
        } else {
            return FALSE;
        }
    }

}

function convertir($promedio) {
    $promedio_array = explode('.', $promedio);
    if (strpos($promedio,".")) {
        if (normalize_calif($promedio_array[0]) && normalize_calif($promedio_array[1])) {
            $text = normalize_calif($promedio_array[0]) . ' PUNTO ' . normalize_calif($promedio_array[1]);
        } else {
            $text = "CALIFICACI&Oacute;N NO VALIDA";
        }
    } else {
        if (normalize_calif($promedio_array[0])) {
            $text = normalize_calif($promedio_array[0]);
        } else {
            $text = "CALIFICACI&Oacute;N NO VALIDA";
        }
    }
    echo $text;
}

if (!function_exists('ordenar_array_asc_by_elemento')) {

	function ordenar_array_asc_by_elemento($array_nuevo, $elemento_a_ordenar)
	{

		$ordenar = array($elemento_a_ordenar => array());
		foreach ($array_nuevo as $clave => $nuevo_a) {
			$ordenar[$elemento_a_ordenar][$clave] = $nuevo_a[$elemento_a_ordenar];
		}
		array_multisort($ordenar[$elemento_a_ordenar], SORT_ASC, $array_nuevo);
		return $array_nuevo;
	}
}
