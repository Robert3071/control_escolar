<?php
function sexs_list()
{
    $data = array(
        ''  => 'Seleccione su sexo.',
        '1' => 'Hombre',
        '2' => 'Mujer'
    );
    return $data;
}

function civil_state()
{
    $data = array(
        ''  => 'Seleccione una opción.',
        '1' => 'Soltero(a)',
        '2' => 'Casado(a)',
        '3' => 'Unión libre',
        '4' => 'Divorciado(a)'
    );
    return $data;
}

function friendship()
{
    $data = array(
        ''  => 'Seleccione una opción.',
        '1' => 'Padres',
        '2' => 'Pareja',
        '3' => 'Hermano(a)',
        '4' => 'Abuelos',
        '5' => 'Familiar',
        '6' => 'Otro',
    );
    return $data;
}

function isAlergy()
{
    $data = array(
        ''    => 'Seleccione una opción.',
        '1' => 'Sí',
        '2' => 'No'
    );
    return $data;
}


function isDisability()
{
    $data = array(
        ''  => 'Seleccione una opción.',
        '1' => 'Sí',
        '2' => 'No'
    );
    return $data;
}

function typeBlood()
{
    $data = array(
        ''  => 'Seleccione una opción.',
        '1' => 'Lo desconozco',
        '2' => 'A+',
        '3' => 'A-',
        '4' => 'B+',
        '5' => 'B-',
        '6' => 'O+',
        '7' => 'O-',
        '8' => 'AB+',
        '9' => 'AB-'
    );
    return $data;
}


function isCronic()
{
    $data = array(

        ''  => 'Seleccione una opción.',
        '1' => 'Sí',
        '2' => 'No'
    );
    return $data;
}

function atencionPsicology()
{
    $data = array(
        ''  => 'Seleccione una opción.',
        '1' => 'Sí',
        '2' => 'No'
    );
    return $data;
}

function school()
{
    $data = array(
        ''  => 'Seleccione una opción.',
        '1' => 'Vinculada a la SEP',
        '2' => 'Privada',
        '3' => 'UNAM',
        '4' => 'IPN',
        '5' => 'Abierta',
    );
    return $data;
}

function schoolIRC()
{
    $data = array(
        ''  => 'Seleccione una Unidad.',
        '1' => 'Unidad Académica Azcapotzalco',
        '2' => 'Unidad Académica Coyoacán',
        '3' => 'Unidad Académica Gustavo A. Madero',
        '4' => 'Unidad Académica Justo Sierra',
        '5' => 'Unidad Académica a Distancia'
        //'6' => 'Unidad Académica Magdalena Contreras'
    );
    return $data;
}

function degree()
{
    $data = array(
        ''  => 'Seleccione la licenciatura',
        '1' => 'Ingeniería en Control y Automatización',
        '2' => 'Licenciatura en Administración y Comercio',
        '3' => 'Licenciatura en Ciencias Ambientales',
        '4' => 'Licenciatura en Ciencias de la Comunicación',
        '5' => 'Licenciatura en Ciencias de Datos',
        '6' => 'Licenciatura en Contaduría y Finanzas',
        '7' => 'Licenciatura en Derecho y Criminología',
        //'8' => 'Licenciatura en Humanidades y Narrativas Multimedia',
        '9' => 'Licenciatura en Mercadotecnia y Ventas',
        '10' => 'Licenciatura en Psicología',
        '11' => 'Licenciatura en Relaciones Internacionales',
        '12' => 'Licenciatura en Tecnología de Información y Comunicación',
        '13' => 'Licenciatura en Turismo',
        '14' => 'Licenciatura en Urbanismo y Desarrollo Metropolitano'
    );
    return $data;
}

function internet()
{
    $data = array(
        ''  => 'Seleccione una opción.',
        '1' => 'Sí',
        '2' => 'No'
    );
    return $data;
}

function peopleHouse()
{
    $data = array(
        ''  => 'Seleccione una opción.',
        '1' => 'Vivo Soló',
        '2' => '1 a 3 personas',
        '3' => '4 a 6 perosnas',
        '4' => '7 a 10 personas',
        '5' => 'Más de 10 personas',
    );
    return $data;
}

function instancia()
{
    $data = array(
        ''  => 'Seleccione una opción.',
        '1' => 'No cuento con ninguna',
        '2' => 'IMSS',
        '3' => 'ISSSTE',
        '4' => 'Privado',
        '5' => 'otro',
    );
    return $data;
}


function hand()
{
    $data = array(
        ''  => 'Seleccione una opción.',
        '1' => 'Zurdo',
        '2' => 'Diestro'
    );
    return $data;
}

function indi()
{
    $data = array(
        ''  => 'Seleccione una opción.',
        '1' => 'Sí',
        '2' => 'No'
    );
    return $data;
}
