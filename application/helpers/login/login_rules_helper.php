<?php

function get_login_rules()
{
    return $config = array(
        array(
            'field'     => 'email',
            'label'     =>    'Correo electrónico',
            'rules'     =>    'required|valid_email',
            'errors'    =>    array(
                'required'        =>    'El %s es indispensable para loguearte',
                'valid_email'     =>    'El %s no es correcto'
            )
        ),
        array(
            'field'     => 'password',
            'label'     =>    'Contraseña',
            'rules'     =>    'required',
            'errors'    =>    array(
                'required'        =>    'La %s es indispensable para loguearte'
            )
        )
    );
}
