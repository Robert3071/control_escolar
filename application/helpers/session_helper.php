<?php

//este helper solo se usa en el login para redireccionar en caso de que tenga una sesion  ctiva ,mandarlos a su respectivo modulo
//los helpers estan hechos para mostrar los errores si no hay ningun error entonces la ejecucion sigue normalmente 
if (!function_exists('checar_session')) {

    function checar_session() {
        @session_start();
        $CI = &get_instance();
        if (isset($_SESSION['logueado'])) {
            if ($_SESSION['logueado'] == 1) {
                switch ($_SESSION['id_type']) {
                    case 2:
                        redirect('session/teacher/Teacher_dashboard');
                        break;
                    case 3 :
                        redirect('session/admin/Admin_dashboard/grupos');
                        break;
                    case 4:
                        redirect('session/admin_jef_carr/Admin_dashboard/grupos');
                        break;
                    case 5:
                        redirect('session/admin/Admin_dashboard/grupos');
                        break;
                }
            }
        }
    }

}
if (!function_exists('checar_rol_teacher')) {//uso exclusivo para ver sus datos personales del profesor

    function checar_rol_teacher() {
        $CI = &get_instance();
        $CI->load->helper('calendar');
        checar_rol([2]);
        $fecha_server = strtotime(date_today_mex());
        if ($fecha_server >= calendario_teacher_1_fecha_ini() && $fecha_server <= calendario_teacher_1_fecha_fin()) {
            calendario_teacher(calendario_docente_1(), 1);
        } elseif ($fecha_server >= calendario_teacher_2_fecha_ini() && $fecha_server <= calendario_teacher_2_fecha_fin()) {
            calendario_teacher(calendario_docente_2(), 2);
        } elseif ($fecha_server >= calendario_teacher_4_fecha_ini() && $fecha_server <= calendario_teacher_4_fecha_fin()) {
            calendario_teacher(calendario_docente_4(), 4);
        } elseif ($fecha_server >= calendario_teacher_5_fecha_ini() && $fecha_server <= calendario_teacher_5_fecha_fin()) {
            calendario_teacher(calendario_docente_5(), 5);
        } elseif ($fecha_server >= calendario_teacher_7_fecha_ini() && $fecha_server <= calendario_teacher_7_fecha_fin()) {

            calendario_teacher(calendario_docente_7(), 7);
        } else {
            @session_destroy();
            cerrado_view(2);
        }
    }

}


if (!function_exists('checar_teacher_calificar')) {

    function checar_teacher_calificar() {
        $CI = &get_instance();
        $CI->load->helper('calendar');
        checar_rol([2]);
        $fecha_server = strtotime(date_today_mex());
        if ($_SESSION['update_password'] == 1) {

            if ($fecha_server >= calendario_teacher_1_fecha_ini() && $fecha_server <= calendario_teacher_1_fecha_fin() && $_SESSION['update_password'] == 1) {
                calendario_teacher(calendario_docente_1(), 1);
//                echo 'aqui';
            } elseif ($fecha_server >= calendario_teacher_2_fecha_ini() && $fecha_server <= calendario_teacher_2_fecha_fin() && $_SESSION['update_password'] == 1) {

                calendario_teacher(calendario_docente_2(), 2);
            } else {//si tiene sesion pero las fechas ya se pasaron entonces mostrara que el sistema esta cerrado
                @session_destroy();
                cerrado_view(2);
            }
        } else {
            checar_session();
        }
    }

}
if (!function_exists('checar_teacher_calificar_extra')) {

    function checar_teacher_calificar_extra() {
        $CI = &get_instance();
        $CI->load->helper('calendar');
        checar_rol([2]);
        $fecha_server = strtotime(date_today_mex());
        if ($_SESSION['update_password'] == 1) {
            if ($fecha_server >= calendario_teacher_5_fecha_ini() && $fecha_server <= calendario_teacher_5_fecha_fin() && $_SESSION['update_password'] == 1) {
                calendario_teacher(calendario_docente_5(), 5);
            } else {//si tiene sesion pero las fechas ya se pasaron entonces mostrara que el sistema esta cerrado
                @session_destroy();
                cerrado_view(2);
            }
        } else {
            checar_session();
        }
    }

}
if (!function_exists('checar_teacher_rectificar')) {

    function checar_teacher_rectificar() {
        $CI = &get_instance();
        $CI->load->helper('calendar');
        checar_rol([2]);
        $fecha_server = strtotime(date_today_mex());
        if ($_SESSION['update_password'] == 1) {

            if ($fecha_server >= calendario_teacher_4_fecha_ini() && $fecha_server <= calendario_teacher_4_fecha_fin() && $_SESSION['update_password'] == 1) {
                calendario_teacher(calendario_docente_4(), 4);
            } elseif ($fecha_server >= calendario_teacher_7_fecha_ini() && $fecha_server <= calendario_teacher_7_fecha_fin() && $_SESSION['update_password'] == 1) {
                calendario_teacher(calendario_docente_7(), 7);
            } else {
                @session_destroy();
                cerrado_view(2);
            }
        } else {
            checar_session();
        }
    }

}
if (!function_exists('checar_teacher_rectificar_extras')) {

    function checar_teacher_rectificar_extras() {
        $CI = &get_instance();
        $CI->load->helper('calendar');
        checar_rol([2]);
        $fecha_server = strtotime(date_today_mex());
        if ($_SESSION['update_password'] == 1) {

            if ($fecha_server >= calendario_teacher_7_fecha_ini() && $fecha_server <= calendario_teacher_7_fecha_fin() && $_SESSION['update_password'] == 1) {
                calendario_teacher(calendario_docente_7(), 7);
            } else {
                @session_destroy();
                cerrado_view(2);
            }
        } else {
            checar_session();
        }
    }

}
if (!function_exists('checar_rol_admin')) {

    function checar_rol_admin() {
        checar_rol([3, 5]);
    }

}
if (!function_exists('checar_rol_admin_estudiantiles')) {

    function checar_rol_admin_estudiantiles() {
        checar_rol([5]);
    }

}
if (!function_exists('checar_rol_admin_jef_carre')) {

    function checar_rol_admin_jef_carre() {
        checar_rol([4]);
    }

}

//verifica que el usaurio al acceder con su rol tenga permisos para acceder
if (!function_exists('checar_rol')) {

    function checar_rol($rol) {
        @session_start();
        $CI = &get_instance();
        if (!(isset($_SESSION['id_type']) && isset($_SESSION['logueado']) && in_array($_SESSION['id_type'], $rol) && $_SESSION['logueado'] == 1)) {
            $data['message'] = 'Para ingresar a este sitio se debe iniciar sesi&oacute;n o tener permiso';
//resolver esta parte 403,401

            switch ($rol[0]) {
                case 1:
                    $data['login_url'] = base_url() . $CI->config->item('login_url'); //viene desde rest.php de cms
                    break;
                case 2:
                    $data['login_url'] = base_url() . $CI->config->item('login_teacher_url'); //viene desde rest.php de cms
                    break;
                case 3 ://admin super admin
                    $data['login_url'] = base_url() . $CI->config->item('login_admin_url'); //viene desde rest.php de cms
                case 4://admin jefe de carrera
                    $data['login_url'] = base_url() . $CI->config->item('login_jefe_carrera_url'); //viene desde rest.php de cms
                    break;
                case 5://admin asuntos estudiantiles-no peude rectificar
                    $data['login_url'] = base_url() . $CI->config->item('login_admin_url'); //viene desde rest.php de cms
                    break;
            }
            http_response_code(401);
            exit($CI->load->view('redirect_view', $data, TRUE));
        }
    }

}

//helpers para validar calendarios

function date_today_mex() {
    date_default_timezone_set('America/Mexico_City');
    return date('Y-m-d H:i:s');
}

//verifica que  su lic este entre las fechas permitidas
if (!function_exists('calendario_teacher')) {

    function calendario_teacher($calendar_dates, $tipo_calendario, $view_nav = 0) {
        @session_start();
        $CI = &get_instance();
        $CI->load->helper('calendar');
        $CI->load->model('teacher/Actas_model');
        $fecha_server = strtotime(date_today_mex());
        $dato = 0;
        $lic = array();
        $licenciaturas = $CI->Actas_model->get_lics_by_id_teacher($_SESSION['id_teacher']);
//        foreach ($calendar_dates as $calendar_date) {
        foreach ($calendar_dates as $calendar_date) {
            foreach ($licenciaturas as $licenciatura) {

                if (($fecha_server >= $calendar_date['fecha_ini'] &&
                        $fecha_server <= $calendar_date['fecha_fin']) &&
                        $licenciatura["ID_campus_career"] == $calendar_date['id_campus_carrer']
                ) {
                    $lic[]+=($calendar_date['id_campus_carrer']);
                    $dato += 1;
                }
            }
        }
         $_SESSION['id_campus_carrer'] = $lic;
        if ($view_nav == 0) {//esto es solo para validar los controladores
            if ($dato == 0) {
                @session_destroy();
                calendario_view($tipo_calendario);
            } else {
                
            }
        } else {//esto e spara controlar que s epuede ver en el nav
            return $dato;
        }
    }

}

if (!function_exists('calendario_view')) {

    function calendario_view($numero_calendario) {
        $CI = &get_instance();
        $data = array();
        $data = [
            'inicio_view' => $CI->load->view('templates/general/calendarios/calendario_view', array("numero_calendario"=>$numero_calendario), TRUE),
        ];


        $data = array(
            'header' => $CI->load->view('templates/general/header_view', $data, TRUE),
            'main' => $CI->load->view('templates/general/main_view', $data, TRUE),
            'footer' => $CI->load->view('templates/general/footer_view', $data, TRUE)
        );
        exit($CI->load->view('layout_general_view', $data, TRUE));
    }

}
if (!function_exists('cerrado_view')) {

    function cerrado_view($tipo) {
        $CI = &get_instance();
        $data = array();
        switch ($tipo) {
            case 2:

                $data = [
                    'inicio_view' => $CI->load->view('templates/general/cierre/cerrado_teacher_view', $data, TRUE),
                ];

                break;
            case 3:

                $data = [
                    'inicio_view' => $CI->load->view('templates/general/cierre/cerrado_admin_view', $data, TRUE),
                ];

                break;
        }

        $data = array(
            'header' => $CI->load->view('templates/general/header_view', $data, TRUE),
            'main' => $CI->load->view('templates/general/main_view', $data, TRUE),
            'footer' => $CI->load->view('templates/general/footer_view', $data, TRUE)
        );
        exit($CI->load->view('layout_general_view', $data, TRUE));
    }

}
