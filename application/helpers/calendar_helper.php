<?php
function date_today()
{
    date_default_timezone_set('America/Mexico_City');
    return strtotime(date('Y-m-d H:i:s'));
}
// -------------------------------------------------------------------------------- C A L E N D A R I O      D O C E N T E--------------------------------------------------
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//primer bloque ---
//Registro de calificaciones primer bloque
//Ordinario
//26 al 30 abril
if (!function_exists('calendario_teacher_1_title')) {

    function calendario_teacher_1_title()
    {
        return "Registro de calificaci&oacute;n primer bloque- Docente";
    }
}
if (!function_exists('calendario_teacher_1_fecha_ini')) {

    function calendario_teacher_1_fecha_ini()
    {
        return strtotime('2022-11-16 16:00:00');
    }
}
if (!function_exists('calendario_teacher_1_fecha_fin')) {

    function calendario_teacher_1_fecha_fin()
    {
        return strtotime('2022-11-27 23:59:00');
    }
}
if (!function_exists('calendario_docente_1')) {

    function calendario_docente_1()
    {
        return $calendar_dates = array(
            ["id_campus_carrer" => 1, 'fecha_ini' => strtotime("2022-10-26 07:00:00"), 'fecha_fin' => strtotime("2022-11-27 23:59:00")],//lAC
            ["id_campus_carrer" => 2, 'fecha_ini' => strtotime("2022-10-26 07:00:00"), 'fecha_fin' => strtotime("2022-11-27 23:59:00")],//LMV
            ["id_campus_carrer" => 3, 'fecha_ini' => strtotime("2022-10-26 07:00:00"), 'fecha_fin' => strtotime("2022-11-27 23:59:00")],//LAIT
            ["id_campus_carrer" => 4, 'fecha_ini' => strtotime("2022-10-26 07:00:00"), 'fecha_fin' => strtotime("2022-11-27 23:59:00")],//LHNM
			["id_campus_carrer" => 5, 'fecha_ini' => strtotime("2022-10-26 07:00:00"), 'fecha_fin' => strtotime("2022-11-27 23:59:00")],//LCF
			["id_campus_carrer" => 6, 'fecha_ini' => strtotime("2022-10-26 07:00:00"), 'fecha_fin' => strtotime("2022-11-27 23:59:00")],//lri	n
			["id_campus_carrer" => 7, 'fecha_ini' => strtotime("2022-10-26 07:00:00"), 'fecha_fin' => strtotime("2022-11-27 23:59:00")],//LP
			["id_campus_carrer" => 8, 'fecha_ini' => strtotime("2022-10-26 07:00:00"), 'fecha_fin' => strtotime("2022-11-27 23:59:00")],//LDC
        );
    }
}
//Registro de calificaciones segundo bloque
//Ordinario
//5 al 9 julio
if (!function_exists('calendario_teacher_2_title')) {

	function calendario_teacher_2_title()
	{
		return "Registro de calificaci&oacute;n segundo bloque-Docente";
	}
}
if (!function_exists('calendario_teacher_2_fecha_ini')) {

    function calendario_teacher_2_fecha_ini()
    {
        return strtotime('2022-07-20  07:00:00');
    }
}
if (!function_exists('calendario_teacher_2_fecha_fin')) {

    function calendario_teacher_2_fecha_fin()
    {
        return strtotime('2022-07-22  23:59:00');
    }
}
if (!function_exists('calendario_docente_2')) {

    function calendario_docente_2()
    {
        return $calendar_dates = array(
            ["id_campus_carrer" => 1, 'fecha_ini' => strtotime("2022-07-20 07:00:00"), 'fecha_fin' => strtotime("2022-07-22 23:59:00")],//lAC
            ["id_campus_carrer" => 2, 'fecha_ini' => strtotime("2022-07-20 07:00:00"), 'fecha_fin' => strtotime("2022-07-22 23:59:00")],//LMV
            ["id_campus_carrer" => 3, 'fecha_ini' => strtotime("2022-07-20 07:00:00"), 'fecha_fin' => strtotime("2022-07-22 23:59:00")],//LAIT
            ["id_campus_carrer" => 4, 'fecha_ini' => strtotime("2022-07-20 07:00:00"), 'fecha_fin' => strtotime("2022-07-22 23:59:00")],//LHNM
            ["id_campus_carrer" => 5, 'fecha_ini' => strtotime("2022-07-20 07:00:00"), 'fecha_fin' => strtotime("2022-07-22 23:59:00")],//LCF
            ["id_campus_carrer" => 6, 'fecha_ini' => strtotime("2022-07-20 07:00:00"), 'fecha_fin' => strtotime("2022-07-22 23:59:00")],//lri	n
            ["id_campus_carrer" => 7, 'fecha_ini' => strtotime("2022-07-20 07:00:00"), 'fecha_fin' => strtotime("2022-07-22 23:59:00")],//LP
            ["id_campus_carrer" => 8, 'fecha_ini' => strtotime("2022-07-20 07:00:00"), 'fecha_fin' => strtotime("2022-07-22 23:59:00")],//LDC
        );
    }
}
//Rectificación de calificación de los dos bloques
//Ordinario
//10 al 13 julio
if (!function_exists('calendario_teacher_4_title')) {

	function calendario_teacher_4_title()
	{
		return "Rectificaci&oacute;n de calificaci&oacute;n de los dos bloques ordinario-docente";
	}
}
if (!function_exists('calendario_teacher_4_fecha_ini')) {

    function calendario_teacher_4_fecha_ini()
    {
        return strtotime('2022-11-14 07:00:00');
    }
}
if (!function_exists('calendario_teacher_4_fecha_fin')) {

    function calendario_teacher_4_fecha_fin()
    {
        return strtotime('2022-11-14 11:59:00');
    }
}
if (!function_exists('calendario_docente_4')) {

    function calendario_docente_4()
    {
        return $calendar_dates = array(
            ["id_campus_carrer" => 1, 'fecha_ini' => strtotime("2022-11-14 07:00:00"), 'fecha_fin' => strtotime("2022-11-15 23:59:00")],//lAC
            ["id_campus_carrer" => 2, 'fecha_ini' => strtotime("2022-11-14 07:00:00"), 'fecha_fin' => strtotime("2022-11-15 23:59:00")],//LMV
            ["id_campus_carrer" => 3, 'fecha_ini' => strtotime("2022-11-14 07:00:00"), 'fecha_fin' => strtotime("2022-11-15 23:59:00")],//LAIT
            ["id_campus_carrer" => 4, 'fecha_ini' => strtotime("2022-11-14 07:00:00"), 'fecha_fin' => strtotime("2022-11-15 23:59:00")],//LHNM
            ["id_campus_carrer" => 5, 'fecha_ini' => strtotime("2022-11-14 07:00:00"), 'fecha_fin' => strtotime("2022-11-15 23:59:00")],//LCF
            ["id_campus_carrer" => 6, 'fecha_ini' => strtotime("2022-11-14 07:00:00"), 'fecha_fin' => strtotime("2022-11-15 23:59:00")],//lri	n
            ["id_campus_carrer" => 7, 'fecha_ini' => strtotime("2022-11-14 07:00:00"), 'fecha_fin' => strtotime("2022-11-15 23:59:00")],//LP
            ["id_campus_carrer" => 8, 'fecha_ini' => strtotime("2022-11-14 07:00:00"), 'fecha_fin' => strtotime("2022-11-15 23:59:00")],//LDC
        );
    }
}
//Registro de calificaciones de extraordinarios
// Extraordinario
// 2 al 6 agosto
if (!function_exists('calendario_teacher_5_title')) {

	function calendario_teacher_5_title()
	{
		return "Registro de calificaci&oacute;n de extraordinarios-docente";
	}
}
if (!function_exists('calendario_teacher_5_fecha_ini')) {

    function calendario_teacher_5_fecha_ini()
    {
        return strtotime('2022-10-26 07:00:00');
    }
}
if (!function_exists('calendario_teacher_5_fecha_fin')) {

    function calendario_teacher_5_fecha_fin()
    {
        return strtotime('2022-11-10 23:59:00');
    }
}

if (!function_exists('calendario_docente_5')) {

    function calendario_docente_5()
    {

        return $calendar_dates = array(
            ["id_campus_carrer" => 1, 'fecha_ini' => strtotime("2022-10-26 07:00:00"), 'fecha_fin' => strtotime("2022-11-10 23:59:00")],//lAC
            ["id_campus_carrer" => 2, 'fecha_ini' => strtotime("2022-10-26 07:00:00"), 'fecha_fin' => strtotime("2022-11-10 23:59:00")],//LMV
            ["id_campus_carrer" => 3, 'fecha_ini' => strtotime("2022-10-26 07:00:00"), 'fecha_fin' => strtotime("2022-11-10 23:59:00")],//LAIT
            ["id_campus_carrer" => 4, 'fecha_ini' => strtotime("2022-10-26 07:00:00"), 'fecha_fin' => strtotime("2022-11-10 23:59:00")],//LHNM
            ["id_campus_carrer" => 5, 'fecha_ini' => strtotime("2022-10-26 07:00:00"), 'fecha_fin' => strtotime("2022-11-10 23:59:00")],
            ["id_campus_carrer" => 6, 'fecha_ini' => strtotime("2022-10-26 07:00:00"), 'fecha_fin' => strtotime("2022-11-10 23:59:00")],
            ["id_campus_carrer" => 7, 'fecha_ini' => strtotime("2022-10-26 07:00:00"), 'fecha_fin' => strtotime("2022-11-10 23:59:00")],
            ["id_campus_carrer" => 8, 'fecha_ini' => strtotime("2022-10-26 07:00:00"), 'fecha_fin' => strtotime("2022-11-10 23:59:00")],
        );
    }
}
//Rectificación de calificación
//Extraordinario
//9 al 13 agosto
if (!function_exists('calendario_teacher_7_title')) {

	function calendario_teacher_7_title()
	{
		return "Rectificaci&oacute;n de calificaci&oacute;n de extraordinarios-docente";
	}
}
if (!function_exists('calendario_teacher_7_fecha_ini')) {

    function calendario_teacher_7_fecha_ini()
    {
        return strtotime('2022-11-14 07:00:00');
    }
}
if (!function_exists('calendario_teacher_7_fecha_fin')) {

    function calendario_teacher_7_fecha_fin()
    {
        return strtotime('2022-11-15 23:59:00');
    }
}

if (!function_exists('calendario_docente_7')) {

    function calendario_docente_7()
    {
        return $calendar_dates = array(
            ["id_campus_carrer" => 1, 'fecha_ini' => strtotime("2022-08-05 07:00:00"), 'fecha_fin' => strtotime("2022-11-15 23:59:00")],
            ["id_campus_carrer" => 2, 'fecha_ini' => strtotime("2022-08-05 07:00:00"), 'fecha_fin' => strtotime("2022-11-15 23:59:00")],
            ["id_campus_carrer" => 3, 'fecha_ini' => strtotime("2022-08-05 07:00:00"), 'fecha_fin' => strtotime("2022-11-15 23:59:00")],
            ["id_campus_carrer" => 4, 'fecha_ini' => strtotime("2022-08-05 07:00:00"), 'fecha_fin' => strtotime("2022-11-15 23:59:00")],
			["id_campus_carrer" => 5, 'fecha_ini' => strtotime("2022-08-05 07:00:00"), 'fecha_fin' => strtotime("2022-11-15 23:59:00")],
			["id_campus_carrer" => 6, 'fecha_ini' => strtotime("2022-08-05 07:00:00"), 'fecha_fin' => strtotime("2022-11-15 23:59:00")],
			["id_campus_carrer" => 7, 'fecha_ini' => strtotime("2022-08-05 07:00:00"), 'fecha_fin' => strtotime("2022-11-15 23:59:00")],
			["id_campus_carrer" => 8, 'fecha_ini' => strtotime("2022-08-05 07:00:00"), 'fecha_fin' => strtotime("2022-11-15 23:59:00")],
        );
    }
}
if (!function_exists('anio')) {

    function anio()
    {
        return date('2022');
    }
}
if (!function_exists('ciclo')) {
    function ciclo()
    {
        $ciclo = '2022-2-B1';
        return $ciclo;
    }
}
if (!function_exists('regex_calificacion')) {
    function regex_calificacion()
    {
        return "/^[1-9][0-9]$|^[0-9]\.[1-9]{1,2}$|^[0-9][0-9]\.[0-9]{1,2}$|^[0-9]\.[0-9]{1,2}$|^[0-9]$|^[N,n][P,p]$|^100|[S,s][C,c]$/";

    }
}
