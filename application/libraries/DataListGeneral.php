<?php

class DataListGeneral
{

    public function get_job_list()
    {
        $jobs = array(
            '' => 'Administrativos',
            '1' => 'Biología',
            '2' => 'Comunicaciones',
            '3' => 'Contabilidad',
            '4' => 'Construcción',
            '5' => 'Cratividad y Diseño Comercial',
            '6' => 'Derecho y Leyes',
            '7' => 'Educación',
            '8' => 'Ingeniería',
            '9' => 'Logística, Transportación',
            '10' => 'Manufactura, Producción y Operación',
            '11' => 'Mercadotecnia, Publicidad y Relaciones Públicas',
            '12' => 'Recursos Humanos',
            '13' => 'Salud y Belleza',
            '14' => 'Sector Salud',
            '15' => 'Seguro y Reaseguro',
            '16' => 'Tecnologías de la Información / Sistemas',
            '17' => 'Turismo y Gastronomía',
            '18' => 'Ventas',
            '19' => 'Veterinaria'
        );
        return $jobs;
    }

    public function sex()
    {
        $data = array(
            ''     => 'Selecciona una opción, por favor.',
            '1' => 'Hombre',
            '2' => 'Mujer',
        );
        return $data;
    }

    public function civil_state()
    {
        $data = array(
            ''    => 'Selecciona una opción, por favor.',
            '1' => 'Soltero(a)',
            '2' => 'Casado(a)',
            '3' => 'Unión libre',
            '4' => 'Divorciado'
        );
        return $data;
    }

    public function have_job()
    {
        $data = array(
            ''     => 'Selecciona una opción, por favor.',
            '1' => 'N/A',
            '2' => 'Medio tiempo',
            '3' => 'Tiempo completo',
        );
        return $data;
    }

    public function campus()
    {
        $data = array(
            ''     => 'Selecciona una opción, por favor.',
            '1' => 'Azcapotzalco',
            '2' => 'Coyoacán',
            '3' => 'Gustavo A. Madero',
            '4' => 'Unidad Académica a Distancia',
            '5' => 'Justo Sierra'
        );
        return $data;
    }

    public function modality()
    {
        $data = array(
            ''         => 'Selecciona una opción, por favor.',
            '1'        => 'Presencial',
            '2'        => 'A distancia'
        );
        return $data;
    }

    public function yes_not()
    {

        $data = array(
            '0'        => 'No',
            '1'        => 'Si',
        );
        return $data;
    }

    public function career_irc()
    {

        $data = array(
            ''         => 'Selecciona una opción, por favor.',
            '1'     => 'Ingeniería en Control y Automatización', // IPN
            '2'     => 'Licenciatura en Administración y Comercio', //A distancia
            '3'     => 'Licenciatura en Ciencias Ambientales', //UNAM
            '4'     => 'Licenciatura en Ciencias de la Comunicación', //UNAM
            '5'     => 'Licenciatura en Ciencias de Datos para Negocios', //Presencial
            '6'     => 'Licenciatura en Contaduría y Finanzas', //Presencial
            '7'     => 'Licenciatura en Derecho y Criminología', //Presencial
            '8'     => 'Licenciatura en Derecho y Seguridad Cuidadana', //Presencial
            '9'     => 'Licenciatura en Mercadotecnia y Ventas', //A distancia
            '10'     => 'Licenciatura en Psicología', // IPN
            '11'     => 'Licenciatura en Relaciones Internacionales', //UNAM
            '12'     => 'Licenciatura en Tecnología de la Información y Comunicación', //A distancia
            '13'     => 'Licenciatura en Turismo', //IPN
            '14'     => 'Licenciatura en Urbanismo y Desarrollo Metropolitano' //Presencial
        );
        return $data;
    }

    public function career_here_irc()
    {

        $data = array(
            ''         => 'Selecciona una opción, por favor.',
            '1'     => 'Ingeniería en Control y Automatización', // IPN
            '3'     => 'Licenciatura en Ciencias Ambientales', //UNAM
            '4'     => 'Licenciatura en Ciencias de la Comunicación', //UNAM
            '5'     => 'Licenciatura en Ciencias de Datos para Negocios', //Presencial
            '6'     => 'Licenciatura en Contaduría y Finanzas', //Presencial
            '7'     => 'Licenciatura en Derecho y Criminología', //Presencial
            '8'     => 'Licenciatura en Derecho y Seguridad Cuidadana', //Presencial
            '10'     => 'Licenciatura en Psicología', // IPN
            '11'     => 'Licenciatura en Relaciones Internacionales', //UNAM
            '13'     => 'Licenciatura en Turismo', //IPN
            '14'     => 'Licenciatura en Urbanismo y Desarrollo Metropolitano' //Presencial
        );
        return $data;
    }

    public function career_virtual_irc()
    {

        $data = array(
            ''         => 'Selecciona una opción, por favor.',
            '2'     => 'Licenciatura en Administración y Comercio', //A distancia
            '9'     => 'Licenciatura en Mercadotecnia y Ventas', //A distancia
            '12'     => 'Licenciatura en Tecnología de la Información y Comunicación', //A distancia
        );
        return $data;
    }

    public function shifts()
    {

        $data = array(
            ''         => 'Selecciona una opción, por favor.',
            '1'     => 'Primer turno: 7:00 a 11:00',
            '2'     => 'Segundo turno: 11:00 a 15:00',
            '3'     => 'Tercer turno: 15:00 a 19:00'
        );
        return $data;
    }

    public function dependets()
    {

        $data = array(
            ''         => 'Selecciona una opción, por favor.',
            '0'     => 0,
            '1'     => 1,
            '2'     => 2,
            '3'     => 3,
            '4'     => 4,
            '5'     => 5,
            '6'     => 6,
            '7'     => 7,
            '8'     => 8,
            '9'     => 9,
            '10'    => 10,
        );
        return $data;
    }

    public function get_gradeStudies_list()
    {
        $data = array(
            '' => 'Selecciona una de las opciones',
            '1' => 'Licenciatura',
            '2' => 'Ingeniería',
            '3' => 'Maestría',
            '4' => 'Doctorado',
            '5' => 'Posdoctorado',
        );
        return $data;
    }

    public function time_living()
    {
        $data = array(
            ''     => 'Selecciona una opción, por favor.',
            '1' => 'Menos de un año',
            '2' => 'Más de un año, menos de 5 años',
            '3' => 'Más de 5 años'
        );
        return $data;
    }

    public function high_school()
    {
        $data = array(
            ''         => 'Selecciona una opción, por favor.',
            '1'        => 'Vinculada a la SEP',
            '2'        => 'Privada',
            '3'        => 'UNAM',
            '4'        => 'IPN',
            '5'        => 'Abierta'
        );
        return $data;
    }
}
