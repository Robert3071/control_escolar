<?php
defined('BASEPATH') or exit('No direct script access allowed');
// Al requerir el autoload, cargamos todo lo necesario para trabajar
//require_once APPPATH."third_party/dompdf/src/autoloader.php";

require_once APPPATH . 'third_party/dompdf/lib/html5lib/Parser.php';
require_once APPPATH . 'third_party/dompdf/src/Autoloader.php';
Dompdf\Autoloader::register();

use Dompdf\Dompdf;

class Pdfgenerator
{
    // por defecto, usaremos papel A4 en vertical, salvo que digamos otra cosa
    //al momento de generar un PDFpublic
    function generate($html, $filename = '', $stream = TRUE, $paper = 'A4', $orientation = "portrait")
    {
        $dompdf = new Dompdf();
        $dompdf->loadHtml($html);
        $dompdf->setPaper($paper, $orientation);
        $dompdf->render();
        if ($stream) {
            // "Attachment" => 1 hará que por defecto los PDF se descarguen
            // en lugar de presentarse en pantalla.
            $dompdf->stream($filename . ".pdf", array("Attachment" => 1));
        } else {
            return $dompdf->output();
        }
    }
    function generate_multi_pdf_save($html, $filename = '', $stream = TRUE, $paper = 'A4', $orientation = "portrait",$folder)
    {
        $dompdf = new Dompdf();
        $dompdf->loadHtml($html);
        $dompdf->setPaper($paper, $orientation);
        $dompdf->render();
		$output= $dompdf->output();
		$file_to_save=$folder.$filename;
        if (!file_exists($folder)) {
        	if(mkdir($folder,0777,true)){
				file_put_contents($file_to_save,$output);
			}
        } else {
            file_put_contents($file_to_save,$output);
        }
    }
}
