var app = new Vue({
    el: '#app',
    data: {
        passwd_usuario: '',
        passwd_usuario_edit: '',
        message_verify_pass: '',
        clase: '',
        verif_pass: 0,
        regex_validacion_password: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&#.$($)$-$_])[A-Za-z\d$@$!%*?&#.$($)$-$_]{8}$/,
        type_password: "password",
        icon_password:"fas fa-eye-slash",
        type_password_edit: "password",
        icon_password_edit:"fas fa-eye-slash"

    },
    methods: {
        verificar_password() {
            if (this.passwd_usuario.length !== 0
                    && this.passwd_usuario_edit === this.passwd_usuario
                    && (this.regex_validacion_password.test(this.passwd_usuario_edit) && this.regex_validacion_password.test(this.passwd_usuario))) {
                this.message_verify_pass = "Coinciden las contrase\u00F1as.";
                this.clase = 'confirmacion';
                this.verif_pass = 0;

            } else
            {
                this.verif_pass = 1;
                this.message_verify_pass = "No coinciden las contrase\u00F1as.";
                this.clase = 'negacion';
            }
        },
        submit_updated_password() {
            axios({
                method: 'post',
                url: base_url + 'session/teacher/Teacher_dashboard/update_password',
                data: {
                    password: this.passwd_usuario
                }
            }).then((res) => {
                console.log(res);
                if (res.data == 0) {
                    Swal.fire({
                        title: 'No coinciden las contrase\u00F1as.',
                        icon: 'error',
                        confirmButtonText: 'Continuar',
                        cancelButtonText: 'Cancelar',
                        showCloseButton: true,
                        width: 400
                    });
                } else {
                    Swal.fire({
                        title: 'Se actualiz\u00F3 correctamente la contrase\u00F1a.',
                        text: "El sistema cerrar\u00E1 sesi\u00F3n.",
                        icon: 'success',
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Aceptar'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            setTimeout(redireccionar, 2000);
                        }
                    });
                }
            }).catch((error) => {
                Swal.fire({
                    title: 'Ocurri\u00F3 un error',
                    icon: 'error',
                    confirmButtonText: 'Continuar',
                    showCloseButton: true,
                    width: 400
                });

            });
        }, show_password() {
            if (this.type_password === 'password') {
                this.type_password = "text";
                this.icon_password = "fas fa-eye";
            } else {
                this.type_password = "password";
                this.icon_password = "fas fa-eye-slash";
            }
        }, show_password_edit() {
            if (this.type_password_edit === 'password') {
                this.type_password_edit = "text";
                this.icon_password_edit = "fas fa-eye";
            } else {
                this.type_password_edit = "password";
                this.icon_password_edit = "fas fa-eye-slash";
            }
        }
    }
});
function redireccionar() {
    window.location.href = base_url + 'acceso/Login/logout_teacher';
}



var alfabeto = '!#$%&*.0123456789?@ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz',
        desplazamiento = 3;

function cifrar(input, desplazamiento) {
    var resultado = '';
    //Usaremos un bucle
    for (var i = 0, c; c = input[i]; i++) {
        var caracter_actual = '',
                indice_actual = alfabeto.indexOf(c);
        console.log(alfabeto.indexOf(c));
        if ((indice_actual + desplazamiento) <= alfabeto.length) {
            //Desaplazamos el caracter.
            caracter_actual = alfabeto[ (indice_actual + desplazamiento) ];

            //Convertimos a ASCII
            caracter_actual = caracter_actual.charCodeAt(0);

            //El guion lo usamos para delimitar cada caracter cifrado y poder decifrarlo.
            resultado += caracter_actual + '-';
        } else {
            var indice_actual_temporal = indice_actual + desplazamiento,
                    sobrante = indice_actual_temporal - alfabeto.length;

            //
            //Usaremos solo el desplazamiento para reemplazar el caracter.
            caracter_actual = alfabeto[sobrante];

            //Convertimos a ASCII
            caracter_actual = caracter_actual.charCodeAt(0);

            resultado += caracter_actual + '-';
        }
    }

    return(resultado.replace(/\-$/, ''));
}





