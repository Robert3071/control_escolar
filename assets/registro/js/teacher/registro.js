var app = new Vue({
	el: '#app',
	data: {
		campus: null,
		modalidad: null,
		years: '2',
		campus_valor: '',
		ver_modalidad: '',
		anio_valor: '', //para el form
		campus_select: '', //para el form
		modalidad_valor: '', //para el form
		groups: [],
		calificacion: [],
		calificacionvali: [],
		ID_teacher_by_group: [],
		ID_student: [],
		status_boton: 1,
		status: 1,
		regex_validacion_calificacion: regex_calificacion,
		error_message: false,
		mostrar_calificar: '2',
		pass_user: '',
		error_credenciales: '2',
		consulta_firma: null,
		ciclo: '2', // Para mostrar form de ciclo
		cycle_axios: '', // traer el ciclo desde axios
		ciclo_valor: '',
		file: '',
		btn_irc: true,
		text_btn_irc: ""
	},
	methods: {
		loadTextFromFile(ev) {
			const file = ev.target.files[0];
			const reader = new FileReader();
			reader.onload = e => {
				this.textFiel = e.target.result;
			};
			console.log(file.name.split(".")[1] );
			if (file.size==0 || file.name.split(".")[1]!=='irc' ) {
				this.btn_irc = false;
				this.text_btn_irc = "No se selccion\u00F3 correctamente el archivo .irc o est\u00E1 vacio";
			} else {
				this.btn_irc = true;
				this.text_btn_irc = " Se seleccion\u00F3 correctamente el archivo .irc";
				this.file = reader.readAsText(file);
			}
		},
		compara_pass() {
			this.comparar(this.textFiel, this.pass_user);
		},
		comparar(file, pass) {
			let muestra = 0;
			axios.get(base_url + 'session/teacher/Teacher_dashboard/get_fiel_teacher_firma')
				.then(function (response) {
					this.consulta_firma = response.data
				})
				.catch(function (error) {
					console.log(error);
				})
				.then(function () {
					if (file === this.consulta_firma.key_validate && pass === this.consulta_firma.key_pass) {
						let muestra = document.getElementById('prueba_axios');
						let borra = document.getElementById('firma');
						this.mostrar_calificar = '1';
						muestra.innerHTML = "<button  class='btn obtener' type='submit'>Calificar grupo</button>";
						padre = borra.parentNode;
						padre.removeChild(borra);
						console.log('Correcto');
						Swal.fire({
							title: 'Firma válida.',
							icon: 'success',
							confirmButtonText: 'Continuar',
							showCancelButton: true,
							showCloseButton: true,
							cancelButtonText: 'Cancelar',

							width: 400
						});
					} else {
						Swal.fire({
							title: 'Credenciales erróneas',
							icon: 'error',
							confirmButtonText: 'Continuar',
							//showCancelButton: true,
							cancelButtonText: 'Cancelar',
							showCloseButton: true,
							width: 400
						});
						return false;
					}
				});
			this.mostrar_calificar = '1';
		},
		validate_calificacion: function (i) {
			return (this.calificacion[i] === this.calificacionvali[i] && (this.regex_validacion_calificacion.test(this.calificacion[i]) && this.regex_validacion_calificacion.test(this.calificacionvali[i])))
				? true : false;
		},
		get_all_students(id_group) {
			axios.get(base_url + `session/teacher/Teacher_dashboard/calificar_grupo?ID_teacher_by_group=${id_group}`).then(function (response) {
			});
		},
		onSubmit_group() {
			axios.get(base_url + 'session/teacher/Teacher_dashboard/get_all_groups_by_id_teacher_id_group_year_and_id_type_group',
				{
					params: {
						anio_valor: this.anio_valor,
						campus_select: this.campus_select,
						modalidad_valor: this.modalidad_valor
					}
				})
				.then((res) => {
					this.groups = res.data;
				})
				.catch((error) => {
				}).finally(() => {
			});
		},
		campus2(dato) {
			this.campus_select = dato;
			this.traer_anios(dato);
		},
		traer_anios(camp) {
			axios.get(base_url + `session/teacher/Teacher_dashboard/get_years_by_id_teacher?ID_campus_career=${camp}`).then(response => (this.years = response.data))
		},
		submitForm() {

			axios.get(base_url + 'session/teacher/Teacher_dashboard/get_all_groups_by_id_teacher_id_group_year_and_id_type_group',
				{
					params: {
						anio_valor: this.anio_valor,
						campus_select: this.campus_select,
						ciclo: this.ciclo_valor,
						modalidad_valor: this.modalidad_valor
					}
				})
				.then((res) => {
					this.groups = res.data;
				})
				.catch((error) => {
				}).finally(() => {
			});
		},
		campus2(dato) {
			this.campus_select = dato;
			this.traer_anios(dato);
			this.modalidad = '';
		},
		traer_anios(camp) {
			axios.get(base_url + `session/teacher/Teacher_dashboard/get_years_by_id_teacher?ID_campus_career=${camp}`).then(response => (this.years = response.data));
		},
		traer_modalidad2() {
			this.get_modality();
			this.ver_modalidad = 1;
		},
		traer_modalidad(dato) {
			this.ciclo_valor = dato;
			this.get_modality();
			this.ver_modalidad = 1;
		},
		traer_ciclo2() {

			this.ciclo = 1;
		},
		traer_ciclo(dato) {
			this.anio_valor = dato;
			this.get_cycle();
			this.ciclo = 1;
		},
		get_cycle() {
			axios.get(base_url + `session/teacher/Teacher_dashboard/get_cicle_by_id_teacher?ID_campus_career=${this.campus_select}&anio=${this.anio_valor}`).then(response => (this.cycle_axios = response.data /*console.log(response.data)*/))
		},
		get_modality() {
			axios.get(base_url + `session/teacher/Teacher_dashboard/get_all_type_group_by_id_teacher?ID_campus_career=${this.campus_select}&year_active=${this.anio_valor}&ciclo=${this.ciclo_valor}`).then(response => (this.modalidad = response.data))
		},
		poner_modalidad(modalidad_valor) {
			this.modalidad_valor = modalidad_valor;
		},
		onSubmit_califiaciones: function (grupo) {
			let cont = 0;
			for (let i = 0; i < grupo; i++) {
				if (this.calificacion[i] === this.calificacionvali[i] && (this.regex_validacion_calificacion.test(this.calificacion[i]) && this.regex_validacion_calificacion.test(this.calificacionvali[i]))) {
					cont++;
				}
			}
			if (cont !== grupo) {
				event.preventDefault();
				Swal.fire({
					title: 'Verifique que las calificaciones coincidan.',
					icon: 'error',
					confirmButtonText: 'Continuar',
					showCloseButton: true,
					cancelButtonText: 'Cancelar',
					width: 400
				});
			}
		},
	},
	created() {
		axios.get(base_url + 'session/teacher/Teacher_dashboard/get_all_campus_by_id_teacher').then(response => (this.campus = response.data));
	}
});
