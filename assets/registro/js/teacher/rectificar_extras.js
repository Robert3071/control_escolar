var app = new Vue({
    el: '#app',
    data: {
        rec_campus: null,
        rec_modalidad: null,
        rec_years: '2',
        rec_campus_valor: '',
        rec_ver_modalidad: '',
        rec_anio_valor: '', //para el form
        rec_campus_select: '', //para el form
        rec_modalidad_valor: '', //para el form
        rec_groups: [],
        rec_calificacion: [],
        rec_calificacionvali: [],
        rec_ID_teacher_by_group: [],
        rec_ID_student: [],
        rec_status_boton: 1,
        rec_status: 1,
     regex_validacion_calificacion: regex_calificacion,
        rec_error_message: false,
        rec_mostrar_calificar: '2',
        rec_pass_user: '',
        rec_error_credenciales: '2',
        rec_consulta_firma: null,
        rec_grupos: null,
        ciclo: '2', //mostrar ciclo
        cycle_axios: '', // Guardar ciclo del axios
        cycle_selec: '', // ciclo seleccionado
    },
    methods: {
        loadTextFromFile(ev) {
            const file = ev.target.files[0];
            const reader = new FileReader();
            reader.onload = e => {
                this.rec_textFiel = e.target.result;
            };
            reader.readAsText(file);
        },
        compara_pass() {
            this.comparar(this.rec_textFiel, this.rec_pass_user);
        },
        comparar(file, pass) {
            let rec_muestra = 0;
            axios.get(base_url + 'session/teacher/Teacher_dashboard/get_fiel_teacher_firma')
                    .then(function (response) {
                        this.rec_consulta_firma = response.data;
                    })
                    .catch(function (error) {
                    })
                    .then(function () {
                        if (file === this.rec_consulta_firma.key_validate && pass === this.rec_consulta_firma.key_pass) {
                            let muestra = document.getElementById('prueba_axios');
                            let borra = document.getElementById('firma');
                            this.rec_mostrar_calificar = '1';
                            muestra.innerHTML = "<button  class='btn obtener' type='submit'>Calificar grupo</button>";
                            padre = borra.parentNode;
                            padre.removeChild(borra);
                            Swal.fire({
                                title: 'Firma válida.',
                                icon: 'success',
                                confirmButtonText: 'Continuar',
                                //showCancelButton: true,
                                showCloseButton: true,
                                width: 400
                            });
                        } else {
                            Swal.fire({
                                title: 'Credenciales erróneas',
                                icon: 'error',
                                confirmButtonText: 'Continuar',
                                //showCancelButton: true,
                                showCloseButton: true,
                                cancelButtonText: 'Cancelar',
                                width: 400
                            });
                            return false;
                        }
                    });
            this.rec_mostrar_calificar = '1';
        },
        validate_calificacion: function (i) {
            return (this.rec_calificacion[i] === this.rec_calificacionvali[i]
                    &&
                    (this.regex_validacion_calificacion.test(this.rec_calificacion[i]) && this.regex_validacion_calificacion.test(this.rec_calificacionvali[i])))

                    ? true : false;
        },
        get_all_students(id_group) {
            axios.get(base_url + `session/teacher/rectification_actas_extra/calificar_grupo?ID_teacher_by_group=${id_group}`).then(function (response) {
            });
        },
        onSubmit_group() {
            axios.get(base_url + 'session/teacher/rectification_actas_extra/get_all_groups_by_id_teacher_id_group_year_and_id_type_group',
                    {params: {anio_valor: this.rec_anio_valor, campus_select: this.rec_campus_select, modalidad_valor: this.rec_modalidad_valor}})
                    .then((res) => {
                        this.rec_groups = res.data;
                    })
                    .catch((error) => {
                    }).finally(() => {
            });
        },
        campus2(dato) {
            this.rec_campus_select = dato;
            this.traer_anios(dato);
        },
        traer_anios(camp) {
            axios.get(base_url + `session/teacher/rectification_actas_extra/get_years_by_id_teacher?ID_campus_career=${camp}`).then(response => (this.rec_years = response.data))
        },
        submitForm() {

            axios.get(base_url + 'session/teacher/rectification_actas_extra/get_all_groups_by_id_teacher_id_group_year_and_id_type_group',
                    {params: {anio_valor: this.rec_anio_valor, campus_select: this.rec_campus_select, ciclo: this.cycle_selec, modalidad_valor: this.rec_modalidad_valor}})
                    .then((res) => {
                        this.rec_groups = res.data;
                    })
                    .catch((error) => {
                    }).finally(() => {
            });
        },
        campus2(dato) {
            this.rec_campus_select = dato;
            this.traer_anios(dato);
            this.rec_modalidad = '';
        },
        traer_anios(camp) {
            axios.get(base_url + `session/teacher/rectification_actas_extra/get_years_by_id_teacher?ID_campus_career=${camp}`).then(response => (this.rec_years = response.data));
        },
        traer_ciclo2() {

            this.ciclo = 1;
        },
        traer_ciclo(dato) {
            this.rec_anio_valor = dato;
            this.get_cycle();
            this.ciclo = 1;
        },
        get_cycle() {
            axios.get(base_url + `session/teacher/rectification_actas_extra/get_ciclo_by_id_teacher?ID_campus_career=${this.rec_campus_select}&year=${this.rec_anio_valor}`).then(response => (this.cycle_axios = response.data /*console.log(response.data)*/))
        },
        traer_modalidad2() {
            this.get_modality();
            this.rec_ver_modalidad = 1;
        },
        traer_modalidad(dato) {
            this.cycle_selec = dato;
            this.get_modality();
            this.rec_ver_modalidad = 1;
        },
        get_modality() {
            axios.get(base_url + `session/teacher/rectification_actas_extra/get_all_type_group_by_id_teacher?ID_campus_career=${this.rec_campus_select}&year_active=${this.rec_anio_valor}`).then(response => (this.rec_modalidad = response.data))
        },
        poner_modalidad(modalidad_valor) {
            this.rec_modalidad_valor = modalidad_valor;
        },
        onSubmit_califiaciones: function (grupo) {
            let cont = 0;
            for (let i = 0; i < grupo; i++) {
                if (this.rec_calificacion[i] === this.rec_calificacionvali[i] && (this.regex_validacion_calificacion.test(this.rec_calificacion[i]) && this.regex_validacion_calificacion.test(this.rec_calificacionvali[i])))

                {
                    cont++;
                }
            }
            if (cont !== grupo) {
                event.preventDefault();
                Swal.fire({
                    title: 'Verifique que las calificaciones coincidan.',
                    icon: 'error',
                    confirmButtonText: 'Continuar',
                    cancelButtonText: 'Cancelar',
                    showCloseButton: true,
                    width: 400
                });
            }
        }
    },
    created() {
        axios.get(base_url + 'session/teacher/rectification_actas_extra/get_all_campus_by_id_teacher').then(response => (this.rec_campus = response.data));
    }
});


