var app = new Vue({
	el: '#app',
	data: {
		licenciaturas: [],
		years: [],
		year: '',
		campus_lic: '',
		ciclos: [],
		ciclo: '',
		tipos_grupos: [],
		tipo_grupo: '',
		grupos: [],
		cantidad_de_grupos: 0,
		cantidad_de_grupos_qualified: 0,
		cantidad_de_grupos_validados: 0
	},
	methods: {
		get_year_by_campus_lic() {
			axios.get(base_url + 'session/admin/validacion/get_year_by_campus_lic',
				{params: {campus_carrer: this.campus_lic}})
				.then((res) => {
					this.years = res.data;
					this.grupos = [];
				});
		},
		get_cicles() {
			axios.get(base_url + 'session/admin/validacion/get_cicles',
				{params: {campus_carrer: this.campus_lic, year: this.year}})
				.then((res) => {
					this.ciclos = res.data;
					this.grupos = [];
				});
		},
		get_type_group() {
			axios.get(base_url + 'session/admin/validacion/get_type_group',
				{params: {campus_carrer: this.campus_lic, year: this.year, ciclo: this.ciclo}})
				.then((res) => {
					this.tipos_grupos = res.data;
					this.grupos = [];
				});
		},
		buscar_grupos() {

			axios.get(base_url + 'session/admin/validacion/get_all_groups_qualified',
				{
					params: {
						campus_carrer: this.campus_lic,
						year: this.year,
						ciclo: this.ciclo,
						tipo_grupo: this.tipo_grupo
					}
				})
				.then((res) => {
					if (res.data.length > 0) {
						this.grupos = res.data;
						this.cantidad_de_grupos_qualified = res.data.length;
						this.get_count_gruops();
						this.get_count_gruops_validated();
					} else {
						this.grupos = res.data;
						Swal.fire({
							title: 'No se encontraron datos',
							icon: 'error',
							confirmButtonText: 'Continuar',
							cancelButtonText: 'Cancelar',
							showCloseButton: true,
							width: 400
						});
					}
				});
		},
		get_count_gruops() {
			axios.get(base_url + 'session/admin/validacion/get_count_gruops',
				{
					params: {
						campus_carrer: this.campus_lic,
						year: this.year,
						ciclo: this.ciclo,
						tipo_grupo: this.tipo_grupo
					}
				})
				.then((res) => {
					this.cantidad_de_grupos = parseInt(res.data.cantidad);
				});
		}, get_count_gruops_validated() {
			axios.get(base_url + 'session/admin/validacion/get_count_gruops_validated',
				{
					params: {
						campus_carrer: this.campus_lic,
						year: this.year,
						ciclo: this.ciclo,
						tipo_grupo: this.tipo_grupo
					}
				})
				.then((res) => {
					this.cantidad_de_grupos_validados = parseInt(res.data.cantidad);
				});
		},
		validad_grupos() {
			axios({
				method: 'post',
				url: base_url + 'session/admin/validacion/validate_grupos',
				data: {
					campus_carrer: this.campus_lic,
					year: this.year,
					ciclo: this.ciclo,
					tipo_grupo: this.tipo_grupo,
					grupos: this.grupos
				}
			}).then((res) => {
				Swal.fire({
					title: 'Se validaron las actas',
					icon: 'success',
					confirmButtonText: 'Continuar',
					cancelButtonText: 'Cancelar',
					showCloseButton: true,
					width: 400
				});
				this.validate = true;
				this.buscar_grupos();
				this.get_count_gruops_validated();
			}).catch((error) => {
				Swal.fire({
					title: 'Ocurri\u00F3 un error',
					icon: 'error',
					confirmButtonText: 'Continuar',
					cancelButtonText: 'Cancelar',
					showCloseButton: true,
					width: 400
				});

			});
		}, download_zip() {
			values = create_values_for_send({
				campus_carrer: this.campus_lic, year: this.year, ciclo: this.ciclo, tipo_grupo: this.tipo_grupo
			})
			get_zip('generar_multi_actas_pdf', values);
			this.delete_multi_actas_pdf();
		},
		delete_multi_actas_pdf() {
			axios.get(base_url + 'session/admin/validacion/delete_multi_actas_pdf',
				{
					params: {
						campus_carrer: this.campus_lic,
						year: this.year,
						ciclo: this.ciclo
					}
				});
		}
	}
	, created() {
		axios.get(base_url + 'session/admin/validacion/get_list_lic_by_campus').then(response => (this.licenciaturas = response.data));
	}
});
var get_zip = function (name_reporte, variables) {
	window.open(base_url + 'session/admin/validacion/' + name_reporte + variables, "_self",
		"width=10,height=10");
};

function create_values_for_send(datos_formulario) {
	var data_get;
	var i = 0;// se usa para que el primer parametro no mande el & al primer valor
	for (var key in datos_formulario) {
		if (i !== 0) {
			data_get = data_get + '&' + key + '=' + datos_formulario[key];
		} else {
			data_get = '?' + key + '=' + datos_formulario[key];
		}
		i++;
	}
	return data_get; // se construye el texto
}

