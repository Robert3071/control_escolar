

 $(document).ready(function() {
  $('#example').DataTable({
    dom: 'Bfrtip',
    "scrollX": true,
    buttons: [{
      extend: 'excelHtml5',
      text: '<i class="fas fa-file-excel fa-2x"></i> ',
      titleAttr: 'Exportar a Excel',
      className: 'btn btn-edit-excel color_excel',
      //title: 'Rsgistro_extraordinarios' + '' + (new Date()).getDate() + '' + (new Date()).getMonth() + '_' + (new Date()).getFullYear()
  },
  {
      extend: 'pdfHtml5',
      text: '<i class="fas fa-file-pdf fa-2x"></i> ',
      titleAttr: 'Exportar a PDF',
      className: 'btn btn-edit-pdf color_pdf',
      //title: 'Rsgistro_extraordinarios' + '' + (new Date()).getDate() + '' + (new Date()).getMonth() + '_' + (new Date()).getFullYear()
  },
  {
      extend: 'print',
      text: '<i class="fa fa-print fa-2x"></i> ',
      titleAttr: 'Imprimir',
      className: 'btn color_imprimir'
  },
  ,
],    
    "paginate": {
      "first": " |< ",
      "previous": "Ant.",
      "next": "Sig.",
      "last": " >| ",
      className: 'color_imprimir'
    },
    "language": {
      "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
    },
    
  });
  $('.dataTables_length').addClass('bs-select');
});